package com.aaa.front.controller;


import com.aaa.front.entity.TbOrderStatusCode;
import com.aaa.front.service.TbOrderStatusCodeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  TbOrderStatusCodeController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-12 10:18:23
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbOrderStatusCode")
public class TbOrderStatusCodeController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbOrderStatusCodeService tbOrderStatusCodeService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbOrderStatusCode> page) {
        return success(this.tbOrderStatusCodeService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbOrderStatusCodeService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbOrderStatusCodeService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param tbOrderStatusCode 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbOrderStatusCode tbOrderStatusCode) {
        return success(this.tbOrderStatusCodeService.save(tbOrderStatusCode));
    }

    /**
     * 修改数据
     *
     * @param tbOrderStatusCode 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbOrderStatusCode tbOrderStatusCode) {
        return success(this.tbOrderStatusCodeService.updateById(tbOrderStatusCode));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbOrderStatusCodeService.removeById(id));
    }
}

