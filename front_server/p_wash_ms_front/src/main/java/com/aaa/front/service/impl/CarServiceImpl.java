package com.aaa.front.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.dao.CarDao;
import com.aaa.front.entity.Car;
import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.service.CarService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ fileName:  CarServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 10:30:56
 * @ version:1.0.0
 */
@Service("carService")
public class CarServiceImpl extends ServiceImpl<CarDao, Car> implements CarService {

    /**
     * 服务对象
     */
    @Resource
    private CarDao carDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<Car> queryPage(PagePlugin<Car> pagePlugin) {
        // 分页参数设置
        Page<Car> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.carDao.selectPage(page, queryWrapper);
    }

    /**
     * 多表联查
     *
     * @return
     */
    @Override
    public List<Car> queryPageAll(Integer userId) {

        // 使用加各种参数  判断某字符串是否为空
        return this.carDao.query(userId);
    }

    @Override
    public List<Car> queryStore(Integer userId, Integer storeId) {
        return carDao.queryStore(userId, storeId);
    }

    /**
     * @return
     */
    @Override
    public List<Car> queryCar(Integer id) {
        // 使用加各种参数  判断某字符串是否为空
        return this.carDao.queryCar(id);
    }

    @Override
    public Car queryCarNo(String carNo, Integer storeId) {
        return carDao.queryCarNo(carNo, storeId);
    }

    @Override
    public IPage queryMune(PagePlugin<Car> pagePlugin) {
        // 分页参数设置
        Page<Car> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        return carDao.queryMune(page);
    }
}

