package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbProductComment
 * @ description: 门店评价
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:14
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_product_comment") //设值数据库表名
public class TbProductComment extends Model<TbProductComment> implements Serializable {
    /**
     * 商品评论id，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 评价类型 1 有图 0 无图
     */
    private String isimage;

    /**
     * 1-5 级，1-3 差评，4 中评，5 好评
     */
    private String starlevel;

    /**
     * 评价信息
     */
    private String evaluate;

    /**
     * 商品id，关联商品表
     */
    private Integer goodid;

    /**
     * 评价时间
     */
    private Date evaluatetime;

    /**
     * 用户id
     */
    private Integer userid;

    /**
     * 图片
     */
    private String image;

    /**
     * 门店id
     */
    private Integer outletid;
}

