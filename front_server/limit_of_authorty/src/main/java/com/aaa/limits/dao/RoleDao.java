package com.aaa.limits.dao;

import com.aaa.limits.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName: 角色信息表(Role)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:40
 * @ version:1.0.0
 */
public interface RoleDao extends BaseMapper<Role> {
    /**
     * 根据参数查询数据
     * @param role
     * @return
     */
    //根据参数查询
    List<Role> queryParam(Role role);

    /**
     * 查询角色权限关联表  根据角色ID查询该角色关联的所有权限ID集合
     * @param roleId 编号
     * @return
     */
    List<Integer> queryMenuIdListByRid(int roleId);


    /**
     * 根据角色ID批量删除该角色对应角色权限关联
     * @param roleId
     * @return
     */
    int deleteRoleByRoleId(int roleId);

    /**
     * 添加职位（角色）和权限的关联
     * @param posId
     * @param menuId
     * @return
     */
    // @Param 给参数命名
    int addPosAndMenu(@Param("roleId") int posId, @Param("menuId") int menuId);
}

