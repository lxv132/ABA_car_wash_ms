package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbOrder
 * @ description: 订单表
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:39
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_order") //设值数据库表名
public class TbOrder extends Model<TbOrder> implements Serializable {
    /**
     * 订单id
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "orderid", type = IdType.AUTO)
    private Integer orderid;

    /**
     * 门店id（关联门店表）
     */
    private Integer storeId;

    /**
     * 商品id（关联商品表）
     */
    private Integer productId;

    /**
     * 地址id （关联地址表）
     */
    private Integer addressId;

    /**
     * 订单创建时间
     */
    private String orderCreateTime;

    /**
     * 订单确定日期
     */
    private String orderDetermineDate;
    /**
     * 订单支付时间
     */
    private String orderPayTime;

    /**
     * 支付方式（1:微信支付，2:支付宝支付）
     */
    private String payType;
    /**
     * 订单号
     */
    private String tcount;

    /**
     * 商品总数
     */
    private String productCount;

    /**
     * 商品总价
     */
    private Double productPrice;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 订单完成时间
     */
    private String orderFinishTime;

    /**
     * 订单状态码
     */
    private Integer orderStatusCode;

    /**
     * 商品使用积分
     */
    private Integer payStatusCode;
    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String storeName;
    /**
     * 门店图片
     */
    @TableField(exist = false)
    private String storeimage;
    /**
     * 商品名称
     */
    @TableField(exist = false)
    private String productName;
    /**
     * 商品单价
     */
    @TableField(exist = false)
    private String price;
    /**
     * 商品图片
     */
    @TableField(exist = false)
    private String imageurl;
    /**
     * 订单状态
     */
    @TableField(exist = false)
    private String orderStatusName;
    /**
     * 商品id
     */
    @TableField(exist = false)
    private Integer commodityId;

    /**
     * 商品名称
     */
    @TableField(exist = false)
    private String commodityName;
    /**
     * 地址详情
     */
    @TableField(exist = false)
    private String detail;
}

