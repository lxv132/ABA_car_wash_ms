package com.aaa.sso.service.Impl;

import com.aaa.common.bo.User;
import com.aaa.common.util.ConstUtil;
import com.aaa.common.util.Result;
import com.aaa.common.util.ResultStatus;
import com.aaa.sso.service.AuthService;
import com.aaa.sso.service.RemoteUserService;
import com.aaa.sso.util.JwtUtils;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @ fileName:UserAuthServiceImpl
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/16 11:41
 * @ version:1.0.0
 */
@Service("uas")
public class UserAuthServiceImpl implements AuthService {
    @Resource
    private RemoteUserService remoteUserService;

    @Resource //依赖注入  spring框架封装的Redis连接模板类
    private RedisTemplate redisTemplate;

    @Override
    public Result auth(String userName, String password) {
        // 根据用户名查询用户信息
        User user = remoteUserService.queryByUserName(userName);
        //判断有没有停用或删除
        if ("1".equals(user.getStatus())||"1".equals(user.getDelFlag())){
            //用户名或者密码错误返回错误JSON
            return new Result(ResultStatus.NO_AUTH_guoqi.getReturnCode(),
                    ResultStatus.NO_AUTH_guoqi.getReturnMes(), "该用户以删除或者下线");
        }
        // 再次使用密码和盐值进行加密运算
        Sha512Hash sha512Hash = new Sha512Hash(password, user.getSalt(),
                ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        // 判断密码是否正确
        if (user.getPassword().equals(sha512Hash.toString())) {
            Map<String,String> payload = new HashMap<>();
            //封装hashmap 放入token中
            payload.put("userId", user.getUserId().toString());
            payload.put("userName", user.getUserName());
            payload.put("login", user.getLoginName());
            //生成token  使用jwt (json web token)
            String token = userName+"-"+JwtUtils.generateToken(payload);
            //将生成的token存储到redis中  方便后面校验token
            //设置redis的key的序列化方式  使用StringRedisSerializer 方式时，存储到redis中的就是string字符串
            redisTemplate.setKeySerializer(new StringRedisSerializer());
            //先查询userName+"-"+*****
            Set keys = redisTemplate.keys(userName + "-*");
            //在循环删除该用户之前登录未过期的token
            for (Object key : keys) {
                redisTemplate.delete(key);
            }
            //把token放入redis把token作为key 用户对象作为值 方便后面使用token获取用户信息
            redisTemplate.opsForValue().set(token,user,8, TimeUnit.HOURS);

            //把token返回给前端
            return new Result(ResultStatus.SUCCESS.getReturnCode(),
                    ResultStatus.SUCCESS.getReturnMes(), token);
        } else {
            //用户名或者密码错误返回错误JSON
            return new Result(ResultStatus.ERROR.getReturnCode(),
                    ResultStatus.ERROR.getReturnMes(), "用户名或者密码错误");
        }
    }

    @Override
    public Result authA(String mobile, String password) {
        return null;
    }

    @Override
    public boolean checkToken(String token) {
        //设置redis的key的序列化方式  使用StringRedisSerializer 方式时，存储到redis中的就是string字符串
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        String substring = token.substring(token.indexOf("-") + 1);
        //根据JwtUtils判断token是否正确  在判断redis是否存在(是否过期)
        if (JwtUtils.verify(substring)&&redisTemplate.hasKey(token)){
            return true;
        }
        return false;
    }

}
