package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.TutorialPicInfo;

/**
 * @ fileName: 教程图片信息表(TutorialPicInfo)表控制层
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:20:06
 * @ version:1.0.0
 */
public interface TutorialPicInfoDao extends BaseMapper<TutorialPicInfo> {

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<TutorialPicInfo> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<TutorialPicInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<TutorialPicInfo> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<TutorialPicInfo> entities);

}

