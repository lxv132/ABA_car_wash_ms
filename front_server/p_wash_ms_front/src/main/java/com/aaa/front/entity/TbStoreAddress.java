package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbStoreAddress
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:22
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_store_address") //设值数据库表名
public class TbStoreAddress extends Model<TbStoreAddress> implements Serializable {
    /**
     * 地址ID，主键
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID，用于关联用户信息
     */
    private Integer userId;

    /**
     * 收件人名称
     */
    private String name;

    /**
     * 收件人手机号码
     */
    private String phone;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区县
     */
    private String district;

    /**
     * 详细地址
     */
    private String detail;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 门店创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 门店更新时间
     */
    private Date updateTime;
}

