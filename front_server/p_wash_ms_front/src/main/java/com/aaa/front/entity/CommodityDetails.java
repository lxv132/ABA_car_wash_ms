package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: CommodityDetails
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:04
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("commodity_details") //设值数据库表名
public class CommodityDetails extends Model<CommodityDetails> implements Serializable {
    /**
     * 商品详情ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "details_id", type = IdType.AUTO)
    private Integer detailsId;

    /**
     * 外键关联商品表
     */
    private Integer commodityId;

    /**
     * 详情内容
     */
    private String content;
}

