package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: ProductReviews
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:29
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("product_reviews") //设值数据库表名
public class ProductReviews extends Model<ProductReviews> implements Serializable {
    /**
     * 商品评价
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "reviews_id", type = IdType.AUTO)
    private Integer reviewsId;

    /**
     * 外键关联商品表
     */
    private Integer productId;

    /**
     * 门店id
     */
    private Integer storeId;

    /**
     * 评价人关联用户表
     */
    private String userId;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评价时间
     */
    private Date evaluateTime;

    /**
     * 图片
     */
    private String picture;

    /**
     * 星级
     */
    private String star;
}

