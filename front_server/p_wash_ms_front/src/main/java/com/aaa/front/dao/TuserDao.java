package com.aaa.front.dao;


import com.aaa.front.entity.Tuser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName: 游客(Tuser)表控制层
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-25 09:52:44
 * @ version:1.0.0
 */
public interface TuserDao extends BaseMapper<Tuser> {


    /**
     * 查询洗车详情信息
     * @param userId
     * @return
     */
    Tuser queryUserWash(@Param("userId") Integer userId);
    //统计图
    List<Tuser> annualQuery(Tuser tuser);



}

