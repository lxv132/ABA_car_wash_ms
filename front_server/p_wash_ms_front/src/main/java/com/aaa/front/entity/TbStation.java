package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * @ fileName: TbStation
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:51:01
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_station") //设值数据库表名
public class TbStation extends Model<TbStation> implements Serializable {
    /**
     * 工位id
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "station_id", type = IdType.AUTO)
    private Integer stationId;

    /**
     * 外键关联门店表
     */
    private Integer storeId;

    /**
     * 图片url
     */
    private String picture;

    /**
     * 几号库
     */
    private String stationName;

    /**
     * 是否空闲(0 空闲中，1 使用中)
     */
    private Integer idle;

    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String storeName;
}

