package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.TbStation;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.TbCommodity;

/**
 * @ fileName: 商品信息
 * (TbCommodity)表控制层
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:32
 * @ version:1.0.0
 */
public interface TbCommodityDao extends BaseMapper<TbCommodity> {

    <E extends IPage<TbCommodity>> E queryAllStore(E page, int storeId);

    <E extends IPage<TbCommodity>> E queryAllUnion(E page, Wrapper<TbCommodity> queryWrapper);
    <E extends IPage<TbCommodity>> E queryAboutCommodity(E page, Wrapper<TbCommodity> queryWrapper);

    //    查询所有门店兑换券
    List<TbCommodity> queryAllTbCommodity(Integer producttypeid);

    /**
     * 根据商品id查询商品
     * @param commodityId
     * @return
     */
    TbCommodity queryAllTbCommodityId(@Param("commodityId") Integer commodityId);
    /**
     * 前端商品显示多表联查商品类型
     * @param product
     * @return
     */
    List<TbCommodity> frontMultiTableJointCheck(TbCommodity product);

}

