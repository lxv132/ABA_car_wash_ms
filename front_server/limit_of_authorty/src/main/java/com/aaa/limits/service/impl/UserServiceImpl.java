package com.aaa.limits.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.limits.dao.UserDao;
import com.aaa.limits.entity.User;
import com.aaa.limits.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ fileName:  UserServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:41
 * @ version:1.0.0
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    /**
     * 服务对象
     */
    @Resource
    private UserDao userDao;

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<User> page(PagePlugin<User> pagePlugin) {
        // 分页参数设置
        IPage<User> page = new Page<User>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            queryWrapper.like(null != pagePlugin.getData().getStatus(), "status", pagePlugin.getData().getStatus());
            queryWrapper.like(null != pagePlugin.getData().getLoginName(), "login_name", pagePlugin.getData().getLoginName());
            queryWrapper.like(null != pagePlugin.getData().getPhonenumber(), "phonenumber", pagePlugin.getData().getPhonenumber());
        }

        return this.userDao.selectPage(page, queryWrapper);
    }

    /**
     * 根据用户编号查询数据
     * @param userName
     * @return
     */
    @Override
    public User queryByUserName(String userName) {
        User user =new User();
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // // 使用加各种参数  判断某字符串是否为空
        user.setUserName(userName);
        // user.setStatus("0");
        // user.setDelFlag("0");
        queryWrapper.eq(null!=user,"user_name",user.getUserName());
        // queryWrapper.eq("del_flag",user.getDelFlag());
        return userDao.selectOne(queryWrapper);
    }
}

