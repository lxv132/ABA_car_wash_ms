package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbStoreEvaluate;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbStoreEvaluateService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 14:17:43
 * @ version:1.0.0
 */
public interface TbStoreEvaluateService extends IService<TbStoreEvaluate> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbStoreEvaluate> queryPage(PagePlugin<TbStoreEvaluate> pagePlugin);
}

