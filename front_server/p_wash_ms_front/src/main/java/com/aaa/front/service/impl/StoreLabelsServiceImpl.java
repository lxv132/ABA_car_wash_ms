package com.aaa.front.service.impl;

import com.aaa.common.vo.Page;

import com.aaa.front.dao.StoreLabelsDao;
import com.aaa.front.entity.StoreLabels;
import com.aaa.front.service.StoreLabelsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (StoreLabels)表服务实现类
 *
 * @author makejava
 * @since 2023-07-01 11:34:27
 */
@Service("storeLabelsService")
public class StoreLabelsServiceImpl extends ServiceImpl<StoreLabelsDao, StoreLabels> implements StoreLabelsService {
    @Resource
    private StoreLabelsDao storeLabelsDao;

    /**
     * 根据商店id查询对应的商店标签
     * @param storeId
     * @return
     */
    @Override
    public List<StoreLabels> selectLabelByStoreId(Integer storeId) {
        return storeLabelsDao.selectLabelByStoreId(storeId);
    }
}

