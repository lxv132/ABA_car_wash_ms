package com.aaa.front.controller;


import com.aaa.front.entity.TbCommodity;
import com.aaa.front.entity.TbStation;
import com.aaa.front.service.TbCommodityService;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ fileName:  TbCommodityController
 * @ description: 商品表
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:32
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbCommodity")
public class TbCommodityController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbCommodityService tbCommodityService;


    /**
     * 查看所有兑换券
     * @return
     */
    @GetMapping("queryAllTbCommodity")
    public Result queryAllTbCommodity(Integer producttypeid){
        return success(this.tbCommodityService.queryAllTbCommodity(producttypeid));
    }

    /**
     * 根据商品id查询商品
     * @param commodityId
     * @return 所有数据
     */
    @GetMapping("queryAllTbCommodityId")
    public Result queryAllTbCommodityId(@RequestParam  Integer commodityId) {
        return success(this.tbCommodityService.queryAllTbCommodityId(commodityId));
    }

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbCommodity> page) {
        return success(this.tbCommodityService.queryPage(page));
    }

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryAllUnion")
    public Result queryAllUnion(@RequestBody PagePlugin<TbCommodity> page) {
        return success(this.tbCommodityService.queryAllUnion(page));
    }


    /**
     * 前端商品商城商品 按照商品类型查询
     * @param product
     * @return
     */
    @PostMapping("frontMultiTableJointCheck")
    public Result frontMultiTableJointCheck(@RequestBody TbCommodity product) {
        return success(this.tbCommodityService.frontMultiTableJointCheck(product));
    }


    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryAboutCommodity")
    public Result queryAboutCommodity(@RequestBody PagePlugin<TbCommodity> page) {
        return success(this.tbCommodityService.queryAboutCommodity(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbCommodityService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbCommodityService.getById(id));
    }

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryAllStore")
    public Result queryAllStore(@RequestBody PagePlugin<TbCommodity> page) {
        return success(this.tbCommodityService.queryAllStore(page));
    }




    /**
     * 新增数据
     *
     * @param tbCommodity 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbCommodity tbCommodity,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        tbCommodity.setCreateBy(loginName);
        tbCommodity.setCreateTime(new Date().toLocaleString());
        return success(this.tbCommodityService.save(tbCommodity));
    }

    /**
     * 修改数据
     *
     * @param tbCommodity 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbCommodity tbCommodity,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        tbCommodity.setUpdateBy(loginName);
        tbCommodity.setUpdateTime(new Date().toLocaleString());
        return success(this.tbCommodityService.updateById(tbCommodity));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
        @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbCommodityService.removeById(id));
    }
}

