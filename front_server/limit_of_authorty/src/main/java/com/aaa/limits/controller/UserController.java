package com.aaa.limits.controller;
import cn.hutool.core.date.DateUtil;
import com.aaa.common.util.BaseController;
import com.aaa.common.util.ConstUtil;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import com.aaa.limits.entity.User;
import com.aaa.limits.service.CommonFileHandlerService;
import com.aaa.limits.service.UserService;
import com.aaa.limits.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import java.util.Map;
import java.util.UUID;
/**
 * @ fileName:  UserController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:40
 * @ version:1.0.0
 */
@RestController
@RequestMapping("user")
public class UserController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;
    @Resource
    private CommonFileHandlerService commonFileHandlerService;
    /**
     * 文件上传对象  必须使用@RequestParam绑定上传对象,不可以少，少了上传不了
     * MultipartFile
     * @param file1
     * @return
     */
    @PostMapping("uploadFile")
    public Result uploadFile(@RequestParam MultipartFile file1){
        //调用封装的服务接口
        return success(commonFileHandlerService.uploadFile(file1,"two/pic/"));
    }
    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<User> page) {
        return success(this.userService.page(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.userService.list());
    }

    /**
     * 通过主键查询单条数据
     * @param token
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("userId");
        String userId = login.toString().replace("\"", "");
        return success(this.userService.getById(Integer.valueOf(userId)));
    }

    /**
     * 按名称查询  登录使用
     * @param userName
     * @return
     */
    @GetMapping("queryByUserName")
    public User queryByUserName(String userName){
        return this.userService.queryByUserName(userName);
    }


    /**
     * 验证老密码是否成功
     * @param password
     * @param token
     * @return
     */
    @GetMapping("/queryByPassword")
    public Result queryByPassword(String password,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("userId");
        String userId = login.toString().replace("\"", "");
        User user = this.userService.getById(Integer.valueOf(userId));
        // 再次使用密码和盐值进行加密运算
        Sha512Hash sha512Hash = new Sha512Hash(password, user.getSalt(),
                ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        if (user.getPassword().equals(sha512Hash.toString())) {
            return success(200);
        } else {
            return success(500);
        }
    }
    /**
     * 修改密码
     * @param pass
     * @param token
     * @return
     */
    @PutMapping("alterPassword")
    public Result alterPassword(String pass,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim userId = stringClaimMap.get("userId");
        String userId1 = userId.toString().replace("\"", "");
        User user = this.userService.getById(Integer.valueOf(userId1));
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");

        user.setCreateName(loginName);

        user.setUserDate(DateUtil.now());
        //随机盐值
        String randomSalt = UUID.randomUUID().toString();
        //设值盐值
        user.setSalt(randomSalt);
        //设值新密码
        Sha512Hash newPassword = new Sha512Hash(pass, randomSalt, ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        //添加到数据库
        user.setPassword(newPassword.toString());
        user.setStatus("0");
        user.setDelFlag("0");
        return success(this.userService.updateById(user));
    }

    /**
     * 新增数据
     * @param user 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody User user,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        user.setCreateName(loginName);
        user.setUserDate(DateUtil.now());
        //随机盐值
        String randomSalt = UUID.randomUUID().toString();
        //设值盐值
        user.setSalt(randomSalt);
        //设值新密码
        Sha512Hash newPassword = new Sha512Hash("123456", randomSalt, ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        //添加到数据库
        user.setPassword(newPassword.toString());
        user.setStatus("0");
        user.setDelFlag("0");
        //必须登录后才可以使用
//        User user1 = (User) session.getAttribute("userInfo");
//        user.setCreateBy(user1.getUserName());
//        user.setUpdateBy(user1.getUserName());
        return success(this.userService.save(user));
    }

    /**
     * 修改数据
     * @param user 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody User user,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        user.setCreateName(loginName);
        user.setUserDate(DateUtil.now());
        user.setStatus("0");
        user.setDelFlag("0");
        return success(this.userService.updateById(user));
    }

    /**
     * 重置密码
     * @param user 实体对象
     * @return 修改结果
     */
    @PostMapping("resetting")
    public Result resetting(@RequestBody User user,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        user.setCreateName(loginName);
        user.setUserDate(DateUtil.now());
        //随机盐值
        String randomSalt = UUID.randomUUID().toString();
        //设值盐值
        user.setSalt(randomSalt);
        //设值新密码
        Sha512Hash newPassword = new Sha512Hash("123456", randomSalt, ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        //添加到数据库
        user.setPassword(newPassword.toString());
        user.setStatus("0");
        user.setDelFlag("0");
        return success(this.userService.updateById(user));
    }
    /**
     * 账号下线（是否停用）
     * @param user 实体对象
     * @return 修改结果
     */
    @PostMapping("Offline")
    public Result Offline(@RequestBody User user,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        user.setCreateName(loginName);
        user.setUserDate(DateUtil.now());
        //获取原始密码
        String oldPassword = user.getPassword();

        //随机盐值
        String randomSalt = UUID.randomUUID().toString();
        //设值盐值
        user.setSalt(randomSalt);
        //设值新密码
        Sha512Hash newPassword = new Sha512Hash(oldPassword, randomSalt, ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        //添加到数据库
        user.setPassword(newPassword.toString());
        user.setStatus("1");
        user.setDelFlag("0");
        return success(this.userService.updateById(user));
    }
    /**
     * 删除数据
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.userService.removeById(id));
    }
}
