package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbStoreEvaluate
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 14:17:41
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_store_evaluate") //设值数据库表名
public class TbStoreEvaluate extends Model<TbStoreEvaluate> implements Serializable {
    /**
     * 门店评价ID，主键
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "evaluate_id", type = IdType.AUTO)
    private Integer evaluateId;

    /**
     * 外键关联门店表
     */
    private Integer storeId;

    /**
     * 评价人关联用户表
     */
    private String userId;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评价时间
     */
    private Date evaluateTime;

    /**
     * 图片（查询有图评论时，判断该字段是否为空）
     */
    private String picture;

    /**
     * 星级(4，5 为好评 3 中评 2,1 差评)
     */
    private String star;
}

