package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: CreditInfo
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-27 14:29:14
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("credit_info") //设值数据库表名
public class CreditInfo extends Model<CreditInfo> implements Serializable {
    /**
     * 积分ID，主键
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID，外键关联用户表
     */
    private Integer userId;

    /**
     * 积分数值
     */
    private Integer credits;

    /**
     * 积分来源
     */
    private String source;

    /**
     * 积分记录创建时间
     */
    private Date createAt;
}

