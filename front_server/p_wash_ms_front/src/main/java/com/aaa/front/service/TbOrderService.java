package com.aaa.front.service;

import com.aaa.front.entity.TbCommodity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbOrder;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  TbOrderService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:41
 * @ version:1.0.0
 */
public interface TbOrderService extends IService<TbOrder> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbOrder> queryPage(PagePlugin<TbOrder> pagePlugin);

    /**
     * 查询订单状态
     * @param userId
     * @param orderStatusCode
     * @return
     */
    List<TbOrder> queryByIdCode(Integer userId, Integer orderStatusCode);
    /**
     * 权限管理需要的字段
     * @param
     */
    IPage<TbOrder>  queryMune(PagePlugin<TbOrder> pagePlugin);
}

