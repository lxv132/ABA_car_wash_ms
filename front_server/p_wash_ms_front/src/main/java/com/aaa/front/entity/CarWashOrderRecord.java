package com.aaa.front.entity;

import lombok.Data;

/**
 * @ fileName:CarWashOrderRecord
 * @ description: 洗车订单记录表
 * @ author:lx
 * @ createTime:2023/7/10 15:43
 * @ version:1.0.0
 */
@Data
public class CarWashOrderRecord {
    private Integer checkoutId;
    private Integer storeId;
    private String detailId;
    private String storeName;
    private String stationName;
    private String timeDifference;
    private String expenditure;
    private String goodid;//判断是否评价（0评价，1没评价）
}
