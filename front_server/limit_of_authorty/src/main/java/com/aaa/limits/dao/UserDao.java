package com.aaa.limits.dao;

import com.aaa.limits.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName: (User)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:41
 * @ version:1.0.0
 */
public interface UserDao extends BaseMapper<User> {
}

