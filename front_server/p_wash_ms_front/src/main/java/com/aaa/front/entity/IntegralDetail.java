package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: IntegralDetail
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-10 10:11:46
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("integral_detail") // 设值数据库表名
public class IntegralDetail extends Model<IntegralDetail> implements Serializable {
    /**
     * 积分流水id
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 外键，关联用户ID
     */
    private Integer userId;

    /**
     * 外键关联门店id
     */
    private Integer storeId;

    /**
     * 支出积分
     */
    private String expenditure;

    /**
     * 收获积分
     */
    private String accountMoney;

    /**
     * 到账时间
     */
    private String rechargeTime;

    /**
     * 支出时间
     */
    private String expenditureTime;
    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String storeName;
}

