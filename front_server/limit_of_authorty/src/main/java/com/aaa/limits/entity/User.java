package com.aaa.limits.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 *@ fileName: User
 *@ description:
 *@ fileName: lx
 *@ createTime: 2023-06-21 11:23:15
 *@ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("sys_user") //设值数据库表名
public class User extends Model<User> implements Serializable {
       /**用户编号
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "user_id",type = IdType.AUTO)
    private Integer userId;

    /** 用户头像 */
    private String avatar;

    /** 登录账号 */
    private String loginName;

    /** 用户名 */
    private String userName;

    /** 手机号码 */
    private String phonenumber;

    /** 密码 */
    private String password;

    /** 盐加密 */
    private String salt;

    /** serviceTotal */
    private Integer storeTotal;

    /** 服务次数 */
    private Integer serviceTotal;

    /** 创建人 */
    private String createName;

    /** 创建时间 */
    private String userDate;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 注册是否通过(0未审核,1通过，2未通过) */
    private Integer passThrough;
}

