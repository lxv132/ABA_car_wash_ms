package com.aaa.front.controller;


import com.aaa.front.entity.UserAndStore;
import com.aaa.front.service.UserAndStoreService;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.Map;

/**
 * @ fileName:  UserAndStoreController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-06 11:01:53
 * @ version:1.0.0
 */

@RestController
@RequestMapping("userAndStore")
public class UserAndStoreController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private UserAndStoreService userAndStoreService;

    /**
     * 根据用户名和店铺名查询
     * @param storeId
     * @param token
     * @return
     */
    @GetMapping("selectByStoreIdAndUserId")
    public Result selectByStoreIdAndUserId(@RequestParam Integer storeId,String token){
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String userId = login.toString().replace("\"", "");
        return success(userAndStoreService.selectByStoreIdAndUserId(Integer.valueOf(userId),storeId));

    }



    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<UserAndStore> page) {
        return success(this.userAndStoreService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.userAndStoreService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.userAndStoreService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param storeId 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestParam Integer storeId,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String userId = login.toString().replace("\"", "");
        UserAndStore userAndStore = new UserAndStore(Integer.valueOf(userId),storeId);
        return success(this.userAndStoreService.save(userAndStore));
    }

    /**
     * 修改数据
     *
     * @param userAndStore 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody UserAndStore userAndStore) {
        return success(this.userAndStoreService.updateById(userAndStore));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.userAndStoreService.removeById(id));
    }
}

