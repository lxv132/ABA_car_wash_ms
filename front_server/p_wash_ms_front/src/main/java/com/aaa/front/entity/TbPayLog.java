package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbPayLog
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:54
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_pay_log") //设值数据库表名
public class TbPayLog extends Model<TbPayLog> implements Serializable {
    /**
     * 订单id
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "orderid", type = IdType.AUTO)
    private Integer orderid;

    /**
     * 订单日志id
     */
    private Integer payLogId;

    /**
     * 支付状态码
     */
    private Integer payStatusCode;

    /**
     * 支付状态
     */
    private Integer payStatusName;

    /**
     * 操作时间
     */
    private Date operationTime;

    /**
     * 操作人
     */
    private String operationId;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 支付状态主键id
     */
    private Integer payStatusId;
}

