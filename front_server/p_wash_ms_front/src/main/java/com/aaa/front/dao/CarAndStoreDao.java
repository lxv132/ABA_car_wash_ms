package com.aaa.front.dao;

import com.aaa.front.entity.CarAndStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @ fileName: (CarAndStore)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 15:56:55
 * @ version:1.0.0
 */
public interface CarAndStoreDao extends BaseMapper<CarAndStore> {
}

