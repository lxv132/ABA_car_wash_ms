package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: CommodityType
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:27
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("commodity_type") //设值数据库表名
public class CommodityType extends Model<CommodityType> implements Serializable {
    /**
     * 商品类型id，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Integer commentId;

    /**
     * 商品类型
     */
    private String name;

    /**
     * 是否有效(0有效/1有效)
     */
    private String isvalid;

    /**
     * 是否允许修改(0有效/1有效)
     */
    private String isedit;

    /**
     * 商品类型描述
     */
    private String description;
}

