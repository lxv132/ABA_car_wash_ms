package com.aaa.front.controller;


import com.aaa.front.entity.*;
import com.aaa.front.service.*;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.xml.crypto.Data;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ fileName:  CheckoutInfoController
 * @ description: 洗车记录
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:26
 * @ version:1.0.0
 */

@RestController
@RequestMapping("checkoutInfo")
public class CheckoutInfoController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private CheckoutInfoService checkoutInfoService;

    /**
     * 车库状态表
     */
    @Resource
    private TbStationController tbStationController;

    @Resource
    private TbStationService tbStationService;


    /**
     * 余额表
     */
    @Resource
    private BalanceService balanceService;

    /**
     * 余额详情
     */
    @Resource
    private UserDetailService userDetailService;

    /**
     * 积分
     */
    @Resource
    private IntegralController integralController;
    /**
     * 积分详情
     */
    @Resource
    private IntegralDetailService integralDetailService;

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<CheckoutInfo> page) {
        return success(this.checkoutInfoService.queryPage(page));
    }

    /**
     * 权限管理需要的字段
     * @param page
     * @return
     */
    @PostMapping("queryMune")
    public Result queryMune(@RequestBody PagePlugin<CheckoutInfo> page){
        return success(checkoutInfoService.queryMune(page));
    }


    /**
     * 根据用户id查询信息
     */
    @GetMapping("/queryUser")
    public Result queryUser(String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String userId = login.toString().replace("\"", "");
        return success(this.checkoutInfoService.queryUser(Integer.valueOf(userId)));
    }

    /**
     * 根据车牌号查询信息
     */
    @GetMapping("/queryCarNo")
    public Result queryCarNo(@RequestParam String carNo) {
        return success(this.checkoutInfoService.queryCarNo(carNo));
    }


    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.checkoutInfoService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.checkoutInfoService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param checkoutInfo 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody CheckoutInfo checkoutInfo) {
        return success(this.checkoutInfoService.save(checkoutInfo));
    }




    /**
     * 洗车订单信息
     * @param date
     * @return
     */
    @GetMapping("queryOrder")
    public Result queryOrder(@RequestParam  String date, @RequestParam Integer userId){
        return success(checkoutInfoService.queryOrder(date, userId));
    }

    /**
     * 向洗车表中添加数据
     *
     * @param
     * @return 新增结果
     */
    @PostMapping("addCar")
    @Transactional // 开启事务
    public Result addCar(@RequestParam Integer userId, @RequestParam Integer carId,
                         @RequestParam Integer storeId, @RequestParam String stationName,@RequestParam Integer stationId) {
        CheckoutInfo checkoutInfo = new CheckoutInfo();
        checkoutInfo.setStartTime(new Date());
        checkoutInfo.setUserId(userId);
        checkoutInfo.setCarId(carId);
        checkoutInfo.setStoreId(storeId);
        checkoutInfo.setStationId(stationId);
        boolean save = this.checkoutInfoService.save(checkoutInfo);


        List<TbStation> tbStations = tbStationController.queryStoreName(storeId, stationName);
        TbStation tbStation = tbStations.get(0);
        tbStation.setIdle(1);
        tbStationController.update(tbStation);


        return success(save);
    }



    /**
     * 结算完成更新数据
     *
     * @param
     * @return 新增结果
     */
    @PostMapping("chargin")
    @Transactional // 开启事务
    public Result chargin(@RequestParam Integer balanceId, @RequestParam double amount, @RequestParam Double totalPrice
            , @RequestParam Integer userId, @RequestParam Integer storeId,
                          @RequestParam Integer stationId, @RequestParam Integer checkoutId) {
        CheckoutInfo checkoutInfo = checkoutInfoService.getById(checkoutId);
        checkoutInfo.setTotalPrice(totalPrice);
        checkoutInfo.setEndTime(new Date());
        checkoutInfo.setComplete("1");
        checkoutInfo.setGoodid("1");
        boolean cisb = checkoutInfoService.updateById(checkoutInfo);

        TbStation tbStation = tbStationService.getById(stationId);
        tbStation.setIdle(0);
        tbStationService.updateById(tbStation);


        Balance balance = balanceService.getById(balanceId);
        balance.setAmount(amount);
        balanceService.updateById(balance);



        UserDetail userDetail = new UserDetail();
        userDetail.setDetailId("car" + new Date().getTime());
        userDetail.setUserId(userId);
        userDetail.setStoreId(storeId);
        userDetail.setExpenditureTime(new Date().toLocaleString());
        userDetail.setCheckoutId(checkoutId);
        System.out.println(userDetail);
        String aaa =totalPrice+"";
        userDetail.setExpenditure(aaa);
        userDetailService.save(userDetail);

        IntegralDetail integralDetail = new IntegralDetail();
        integralDetail.setUserId(userId);
        integralDetail.setStoreId(storeId);
        integralDetail.setAccountMoney(totalPrice+"");
        integralDetail.setRechargeTime(new Date().toLocaleString());
        integralDetailService.save(integralDetail);

        Integral integral =(Integral) integralController.queryInteg(userId, storeId).getData();
        System.out.println("integral = " + integral);
        String substring = totalPrice.toString().substring(0, totalPrice.toString().length() - 2);
        integral.setCredits(Integer.valueOf(substring)+integral.getCredits());
        integral.setSource("1");
        integralController.update(integral);
        return success(cisb);
    }


    /**
     * 修改数据
     *
     * @param checkoutInfo 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody CheckoutInfo checkoutInfo) {
        return success(this.checkoutInfoService.updateById(checkoutInfo));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.checkoutInfoService.removeById(id));
    }


    /**
     * 统计图 查询所有门店的洗车记录
     *
     * @param
     * @return 新增结果
     */
    @GetMapping("/statistics")
    public List<CheckoutStatistics> getCheckoutStatistics(@RequestParam String date) {
        return checkoutInfoService.getDailyCheckoutStatistics(date);
    }

    /**
     * 统计图 查询所有门店的洗车记录
     *
     * @param
     * @return 新增结果
     */
    @GetMapping("/getCheckoutStatisticsId")
    public List<CheckoutStatistics> getDailyCheckoutStatisticsId(@RequestParam Integer storeId) {
        return checkoutInfoService.getDailyCheckoutStatisticsId(storeId);
    }
    /**
     * 今天洗车订单
     * @return
     */
    @GetMapping("/queryOrderTime")
    public Result queryOrderTime(){
        return success(checkoutInfoService.queryOrderTime());
    }

    /**
     * 洗车订单金额
     * @return
     */
    @PostMapping("/queryOrderDay")
    public Result queryOrderDay(){
        return success(checkoutInfoService.queryOrderDay());
    }
    @PostMapping("/queryOrderTotal")
    public Result queryOrderTotal(){
        return success(checkoutInfoService.queryOrderTotal());
    }
}

