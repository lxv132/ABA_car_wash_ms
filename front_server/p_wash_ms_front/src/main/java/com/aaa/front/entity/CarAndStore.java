package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * @ fileName: CarAndStore
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 15:56:56
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("car_and_store") // 设值数据库表名
public class CarAndStore extends Model<CarAndStore> implements Serializable {

    /**
     * 车辆id
     */
    private Integer carId;

    /**
     * 门店id
     */
    private Integer storeId;
}

