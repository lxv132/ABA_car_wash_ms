package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.GoodsImage;
import com.aaa.front.entity.TbStation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

/**
 * @ fileName: 商品图片
 * (GoodsImage)表控制层
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:47
 * @ version:1.0.0
 */
public interface GoodsImageDao extends BaseMapper<GoodsImage> {

    /**
     * 根域商品标号查询商品图片
     * @param page
     * @param pictureid
     * @return
     * @param <E>
     */

    // IPage<TbStation> (IPage<TbStation> page, @Param(Constants.WRAPPER) Wrapper<TbStation> queryWrapper);
    <E extends IPage<GoodsImage>> E queryAllStore(E page, int pictureid);

}

