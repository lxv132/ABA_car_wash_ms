package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.UserDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.IntegralDetail;

/**
 * @ fileName: 积分明细(IntegralDetail)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-10 10:11:45
 * @ version:1.0.0
 */
public interface IntegralDetailDao extends BaseMapper<IntegralDetail> {

    /**
     * 根据用户id和门店id查询余额的详细信息
     * @param userId
     * @param storeId
     * @return
     */
    List<UserDetail> queryUserStore(@Param("userId") Integer userId, @Param("storeId") Integer storeId);

}

