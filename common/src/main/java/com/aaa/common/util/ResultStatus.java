package com.aaa.common.util;

/**
 * @ fileName:ResultStatus
 * @ description:定义统一返回枚举(常量)
 * @ author:lx
 * @ createTime:2023/5/4 9:01
 * @ version:1.0.0
 */

public enum ResultStatus {
    //成功
    SUCCESS(200,"执行成功"),
    //成功
    ERROR(500,"执行失败"),
    //还可以自定义任何统一返回码和提示信息
    //定义参数不能为null的枚举
    PARAM_NOT_EMPTY(5001,"参数不能为空!"),
    REMOTE_METHOD_LIMIT(5002,"远程请求资源被限流!"),
    REMOTE_METHOD_ERROR(5003,"远程请求资源出现异常!"),
    REMOTE_METH_ERROR(5004,"远程请求资源已关闭!"),
    ILLEGAL_CHAR_ERROR(5005,"非法参数异常!"),
    ILLEGAL_IP_ERROR(5006,"非法IP地址!"),
    NO_AUTH_ERROR(5007,"未认证异常!"),
    NO_AUTH_guoqi(5008,"账号失效异常!"),
    ;

    private int returnCode;
    private String returnMes;
    /**
     * 带参构造
     * @param returnCode
     * @param returnMes
     */
    ResultStatus(int returnCode, String returnMes) {
        this.returnCode = returnCode;
        this.returnMes = returnMes;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMes() {
        return returnMes;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMes= returnMessage;
    }
}
