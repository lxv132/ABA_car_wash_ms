package com.aaa.front.service;

import com.aaa.front.entity.TbStation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbCommodity;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  TbCommodityService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:32
 * @ version:1.0.0
 */
public interface TbCommodityService extends IService<TbCommodity> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbCommodity> queryPage(PagePlugin<TbCommodity> pagePlugin);

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbCommodity> queryAllStore(PagePlugin<TbCommodity> pagePlugin);
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbCommodity> queryAllUnion(PagePlugin<TbCommodity> pagePlugin);

    List<TbCommodity> queryAllTbCommodity(Integer producttypeid);


    IPage<TbCommodity> queryAboutCommodity(PagePlugin<TbCommodity> pagePlugin);
    /**
     * 前端商品显示多表联查商品类型
     * @param product
     * @return
     */
    List<TbCommodity> frontMultiTableJointCheck(TbCommodity product);

    /**
     * 根据商品id查询商品
     * @param commodityId
     * @return
     */
    TbCommodity queryAllTbCommodityId(@Param("commodityId") Integer commodityId);
}

