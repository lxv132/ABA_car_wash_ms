package com.aaa.front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.TbOrderDao;
import com.aaa.front.entity.TbOrder;
import com.aaa.front.service.TbOrderService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  TbOrderServiceImpl
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:42
 * @ version:1.0.0
 */
@Service("tbOrderService")
public class TbOrderServiceImpl extends ServiceImpl<TbOrderDao, TbOrder> implements TbOrderService {

    /**
     * 服务对象
     */
    @Resource
    private TbOrderDao tbOrderDao;


    /**
     * 分页查询
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<TbOrder> queryPage(PagePlugin<TbOrder> pagePlugin) {
        // 分页参数设置
        Page<TbOrder> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
             queryWrapper.like(null != pagePlugin.getData().getOrderid(), "orderid", pagePlugin.getData().getOrderid());

        }
        return this.tbOrderDao.selectPage(page, queryWrapper);
    }

    @Override
    public List<TbOrder> queryByIdCode(Integer userId, Integer orderStatusCode) {
        return this.tbOrderDao.queryByIdCode(userId, orderStatusCode);
    }

    @Override
    public IPage<TbOrder> queryMune(PagePlugin<TbOrder> pagePlugin) {
        // 分页参数设置
        Page<TbOrder> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        TbOrder tbOrder = pagePlugin.getData();
        Integer orderStatusCode = tbOrder.getOrderStatusCode();
        String tcount = tbOrder.getTcount();
        return tbOrderDao.queryMune(page, orderStatusCode, tcount);
    }
}

