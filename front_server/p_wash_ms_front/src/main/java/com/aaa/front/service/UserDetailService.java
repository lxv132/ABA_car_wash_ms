package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.UserDetail;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  UserDetailService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-05 09:07:59
 * @ version:1.0.0
 */
public interface UserDetailService extends IService<UserDetail> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<UserDetail> queryPage(PagePlugin<UserDetail> pagePlugin);
    /**
     * 根据用户id和门店id查询余额的详细信息
     * @param userId
     * @param storeId
     * @return
     */
    List<UserDetail> queryUserStore(@Param("userId") Integer userId, @Param("storeId") Integer storeId);

}

