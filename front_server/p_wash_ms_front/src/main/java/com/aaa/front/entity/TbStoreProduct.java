package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbStoreProduct
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 14:18:07
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_store_product") //设值数据库表名
public class TbStoreProduct extends Model<TbStoreProduct> implements Serializable {

    /**
     * ${column.comment}
     */
    private Integer storeId;

    /**
     * ${column.comment}
     */
    private Integer productId;

    /**
     * ${column.comment}
     */
    private Double price;

    /**
     * ${column.comment}
     */
    private Integer stock;
}

