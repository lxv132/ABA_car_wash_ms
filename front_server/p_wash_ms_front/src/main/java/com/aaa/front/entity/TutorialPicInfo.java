package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TutorialPicInfo
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:20:06
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tutorial_pic_info") //设值数据库表名
public class TutorialPicInfo extends Model<TutorialPicInfo> implements Serializable {
    /**
     * 教程图片ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "tutorial_pic_id", type = IdType.AUTO)
    private Integer tutorialPicId;

    /**
     * 教程ID，用于关联教程信息
     */
    private Integer tutorialId;

    /**
     * 教程图片链接
     */
    private String picUrl;

    /**
     * 图片在教程中的排序序号
     */
    private Integer orderNum;

    /**
     * 记录创建时间
     */
    private Date createTime;
}

