package com.aaa.limits.service;

import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.limits.entity.User;
/**
 * @ fileName:  UserService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:41
 * @ version:1.0.0
 */
public interface UserService extends IService<User> {

    IPage<User> page(PagePlugin<User> pagePlugin);

    User queryByUserName(String userName);
}

