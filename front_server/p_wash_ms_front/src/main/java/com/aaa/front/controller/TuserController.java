package com.aaa.front.controller;


import cn.hutool.core.date.DateUtil;
import com.aaa.common.util.BaseController;
import com.aaa.common.util.ConstUtil;
import com.aaa.common.util.Result;
import com.aaa.common.util.SmsUtil;
import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.Balance;
import com.aaa.front.entity.Integral;
import com.aaa.front.entity.Tuser;
import com.aaa.front.service.BalanceService;
import com.aaa.front.service.CommonFileHandlerService;
import com.aaa.front.service.IntegralService;
import com.aaa.front.service.TuserService;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * @ fileName:  TuserController
 * @ description: 前端用户登录
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-25 09:52:43
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tuser")
public class TuserController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TuserService tuserService;
    /**
     * 连接虚拟机
     */
    private Jedis jedis = new Jedis("127.0.0.1", 6379);
    /**
     * 依赖注入  图片上传
     */
    @Resource
    private CommonFileHandlerService commonFileHandlerService;

    /**
     * 文件上传对象  必须使用@RequestParam绑定上传对象,不可以少，少了上传不了
     * MultipartFile
     *
     * @param file1
     * @return
     */
    @PostMapping("uploadFile")
    public Result uploadFile(@RequestParam MultipartFile file1) {
        // 调用封装的服务接口
        return success(commonFileHandlerService.uploadFile(file1, "two/qianduan/"));
    }

    /**
     * 查询洗车详情信息
     * @param userId
     * @return
     */
    @GetMapping("queryUserWash")
    public Result queryUserWash(@RequestParam Integer userId){
        return success(tuserService.queryUserWash(userId));
    }

    /**
     * 验证验证码是否正确
     *
     * @param number
     * @return
     */
    @PostMapping("yanzhen")
    public Result check(String number) {
        // 获取存储到redis中验证码
        String number1 = jedis.get("verifyCode");
        // 比较是否相等
        if (number.equals(number1)) {
            jedis.del("verifyCode");
            return success(200);
        } else {
            return error(500);
        }
    }

    /**
     * 发送验证码存到Redis
     *
     * @param mobile
     * @return
     */
    @PostMapping("createCode")
    public Result mobile(String mobile) {
        // 生成验证码
        String verifyCode = SmsUtil.createVerifyCode();
//        System.out.println("verifyCode = " + verifyCode);
        // 发送验证码
        String sms = SmsUtil.sendSms(mobile, verifyCode);
        //设置超时时间
        jedis.setex("verifyCode", 300, verifyCode);
        return success(sms);
    }

    /**
     * 按手机号查询  登录使用
     *
     * @param mobile
     * @return
     */
    @GetMapping("queryByMobileLogin")
    public Tuser queryByMobile(@RequestParam("mobile") String mobile) {
        return this.tuserService.queryByMobile(mobile);
    }

    /**
     * @param mobile
     * @return
     */
    @GetMapping("queryByMobile")
    public Result queryByUserName(String mobile) {
        Tuser tuser = this.tuserService.queryByMobile(mobile);
        if (null == tuser) {
            return error(500);
        }
        return success(200);
    }
    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Tuser> page) {
        return success(this.tuserService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tuserService.list());
    }


    /**
     * 通过主键查询单条数据
     * @return 单条数据
     */
    @GetMapping("/queryBy")
    public Result queryBy(String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String id = login.toString().replace("\"", "");
        return success(this.tuserService.getById(Integer.valueOf(id)));
    }



    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tuserService.getById(id));
    }

    /**
     * 修改密码
     *
     * @return 新增结果
     */
    @PostMapping("ChangePassword")
    public Result ChangePassword(String mobile,String password) {
        Tuser tuser = this.tuserService.queryByMobile(mobile);
        tuser.setGxtime(DateUtil.now());
        System.out.println("tuser = " + tuser);
        Sha512Hash newPassword = new Sha512Hash(password, tuser.getSalt(), ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        tuser.setPassword(newPassword.toString());
        tuser.setState("0");
        tuser.setAccount(mobile);
        return success(this.tuserService.updateById(tuser));
    }


    /**
     * 新增数据
     *
     * @param tuser 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody Tuser tuser) {
        tuser.setCjtime(DateUtil.now());
        String mobile = tuser.getMobile();
        String randomSalt = UUID.randomUUID().toString();
        //默认头像
        tuser.setAvatar("https://flyskyfly.oss-cn-beijing.aliyuncs.com/two/pic/a1a9b85d-1e50-4bf1-9908-04fea46bb9fe.jpg");
        tuser.setSalt(randomSalt);
        String password = tuser.getPassword();
        Sha512Hash newPassword = new Sha512Hash(password, randomSalt, ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        tuser.setPassword(newPassword.toString());
        tuser.setState("0");
        tuser.setAccount(mobile);

        boolean save = this.tuserService.save(tuser);


        return success(save);
    }

    /**
     * 修改数据
     *
     * @param tuser 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody Tuser tuser) {
        tuser.setGxtime(DateUtil.now());
        return success(this.tuserService.updateById(tuser));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tuserService.removeById(id));
    }

    /**
     * 每日，每周，每月，每年新增及总数
     */
    @GetMapping("annualQuery")
    public Result annualQuery(Tuser tuser){
        return success(tuserService.annualQuery(tuser));
    }
}

