package com.aaa.limits.service;

import com.aaa.limits.entity.Role;
import com.aaa.limits.entity.UserAndRole;


import java.util.List;

public interface UserAndRoleService {
    //添加
    int add(UserAndRole userAndRole);

    //根据id查询拥有的角色
    List<Role> queryById(Integer userId);
}
