package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbStoreAddress;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbStoreAddressService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:24
 * @ version:1.0.0
 */
public interface TbStoreAddressService extends IService<TbStoreAddress> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbStoreAddress> queryPage(PagePlugin<TbStoreAddress> pagePlugin);
}

