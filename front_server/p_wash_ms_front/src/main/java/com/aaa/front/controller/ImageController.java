package com.aaa.front.controller;

import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.front.service.CommonFileHandlerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @ fileName:ImageController
 * @ description:
 * @ author:lx
 * @ createTime:2023/7/5 21:16
 * @ version:1.0.0
 */
@RestController
@RequestMapping("upImage")
public class ImageController extends BaseController {
    @Resource
    private CommonFileHandlerService commonFileHandlerService;
    /**
     * 文件上传对象  必须使用@RequestParam绑定上传对象,不可以少，少了上传不了
     * MultipartFile
     * @param file1
     * @return
     */
    @PostMapping("uploadFile")
    public Result uploadFile(@RequestParam MultipartFile file1){
        //调用封装的服务接口
        return success(commonFileHandlerService.uploadFile(file1,"two/pic/"));
    }
}
