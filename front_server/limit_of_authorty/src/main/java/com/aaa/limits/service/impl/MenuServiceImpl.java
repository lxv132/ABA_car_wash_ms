package com.aaa.limits.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.common.vo.TreeNode;
import com.aaa.limits.dao.MenuDao;
import com.aaa.limits.entity.Menu;
import com.aaa.limits.service.MenuService;
import com.aaa.limits.util.JwtUtils;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ fileName:  MenuServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:31:51
 * @ version:1.0.0
 */
@Service("menuService")
public class MenuServiceImpl extends ServiceImpl<MenuDao, Menu> implements MenuService {

    /**
     * 服务对象
     */
    @Resource
    private MenuDao menuDao;

    /**
     * 添加菜单节点
     * @param treeNode
     * @return
     */
    @Override
    public int add(TreeNode treeNode) {
        return menuDao.add(treeNode);
    }

    /**
     * 更细菜单节点信息
     * @param treeNode
     * @return
     */
    @Override
    public int update(TreeNode treeNode) {
        return menuDao.update(treeNode);
    }

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<Menu> queryPage(PagePlugin<Menu> pagePlugin) {
        // 分页参数设置
        IPage<Menu> page = new Page<Menu>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        IPage iPage = this.menuDao.selectPage(page, queryWrapper);
        iPage.setTotal(iPage.getSize()/ pagePlugin.getPageSize());
        return iPage;
    }

    /**
     * 根据用户编号查询该用户拥有的按钮权限
     * @param userId
     * @return
     */
    @Override
    public List<String> queryButtonPermsByUserId(Integer userId) {
        return menuDao.queryButtonPermsByUserId(userId);
    }

    /**
     * 根据编号查询数据
     * @return
     */
    @Override
    public List<TreeNode> queryTreeNodeByUserId(String token) {
        String tokenSub = token.substring(token.indexOf("-") + 1);
        Map<String, Claim> payloadByToken = JwtUtils.getPayloadByToken(tokenSub);
        Claim userId1 = payloadByToken.get("userId");
        //userId="1"
        String userId = userId1.toString().replace("\"", "");
        //查询所有数据
        List<TreeNode> treeNodeList = menuDao.queryTreeNodeByUserId(Integer.valueOf(userId));
        //定义返回数据列表 拼装成前端要的格式
        List<TreeNode> returnTreeNodeList = new ArrayList<>();
        //循环所有数据
        for (TreeNode treeNode : treeNodeList) {
            //判断是否是根节点，如果是直接添加到返回集合
            if(treeNode.getParentId()==0){
                //一级节点添加返回集合
                returnTreeNodeList.add(treeNode);
                //绑定孩子节点
                bindChildren(treeNode,treeNodeList);
            }
        }
        return returnTreeNodeList;

    }

    @Override
    public List<TreeNode> queryTreeData() {
        List<TreeNode> treeNodes = menuDao.queryByAllMenu();
        // 定义返回对象
        List<TreeNode> treeNodeArrayList = new ArrayList<>();
        // 判断集合是否为空
        //if(treeNodeList!=null&&treeNodeList.size()>0){
        if (!CollectionUtils.isEmpty(treeNodes)) {
            // 循环遍历，查找一级节点
            for (TreeNode treeNode : treeNodes) {
                //父节点为0，都是根节点
                if (treeNode.getParentId() == 0) {
                    // 加入返回集合
                    treeNodeArrayList.add(treeNode);
                    //为1级节点找子节点
                    bindChildren(treeNode,treeNodes);
                }
            }
        }
        return treeNodeArrayList;
    }



    /**
     * 从所有节点中查找当前节点的孩子
     * @param currenTreeNode
     * @param treeNodeList
     */
    private void bindChildren(TreeNode currenTreeNode, List<TreeNode> treeNodeList) {
        // 循环
        for (TreeNode treeNode : treeNodeList) {
            // 判断当前循环节点是否是当前节点的孩子
            if (treeNode.getParentId() == currenTreeNode.getId()) {
                // 获取当前节点的所有孩子
                List<TreeNode> child = currenTreeNode.getChildren();
                // 判断孩子是否为空
                if (child == null) {
                    // 实例化
                    child = new ArrayList<>();
                }
                // 添加当前节点到孩子集合
                child.add(treeNode);
                // 和当前对象进行绑定
                currenTreeNode.setChildren(child);
                // 递归 调用自己为当前节点绑定孩子
                bindChildren(treeNode, treeNodeList);
            }
        }
    }
}

