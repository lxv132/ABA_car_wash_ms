package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.Balance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.UserDetail;

/**
 * @ fileName: 余额详细(UserDetail)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-05 09:07:58
 * @ version:1.0.0
 */
public interface UserDetailDao extends BaseMapper<UserDetail> {

    /**
     * 根据用户id和门店id查询余额的详细信息
     * @param userId
     * @param storeId
     * @return
     */
    List<UserDetail> queryUserStore(@Param("userId") Integer userId, @Param("storeId") Integer storeId);

}

