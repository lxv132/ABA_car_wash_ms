package com.aaa.front.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.dao.AddressDao;
import com.aaa.front.entity.Address;
import com.aaa.front.service.AddressService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ fileName:  AddressServiceImpl
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:14:42
 * @ version:1.0.0
 */
@Service("addressService")
public class AddressServiceImpl extends ServiceImpl<AddressDao, Address> implements AddressService {

    /**
     * 服务对象
     */
    @Resource
    private AddressDao addressDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<Address> queryPage(PagePlugin<Address> pagePlugin) {
        // 分页参数设置
        Page<Address> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
             queryWrapper.like(null != pagePlugin.getData().getAddressId(), "address_id", pagePlugin.getData().getAddressId());

        }
        return this.addressDao.selectPage(page, queryWrapper);
    }

    @Override
    public int updateDefault(Integer addressId) {
        return this.addressDao.updateDefault(addressId);
    }
}

