package com.aaa.limits.controller;


import cn.hutool.core.date.DateUtil;
import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import com.aaa.limits.entity.Role;
import com.aaa.limits.service.RoleService;
import com.aaa.limits.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @ fileName:  RoleController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:40
 * @ version:1.0.0
 */

@RestController
@RequestMapping("role")
public class RoleController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private RoleService roleService;

    /**
     * 查询启用职位(添加员工时,下拉使用)
     * @return
     */
    @GetMapping("query")
    public Result query(){
        return success(roleService.queryAll());
    }

    /**
     * 根据角色ID查询该角色关联的所有权限ID集合
     * @param roleId
     * @return
     */
    @GetMapping("queryMenuIdListByRid")
    public Result queryMenuIdListById(Integer roleId){
        return success(roleService.queryMenuIdListByRid(roleId));
    }

    /**
     * 添加角色权限关联
     * @param roleId
     * @param menuIds
     * @return
     */
    @PostMapping("addPosAndMenu")
    public Result addPosAndMenu(Integer roleId, String menuIds){
        return success(roleService.addPosAndMenu(roleId,menuIds));
    }

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Role> page) {
        return success(this.roleService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.roleService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.roleService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param role 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody Role role,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        role.setCreateBy(loginName);
        role.setCreateTime(DateUtil.now());
        return success(this.roleService.save(role));
    }

    /**
     * 修改数据
     *
     * @param role 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody Role role,String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        role.setUpdateBy(loginName);
        role.setUpdateTime(DateUtil.now());
        return success(this.roleService.updateById(role));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.roleService.removeById(id));
    }
}

