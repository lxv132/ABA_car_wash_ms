package com.aaa.front.service;

import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.entity.Integral;
import com.aaa.front.entity.TbStore;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.Balance;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  BalanceService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:11
 * @ version:1.0.0
 */
public interface BalanceService extends IService<Balance> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<Balance> queryPage(PagePlugin<Balance> pagePlugin);
    /**
     * 根据用户标号查询积分
     * @param userId
     * @return
     */
    Balance queryInteg(Integer userId);

    /**
     * 而根据用户id查询余额，积分，门店名称
     * @param userId
     * @return
     */
    List<Balance> queryAllye(Integer userId);

    /**
     * 根据用户id和门店id查询余额
     * @param userId
     * @param storeId
     * @return
     */
    Balance queryUserAndStore(@Param("userId") Integer userId, @Param("storeId") Integer storeId);
    /**
     * 权限管理需要的字段
     *
     * @param
     */
    IPage queryMune(PagePlugin<Balance> pagePlugin);
}

