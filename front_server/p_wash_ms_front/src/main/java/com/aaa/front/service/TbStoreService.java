package com.aaa.front.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.TbStore;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  TbStoreService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:50:40
 * @ version:1.0.0
 */
public interface TbStoreService extends IService<TbStore> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbStore> queryPage(PagePlugin<TbStore> pagePlugin);

    /**
     * 根据用户编号查询门店
     * @param id
     * @return
     */
    List<TbStore> queryUserStore(Integer id);

    /**
     * 根据用户id和门店id查询店铺信息
     * @param userId
     * @param storeId
     * @return
     */
    TbStore selectStoreByStoreIdAndUserId(@Param("userId") Integer userId, @Param("storeId") Integer storeId);


    /**
     * 求工位数量
     * @param storeId
     * @return
     */
    List queryAllCount(Integer storeId);

    /**
     * 查询店铺信息
     * @return
     */
    List<TbStore> selectStoreByStoreId(TbStore tbStore);

    /**
     * 通过店铺信息查看与之关联的车辆
     * @param carNo
     * @return
     */
    List<TbStore> storeUserCarId(@Param("carNo") String carNo);
}

