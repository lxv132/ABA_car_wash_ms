package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.CommodityType;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  CommodityTypeService
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:27
 * @ version:1.0.0
 */
public interface CommodityTypeService extends IService<CommodityType> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<CommodityType> queryPage(PagePlugin<CommodityType> pagePlugin);

    /**
     * 查询关于商品
     * @return
     */
    List<CommodityType> queryAboutCommodity(Integer commentId);
}

