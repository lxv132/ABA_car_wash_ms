package com.aaa.limits.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.limits.entity.Role;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @ fileName:  RoleService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:40
 * @ version:1.0.0
 */
public interface RoleService extends IService<Role> {

    /**
     * 查询所有启用职位(添加员工时,下拉框添加)
     * @return
     */
    List<Role> queryAll();
    /**
     * 查询角色权限关联表  根据角色ID查询该角色关联的所有权限ID集合
     * @param roleId
     * @return
     */
    List<Integer> queryMenuIdListByRid(int roleId);

    /**
     * 根据角色ID批量删除该角色对应角色权限关联
     * @param roleId
     * @return
     */
    int deletePosMenuByPosId(int roleId);

    /**
     * 添加职位（角色）和权限的关联
     * @param roleId
     * @param menuIds
     * @return
     */
    int addPosAndMenu( int roleId, String menuIds);

    /**
     * 分页带参查询
     * @param pagePlugin
     * @return
     */
    IPage<Role> queryPage(PagePlugin<Role> pagePlugin);
}

