package com.aaa.front.controller;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.Area;
import com.aaa.front.service.AreaService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @ fileName:  AreaController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 17:07:06
 * @ version:1.0.0
 */

@RestController
@RequestMapping("area")
public class AreaController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private AreaService areaService;

    /**
     * 根据地区姓名查询code
     * @param areaName
     * @return
     */
    @GetMapping("queryByAreaName")
    public Result queryByAreaName(String areaName,String city,String province){
        return success(areaService.queryByAreaName(areaName,city,province));
    }

    /**
     * 分页查询所有数据
     *
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Area> page) {
        // 使用加各种参数  判断某字符串是否为空
        return success(this.areaService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.areaService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.areaService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param area 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody Area area) {
        return success(this.areaService.save(area));
    }

    /**
     * 修改数据
     *
     * @param area 实体对象
     * @return 修改结果
     */
    @PutMapping("update")
    public Result update(@RequestBody Area area) {
        return success(this.areaService.updateById(area));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.areaService.removeById(id));
    }
}

