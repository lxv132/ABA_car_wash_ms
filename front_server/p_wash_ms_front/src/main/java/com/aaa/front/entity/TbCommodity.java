package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbCommodity
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:32
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_commodity") //设值数据库表名
public class TbCommodity extends Model<TbCommodity> implements Serializable {
    /**
     * 商品ID，主键
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "commodity_id", type = IdType.AUTO)
    private Integer commodityId;

    /**
     * 外键关联门店
     */
    private Integer storeId;

    /**
     * 外键关联商品分类
     */
    private Integer producttypeid;

    /**
     * 商品名称
     */
    private String commodityName;

    /**
     * 商品图片id
     */
    private Integer pictureid;

    /**
     * 积分
     */
    private String integral;

    /**
     * 价格
     */
    private Double price;

    /**
     * 现价
     */
    private String coupon;

    /**
     * 运费
     */
    private String freight;

    /**
     * 温馨提示
     */
    private String prompt;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private String updateTime;

    /**
     * 外部的商品照片
     */
    @TableField(exist = false)
    private String avterage;
    /**
     * 外部的门店名称
     */
    @TableField(exist = false)
    private String stName;
    /**
     * 外部的商品图片
     */
    @TableField(exist = false)
    private String imageurl;

    /**
     * 外部的门店图片
     */
    @TableField(exist = false)
    private String storeimage;
    /**
     * 外部商品类型名称
     */
    @TableField(exist = false)
    private String commodityType;

    /**
     * 门店评分
     */
    @TableField(exist = false)
    private String country;
    /**
     * 门店id
     */
    @TableField(exist = false)
    private Integer storeIdA;

    //价格顺序表示(0 代表正序 1 代表倒序)
    @TableField(exist = false)  //使用mybatisplus插入时 忽略该字段
    private Integer priceOrder;
}

