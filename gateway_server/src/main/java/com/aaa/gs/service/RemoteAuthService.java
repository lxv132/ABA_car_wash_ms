package com.aaa.gs.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ fileName:RemoteAuthService
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/17 10:46
 * @ version:1.0.0
 */
@FeignClient(name = "SsoServer")
public interface RemoteAuthService {
    /**
     * 校验token
     * @param token
     * @return
     */
    @GetMapping("/Auser/checkToken")
    boolean checkToken(@RequestParam("token") String token);
}
