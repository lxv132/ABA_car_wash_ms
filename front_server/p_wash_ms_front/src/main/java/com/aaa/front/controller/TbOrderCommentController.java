package com.aaa.front.controller;


import com.aaa.front.entity.TbOrderComment;
import com.aaa.front.service.TbOrderCommentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  TbOrderCommentController
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:43
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbOrderComment")
public class TbOrderCommentController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbOrderCommentService tbOrderCommentService;

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbOrderComment> page) {
        return success(this.tbOrderCommentService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbOrderCommentService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbOrderCommentService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param tbOrderComment 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbOrderComment tbOrderComment) {
        return success(this.tbOrderCommentService.save(tbOrderComment));
    }

    /**
     * 修改数据
     *
     * @param tbOrderComment 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbOrderComment tbOrderComment) {
        return success(this.tbOrderCommentService.updateById(tbOrderComment));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbOrderCommentService.removeById(id));
    }
}

