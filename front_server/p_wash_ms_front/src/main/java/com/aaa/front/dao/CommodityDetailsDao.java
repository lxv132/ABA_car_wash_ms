package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.CommodityDetails;

/**
 * @ fileName: 商品详情表(CommodityDetails)表控制层
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:04
 * @ version:1.0.0
 */
public interface CommodityDetailsDao extends BaseMapper<CommodityDetails> {


}

