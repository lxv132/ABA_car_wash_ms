package com.aaa.front.service;

import com.aaa.common.util.Result;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.Address;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @ fileName:  AddressService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:14:42
 * @ version:1.0.0
 */
public interface AddressService extends IService<Address> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<Address> queryPage(PagePlugin<Address> pagePlugin);

    /**
     * 修改所有数据
     *
     * @param addressId 实体对象
     * @return 修改结果
     */
     int updateDefault( Integer addressId);
}

