package com.aaa.front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.UserDetailDao;
import com.aaa.front.entity.UserDetail;
import com.aaa.front.service.UserDetailService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  UserDetailServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-05 09:07:59
 * @ version:1.0.0
 */
@Service("userDetailService")
public class UserDetailServiceImpl extends ServiceImpl<UserDetailDao, UserDetail> implements UserDetailService {

    /**
     * 服务对象
     */
    @Resource
    private UserDetailDao userDetailDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<UserDetail> queryPage(PagePlugin<UserDetail> pagePlugin) {
        // 分页参数设置
        Page<UserDetail> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.userDetailDao.selectPage(page, queryWrapper);
    }

    @Override
    public List<UserDetail> queryUserStore(Integer userId, Integer storeId) {
        return this.userDetailDao.queryUserStore(userId, storeId);
    }
}

