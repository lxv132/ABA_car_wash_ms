package com.aaa.front.dao;


import com.aaa.front.entity.StoreLabels;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (StoreLabels)表数据库访问层
 *
 * @author makejava
 * @since 2023-07-01 11:34:27
 */
public interface StoreLabelsDao extends BaseMapper<StoreLabels> {

    /**
     * 根据商店id查询对应的商店标签
     * @param storeId
     * @return
     */
    List<StoreLabels> selectLabelByStoreId(Integer storeId);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<StoreLabels> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<StoreLabels> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<StoreLabels> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<StoreLabels> entities);

    /**
     * 多表联查查询门店名称
     * @param storeLabels
     * @return
     */
    List<StoreLabels> queryPage(StoreLabels storeLabels);
}

