package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbStoreProduct;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbStoreProductService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 14:18:07
 * @ version:1.0.0
 */
public interface TbStoreProductService extends IService<TbStoreProduct> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbStoreProduct> queryPage(PagePlugin<TbStoreProduct> pagePlugin);
}

