package com.aaa.front.dao;


import com.aaa.front.entity.Area;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @ fileName: 行政区划表(Area)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 17:07:06
 * @ version:1.0.0
 */
public interface AreaDao extends BaseMapper<Area> {

    List<Area> queryAllCity();
}

