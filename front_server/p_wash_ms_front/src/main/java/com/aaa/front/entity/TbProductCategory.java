package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbProductCategory
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:10
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_product_category") //设值数据库表名
public class TbProductCategory extends Model<TbProductCategory> implements Serializable {
    /**
     * 商品id（主键）
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品编号
     */
    private String name;

    /**
     * 是否有效
     */
    private String isvalid;

    /**
     * 是否允许修改
     */
    private String isedit;

    /**
     * 描述
     */
    private String description;
}

