package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbAcarInfo
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:31
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_acar_info") //设值数据库表名
public class TbAcarInfo extends Model<TbAcarInfo> implements Serializable {
    /**
     * 车辆id
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 车辆注册
     */
    private String registration;

    /**
     * 添加车辆
     */
    private String aCar;

    /**
     * 充值缴费
     */
    private String payment;

    /**
     * 删除车辆
     */
    private String dCar;

    /**
     * 联系店主
     */
    private String contact;

    /**
     * 退款名称
     */
    private String rebate;
}

