package com.aaa.front.controller;


import com.aaa.front.entity.TbStoreEvaluate;
import com.aaa.front.service.CommonFileHandlerService;
import com.aaa.front.service.TbStoreEvaluateService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ fileName:  TbStoreEvaluateController
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 14:17:40
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbStoreEvaluate")
public class TbStoreEvaluateController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbStoreEvaluateService tbStoreEvaluateService;
    @Resource
    private CommonFileHandlerService commonFileHandlerService;
    /**
     * 文件上传对象  必须使用@RequestParam绑定上传对象,不可以少，少了上传不了
     * MultipartFile
     * @param file1
     * @return
     */
    @PostMapping("uploadFile")
    public Result uploadFile(@RequestParam MultipartFile file1){
        //调用封装的服务接口
        return success(commonFileHandlerService.uploadFile(file1,"two/lx/"));
    }
    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbStoreEvaluate> page) {
        return success(this.tbStoreEvaluateService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbStoreEvaluateService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbStoreEvaluateService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param tbStoreEvaluate 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbStoreEvaluate tbStoreEvaluate) {
        return success(this.tbStoreEvaluateService.save(tbStoreEvaluate));
    }

    /**
     * 修改数据
     *
     * @param tbStoreEvaluate 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbStoreEvaluate tbStoreEvaluate) {
        return success(this.tbStoreEvaluateService.updateById(tbStoreEvaluate));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbStoreEvaluateService.removeById(id));
    }
}

