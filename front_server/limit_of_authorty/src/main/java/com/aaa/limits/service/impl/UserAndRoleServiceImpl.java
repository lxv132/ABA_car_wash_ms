package com.aaa.limits.service.impl;

import com.aaa.limits.dao.UserAndRoleDao;
import com.aaa.limits.entity.Role;
import com.aaa.limits.entity.UserAndRole;
import com.aaa.limits.service.UserAndRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserAndRoleServiceImpl implements UserAndRoleService {
    //依赖注入，依赖Dao
    @Resource
    private UserAndRoleDao userAndRoleDao;

    //添加数据
    @Override
    public int add(UserAndRole userAndRole) {
        //先进行删除
        userAndRoleDao.delete(userAndRole.getUserId());
        //再添加
        return userAndRoleDao.add(userAndRole);
    }

    //根据id查询拥有的角色
    @Override
    public List<Role> queryById(Integer userId) {
        List<Role> roles = userAndRoleDao.queryById(userId);
        return roles;
    }
}
