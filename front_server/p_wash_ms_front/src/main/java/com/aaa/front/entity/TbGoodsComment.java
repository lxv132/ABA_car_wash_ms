package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbGoodsComment
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:33
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_goods_comment") //设值数据库表名
public class TbGoodsComment extends Model<TbGoodsComment> implements Serializable {
    /**
     * 商品评论id（主键）
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 评价类型（1 有图 0无图 ）
     */
    private String isimage;

    /**
     * 1-5 级（1-3 差评 4 中评 5好评）
     */
    private String starlevel;

    /**
     * 评价信息
     */
    private String evaluate;

    /**
     * 商品id（关联商品表）
     */
    private Integer goodid;

    /**
     * 评价时间
     */
    private String evaluatetime;

    /**
     * 用户id
     */
    private Integer userid;

    /**
     * 图片
     */
    private String image;

    /**
     * 门店id
     */
    private Integer outletid;
    /**
     * 用户名称
     */
    @TableField(exist = false)
    private String userName;
    /**
     * 用户图片
     */
    @TableField(exist = false)
    private String avatar;
    /**
     * 辅助评论
     */
    @TableField(exist = false)
    private Integer score;
}

