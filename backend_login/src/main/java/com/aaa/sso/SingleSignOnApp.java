package com.aaa.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ fileName:SingleSignOnApp
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/16 11:21
 * @ version:1.0.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
@EnableFeignClients //开启Openfeign客户端
public class SingleSignOnApp {
    public static void main(String[] args) {
        SpringApplication.run(SingleSignOnApp.class, args);
    }
}
