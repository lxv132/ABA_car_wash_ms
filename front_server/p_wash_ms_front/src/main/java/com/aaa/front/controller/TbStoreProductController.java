package com.aaa.front.controller;


import com.aaa.front.entity.TbStoreProduct;
import com.aaa.front.service.TbStoreProductService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  TbStoreProductController
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 14:18:06
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbStoreProduct")
public class TbStoreProductController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbStoreProductService tbStoreProductService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbStoreProduct> page) {
        return success(this.tbStoreProductService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbStoreProductService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbStoreProductService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param tbStoreProduct 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbStoreProduct tbStoreProduct) {
        return success(this.tbStoreProductService.save(tbStoreProduct));
    }

    /**
     * 修改数据
     *
     * @param tbStoreProduct 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbStoreProduct tbStoreProduct) {
        return success(this.tbStoreProductService.updateById(tbStoreProduct));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbStoreProductService.removeById(id));
    }
}

