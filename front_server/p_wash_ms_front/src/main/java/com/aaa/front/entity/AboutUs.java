package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: AboutUs
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-25 19:18:34
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("about_us") // 设值数据库表名
public class AboutUs extends Model<AboutUs> implements Serializable {
    /**
     * 关于我们信息ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 关于我们信息标题
     */
    private String title;

    /**
     * 关于我们信息内容(使用富文本编辑器编辑)
     */
    private String content;

    /**
     * 记录创建时间
     */
    private String createTime;
}

