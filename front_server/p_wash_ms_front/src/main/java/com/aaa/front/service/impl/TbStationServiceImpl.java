package com.aaa.front.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.dao.TbStationDao;
import com.aaa.front.entity.TbStation;
import com.aaa.front.service.TbStationService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @ fileName:  TbStationServiceImpl
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:51:01
 * @ version:1.0.0
 */
@Service("tbStationService")
public class TbStationServiceImpl extends ServiceImpl<TbStationDao, TbStation> implements TbStationService {

    /**
     * 服务对象
     */
    @Resource
    private TbStationDao tbStationDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<TbStation> queryPage(PagePlugin<TbStation> pagePlugin) {
        // 分页参数设置
        Page<TbStation> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
             queryWrapper.like(null != pagePlugin.getData().getStationId(), "station_id", pagePlugin.getData().getStationId());
        }
        return this.tbStationDao.selectPage(page, queryWrapper);
    }

    @Override
    public IPage<TbStation> queryAllStore(PagePlugin<TbStation> pagePlugin) {
        // 分页参数设置
        Page<TbStation> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());

        Integer storeId = pagePlugin.getData().getStoreId();
        // 查询封装类
        return this.tbStationDao.queryAllStore(page,storeId);
    }

    @Override
    public IPage<TbStation> queryMune(PagePlugin<TbStation> pagePlugin) {
        // 分页参数设置
        Page<TbStation> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        Integer idle = pagePlugin.getData().getIdle();
        // 查询封装类
        return this.tbStationDao.queryMune(page,idle);
    }
}

