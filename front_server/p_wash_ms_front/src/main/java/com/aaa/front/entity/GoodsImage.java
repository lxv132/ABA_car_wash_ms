package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: GoodsImage
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-30 11:56:05
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_goods_image") // 设值数据库表名
public class GoodsImage extends Model<GoodsImage> implements Serializable {
    /**
     * 商品照片id（主键）
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品图片路径
     */
    private String imageurl;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 图片描述
     */
    private String description;

    /**
     * 商品id
     */
    @TableField(exist = false)
    private Integer pictureid;


}

