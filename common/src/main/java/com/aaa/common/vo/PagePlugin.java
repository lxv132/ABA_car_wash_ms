package com.aaa.common.vo;

import lombok.Data;

/**
 * @ fileName:PagePlugin
 * @ description: 适用于mybaits-plus的分页插件
 * @ author:lx
 * @ createTime:2023/6/15 11:38
 * @ version:1.0.0
 */
@Data
public class PagePlugin<T> {
    //页码
    private Integer pageNo;
    //每页显示的数量
    private Integer pageSize;
    //泛型 输入什么类型就是什么类型
    private T data;
}
