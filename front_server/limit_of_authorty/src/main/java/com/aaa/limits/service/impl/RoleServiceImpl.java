package com.aaa.limits.service.impl;

import com.aaa.common.util.ConstUtil;
import com.aaa.common.vo.PagePlugin;
import com.aaa.limits.dao.RoleDao;
import com.aaa.limits.entity.Role;
import com.aaa.limits.service.RoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ fileName:  RoleServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:40
 * @ version:1.0.0
 */
@Service("roleService")
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements RoleService {

    /**
     * 服务对象
     */
    @Resource
    private RoleDao roleDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<Role> queryPage(PagePlugin<Role> pagePlugin) {
        // 分页参数设置
        Page<Role> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            queryWrapper.like(null != pagePlugin.getData().getRoleName(), "role_name", pagePlugin.getData().getRoleName());
            queryWrapper.like(null != pagePlugin.getData().getStatus(), "status", pagePlugin.getData().getStatus());
            queryWrapper.orderByAsc( "role_sort", pagePlugin.getData().getRoleSort());
        }
        return this.roleDao.selectPage(page, queryWrapper);
    }

    /**
     * 查询所有启用职位(添加员工时,下拉框使用)
     *
     * @return
     */
    @Override
    public List<Role> queryAll() {
        Role role = new Role();
        role.setStatus(String.valueOf(ConstUtil.PositionStatus.ENABLED));
        return roleDao.queryParam(role);
    }
    /**
     * 查询角色权限关联表 根据角色Id查询该角色关联的所有权限Id集合
     * @param roleId
     * @return
     */
    @Override
    public List<Integer> queryMenuIdListByRid(int roleId) {
        return roleDao.queryMenuIdListByRid(roleId);
    }

    /**
     * 根据角色ID批量删除该角色对应角色权限关联
     * @param roleId
     * @return
     */
    @Override
    public int deletePosMenuByPosId(int roleId) {
        return roleDao.deleteRoleByRoleId(roleId);
    }

    @Transactional //保证该方法中的调用的所有方法一起提交或者一起回滚    spring事务
    @Override
    public int addPosAndMenu(int roleId, String menuIds) {
        //1,先调用根据角色批量删除 角色权限关联的方法
        this.deletePosMenuByPosId(roleId);
        //2,再添加关联
        //posId = 1   每次一个值 角色/职位编号
        //menuIds = "100,101,102,200,201,300"
        //添加是否成功标识符
        boolean flag = true;
        //判断menuIds是否为空 不为空时，分割，添加
        if (!StringUtils.isEmpty(menuIds)) {
            //menuIdArray=[100,101,102,200,201,300]
            String[] menuIdArray = menuIds.split(",");
            for (String menuId : menuIdArray) {
                //第1次 100  第2次 101 .....最后一次 300  valueOf 把数字格式的字符串转数字
                int i = roleDao.addPosAndMenu(roleId, Integer.valueOf(menuId));
                if (i < 1) {
                    flag = false;
                }
            }
        }
        //如果循环添加结束，flag仍然是true说明 全部成功
        if (flag) {
            return 0;
        }
        return 1;
    }
}

