package com.aaa.limits.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.common.vo.TreeNode;
import com.aaa.limits.entity.Menu;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @ fileName:  MenuService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:31:51
 * @ version:1.0.0
 */
public interface MenuService extends IService<Menu> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<Menu> queryPage(PagePlugin<Menu> pagePlugin);

    /**
     *
     * @return
     */
    List<TreeNode> queryTreeData();

    /**
     * 根据用户编号，查询菜单树
     * @return
     */

    List<TreeNode> queryTreeNodeByUserId(String token);

    /**
     * 根据用户编号查询该用户拥有的按钮权限
     * @param userId
     * @return
     */
    List<String> queryButtonPermsByUserId(Integer userId);

    /**
     * 添加
     * @param treeNode
     * @return
     */
    int  add(TreeNode treeNode);

    /**
     * 修改
     * @param treeNode
     * @return
     */
    int  update(TreeNode treeNode);
}

