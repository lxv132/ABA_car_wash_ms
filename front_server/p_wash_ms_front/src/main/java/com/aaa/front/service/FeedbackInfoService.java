package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.FeedbackInfo;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  FeedbackInfoService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-27 14:28:24
 * @ version:1.0.0
 */
public interface FeedbackInfoService extends IService<FeedbackInfo> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<FeedbackInfo> queryPage(PagePlugin<FeedbackInfo> pagePlugin);
}

