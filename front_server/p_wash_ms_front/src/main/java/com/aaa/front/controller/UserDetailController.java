package com.aaa.front.controller;


import com.aaa.front.entity.UserDetail;
import com.aaa.front.service.UserDetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.Date;

/**
 * @ fileName:  UserDetailController
 * @ description:余额详情
 * @ fileName: lx
 * @ createTime: 2023-07-05 09:07:57
 * @ version:1.0.0
 */

@RestController
@RequestMapping("userDetail")
public class UserDetailController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private UserDetailService userDetailService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<UserDetail> page) {
        return success(this.userDetailService.queryPage(page));
    }

    /**
     * 根据用户编号和门店编号查询余额详情
     *
     */
    @GetMapping("/queryUserStore")
    public Result queryUserStore(@RequestParam Integer userId, @RequestParam Integer storeId) {
        return success(this.userDetailService.queryUserStore(userId,storeId));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.userDetailService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(String id) {
        return success(this.userDetailService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param userDetail 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody UserDetail userDetail) {
        userDetail.setRechargeTime(new Date().toLocaleString());
        return success(this.userDetailService.save(userDetail));
    }

    /**
     * 修改数据
     *
     * @param userDetail 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody UserDetail userDetail) {
        userDetail.setExpenditure(new Date().toLocaleString());
        return success(this.userDetailService.updateById(userDetail));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.userDetailService.removeById(id));
    }
}

