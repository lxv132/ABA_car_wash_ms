package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.TbOrderStatusCode;

/**
 * @ fileName: 订单状态
 * (TbOrderStatusCode)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-12 10:18:24
 * @ version:1.0.0
 */
public interface TbOrderStatusCodeDao extends BaseMapper<TbOrderStatusCode> {

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<TbOrderStatusCode> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<TbOrderStatusCode> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<TbOrderStatusCode> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<TbOrderStatusCode> entities);

}

