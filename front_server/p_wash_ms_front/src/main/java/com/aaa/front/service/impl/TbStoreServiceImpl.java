package com.aaa.front.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.dao.TbStoreDao;
import com.aaa.front.entity.TbStation;
import com.aaa.front.entity.TbStore;
import com.aaa.front.service.TbStoreService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @ fileName:  TbStoreServiceImpl
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:50:40
 * @ version:1.0.0
 */
@Service("tbStoreService")
public class TbStoreServiceImpl extends ServiceImpl<TbStoreDao, TbStore> implements TbStoreService {

    /**
     * 服务对象
     */
    @Resource
    private TbStoreDao tbStoreDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<TbStore> queryPage(PagePlugin<TbStore> pagePlugin) {
        // 分页参数设置
        Page<TbStore> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            queryWrapper.like(null != pagePlugin.getData().getStoreId(), "store_id", pagePlugin.getData().getStoreId());
        }
        return this.tbStoreDao.selectPage(page, queryWrapper);
    }
    /**
     * 根据用户id和门店id查询店铺信息
     *
     * @param userId
     * @param storeId
     * @return
     */
    @Override
    public TbStore selectStoreByStoreIdAndUserId(Integer userId, Integer storeId) {
        return tbStoreDao.selectStoreByStoreIdAndUserId(userId, storeId);
    }
    /**
     * 根据用户编号查询门店
     *
     * @param id
     * @return
     */
    @Override
    public List<TbStore> queryUserStore(Integer id) {
        return this.tbStoreDao.queryUserStore(id);
    }

    @Override
    public List queryAllCount(Integer storeId) {
        List<TbStore> tbStations = this.tbStoreDao.queryAllCount(storeId);

        TbStore tbStation1 = tbStations.get(0);

        List<TbStore> tbStations1 = this.tbStoreDao.queryAllCounA(storeId);

        TbStore tbStation2 = tbStations1.get(0);
        TbStore tbStation = new TbStore();
        tbStation.setCountA(tbStation1.getCountA());
        tbStation.setCountB(tbStation2.getCountB());

        ArrayList<Object> objects = new ArrayList<>();
        objects.add(tbStation);

        return objects;
    }

    /**
     * 查询店铺信息
     * @return
     */
    @Override
    public List<TbStore> selectStoreByStoreId(TbStore tbStore) {
        return tbStoreDao.selectStoreByStoreId(tbStore);
    }

    @Override
    public List<TbStore> storeUserCarId(String carNo) {
        return tbStoreDao.storeUserCarId(carNo);
    }
}

