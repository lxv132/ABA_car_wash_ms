package com.aaa.front.util;

import com.aaa.common.util.Result;
import com.aaa.front.config.SpringRabbitDeadLetterMQConfig;
import com.aaa.front.controller.TbOrderController;

import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * ProjectName sbm_rabbitmq_consumer_demo_20230605
 * Author lx
 * Date 2023/06/05 上午 09:49
 * Version 1.0.0
 * Description 编写消费者工具类 用来消费信息
 */
@Component
@Log4j2
public class RabbitConsumerUtil {

    @Resource
    private TbOrderController tbOrderController;
    /**
     * 监听方法 用来动态获取Rabbitmq中的消息
     * 用来讲解  死信队列
     * @param message
     */
    @RabbitListener(queues = SpringRabbitDeadLetterMQConfig.DEAD_LETTER_QUEUE_1)
    public void recveiveMessage(Message message, Channel channel){
        System.out.println("消息进入死信队列................................................................");
        //获取消息的唯一识别符，数字
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            String s = new String(message.getBody());
            log.info("收到的消息为：" + new String(message.getBody()));
            Result cancelupdate = tbOrderController.Cancelupdate(s);
            System.out.println(" 订单超时未支付 " + cancelupdate);
            channel.basicNack(deliveryTag,true,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
