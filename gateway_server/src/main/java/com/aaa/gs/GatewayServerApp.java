package com.aaa.gs;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ fileName:GatewayServerApp
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/12 9:32
 * @ version:1.0.0
 */
@SpringBootApplication //springboot 启动类注解
@EnableDiscoveryClient // 开启发现客户端  注册中心客户端注解
@EnableFeignClients//开启Openfeign客户端
public class GatewayServerApp {
    public static void main(String[] args) {
       SpringApplication.run(GatewayServerApp.class, args);
    }
}
