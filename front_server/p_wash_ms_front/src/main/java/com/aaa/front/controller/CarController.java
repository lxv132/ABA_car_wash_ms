package com.aaa.front.controller;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.Car;
import com.aaa.front.entity.CarAndStore;
import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.service.CarAndStoreService;
import com.aaa.front.service.CarService;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * @ fileName:  CarController
 * @ description: 车辆信息表
 * @ fileName: lx
 * @ createTime: 2023-06-28 10:30:55
 * @ version:1.0.0
 */

@RestController
@RequestMapping("car")
public class CarController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private CarService carService;
    @Resource
    private CarAndStoreService carAndStoreService;


    /**
     * 权限管理需要的字段
     * @param page
     * @return
     */
    @PostMapping("queryMune")
    public Result queryMune(@RequestBody PagePlugin<Car> page){
        return success(carService.queryMune(page));
    }



    /**
     * 多表联查
     * @return 所有数据
     */
    @GetMapping("queryPageAll")
    public Result queryPageAll(String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String userId = login.toString().replace("\"", "");
        return success(this.carService.queryPageAll(Integer.valueOf(userId)));
    }
    /**
     * 根据用户和门店id查询数据
     * @return 所有数据
     */
    @GetMapping("queryStore")
    public Result queryStore(String token,@RequestParam Integer storeId) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String userId = login.toString().replace("\"", "");
        return success(this.carService.queryStore(Integer.valueOf(userId),storeId));
    }
    /**
     * 多表联查
     * @return 所有数据
     */
    @GetMapping("queryCar")
    public Result queryCar(Integer id) {
        return success(this.carService.queryCar(id));
    }


    /**
     * 根据车牌查询
     * @return 所有数据
     */
    @GetMapping("queryCarNo")
    public Result queryCarNo(@RequestParam  String carNo,@RequestParam Integer storeId) {
        return success(this.carService.queryCarNo(carNo, storeId));
    }




    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Car> page) {
        return success(this.carService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.carService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.carService.getById(id));
    }




    /**
     * 新增数据
     *
     * @param car 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody Car car,String token,Integer storeId) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        car.setCreateBy(loginName);
        car.setCreateTime(new Date().toLocaleString());
        boolean save = this.carService.save(car);

        CarAndStore carAndStore = new CarAndStore();
        carAndStore.setCarId(car.getCarId());
        carAndStore.setStoreId(storeId);
        carAndStoreService.save(carAndStore);

        return success(save);
    }

    /**
     * 修改数据
     *
     * @param car 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody Car car) {
        return success(this.carService.updateById(car));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.carService.removeById(id));
    }
}

