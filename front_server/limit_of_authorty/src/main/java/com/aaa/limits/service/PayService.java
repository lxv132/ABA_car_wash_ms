    package com.aaa.limits.service;





import com.aaa.limits.entity.UserDetail;

import java.util.Map;

/**
 * @ fileName:PayService
 * @ description:
 * @ author:
 * @ date: 2023/4/7 22:47
 * @ version:1.0.0
 */
public interface PayService {
    /**
     *获取支持二维码
     * @param detail
     * @return
     */
    Map getNativeByOrder(UserDetail detail);


    /**
     * 获取支付状态
     * @param detailId
     * @return
     */
    Map getPayStatus(String detailId);
}
