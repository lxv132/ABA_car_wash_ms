package com.aaa.front.controller;


import com.aaa.front.entity.CommodityType;
import com.aaa.front.service.CommodityTypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.List;

/**
 * @ fileName:  CommodityTypeController
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:26
 * @ version:1.0.0
 */

@RestController
@RequestMapping("commodityType")
public class CommodityTypeController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityTypeService commodityTypeService;

    /**
     * 查询关于商品
     * @return
     */
    @PostMapping("queryAboutCommodity")
    public Result queryAboutCommodity(Integer commentId) {
        return success(commodityTypeService.queryAboutCommodity(commentId));
    }


    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<CommodityType> page) {
        return success(this.commodityTypeService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        QueryWrapper<CommodityType> commodityTypeQueryWrapper = new QueryWrapper<>();
        commodityTypeQueryWrapper.eq("isvalid",'0');
        return success(this.commodityTypeService.list(commodityTypeQueryWrapper));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.commodityTypeService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param commodityType 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody CommodityType commodityType) {
        return success(this.commodityTypeService.save(commodityType));
    }

    /**
     * 修改数据
     *
     * @param commodityType 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody CommodityType commodityType) {
        return success(this.commodityTypeService.updateById(commodityType));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.commodityTypeService.removeById(id));
    }
}

