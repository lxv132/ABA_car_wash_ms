package com.aaa.front.dao;

import com.aaa.front.entity.Car;
import com.aaa.front.entity.TbStation;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName: 门店工位表(TbStation)表控制层
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:51:01
 * @ version:1.0.0
 */
public interface TbStationDao extends BaseMapper<TbStation> {

    // IPage<TbStation> (IPage<TbStation> page, @Param(Constants.WRAPPER) Wrapper<TbStation> queryWrapper);
    <E extends IPage<TbStation>> E queryAllStore(E page, int storeId);


    // IPage<TbStation> (IPage<TbStation> page, @Param(Constants.WRAPPER) Wrapper<TbStation> queryWrapper);
    <E extends IPage<TbStation>> E queryMune(E page, @Param("idle") Integer idle);
}

