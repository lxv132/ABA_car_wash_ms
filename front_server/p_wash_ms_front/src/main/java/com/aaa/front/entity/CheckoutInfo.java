package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: CheckoutInfo
 * @ description: 门店记录
 * @ fileName: lx
 * @ createTime: 2023-07-08 09:35:46
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("checkout_info") // 设值数据库表名
public class CheckoutInfo extends Model<CheckoutInfo> implements Serializable {
    /**
     * 结算ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "checkout_id", type = IdType.AUTO)
    private Integer checkoutId;

    /**
     * 用户ID，用于关联用户信息
     */
    private Integer userId;

    /**
     * 车辆ID，用于关联车辆信息
     */
    private Integer carId;

    /**
     * 门店id，确定在那个门店进行洗车
     */
    private Integer storeId;

    /**
     * 工位id，确定在那个门店的那个进行洗车
     */
    private Integer stationId;

    /**
     * 判断事务是否完成 (1完成，0没有完成)
     */
    private String complete;

    /**
     * 服务开始时间
     */
    private Date startTime;

    /**
     * 服务结束时间
     */
    private Date endTime;

    /**
     * 服务总价格
     */
    private Double totalPrice;

    /**
     * 记录创建时间
     */
    private String goodid;

    /**
     * 门店名号
     */
    @TableField(exist = false)
    private String storeName;
    /**
     * 车牌
     */
    @TableField(exist = false)
    private String carNo;
    /**
     * 洗车单价
     */
    @TableField(exist = false)
    private double washPrice;

    /**
     * 用户昵称
     */
    @TableField(exist = false)
    private String smallname;


    /**
     * 门店工位名称
     */
    @TableField(exist = false)
    private String stationName;

    @TableField(exist = false)
    private Integer orderTotal;

    @TableField(exist = false)
    private Integer orderNum;

    @TableField(exist = false)
    private String date;

    @TableField(exist = false)
    private String cumulativeTotal;
    @TableField(exist = false)
    private String dailyTotal;

}

