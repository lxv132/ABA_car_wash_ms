package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.AboutUs;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  AboutUsService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-25 19:18:35
 * @ version:1.0.0
 */
public interface AboutUsService extends IService<AboutUs> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<AboutUs> queryPage(PagePlugin<AboutUs> pagePlugin);
}

