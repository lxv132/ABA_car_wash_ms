package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbOrderStatusCode
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-12 10:18:24
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_order_status_code") // 设值数据库表名
public class TbOrderStatusCode extends Model<TbOrderStatusCode> implements Serializable {
    /**
     * 订单状态主键id 自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "ststus_id", type = IdType.AUTO)
    private Integer ststusId;

    /**
     * 订单状态码
     */
    private Integer orderStatusCode;

    /**
     * 订单状态
     */
    private String orderStatusName;
}

