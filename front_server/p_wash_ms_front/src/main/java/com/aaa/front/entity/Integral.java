package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: Integral
 * @ description:积分表
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:41
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_user_integral") //设值数据库表名
public class Integral extends Model<Integral> implements Serializable {
    /**
     * 积分ID
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "integral_id", type = IdType.AUTO)
    private Integer integralId;

    /**
     * 用户ID，外键关联用户表
     */
    private Integer userId;
    /**
     * 用户ID，外键关联用户表
     */
    private Integer storeId;

    /**
     * 积分数值
     */
    private Integer credits;

    /**
     * 积分来源
     */
    private String source;

    /**
     * 积分记录创建时间
     */
    private String integralTime;
    /**
     * 用户名
     */
    @TableField(exist = false)
    private String smallname;
    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String storeName;
}

