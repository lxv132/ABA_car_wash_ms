package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: UserDetail
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-05 09:07:58
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_user_detail") // 设值数据库表名
public class UserDetail extends Model<UserDetail> implements Serializable {
    /**
     * 明细ID
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "detail_id", type = IdType.INPUT)
    private String detailId;

    /**
     * 外键，关联用户ID
     */
    private Integer userId;
    /**
     * 外键关联门店id
     */
    private Integer storeId;

    /**
     * 支出金额
     */
    private String expenditure;

    /**
     * 实际充值金额
     */
    private Double rechargeMoney;

    /**
     * 优惠金额
     */
    private Double discountsMoney;

    /**
     * 实际到账金额 (实际充值金额+优惠金额)
     */
    private String accountMoney;

    /**
     * 充值时间
     */
    private String rechargeTime;

    /**
     * 支出时间
     */
    private String expenditureTime;
    /**
     * 洗车订单id
     */
    private Integer checkoutId;

    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String storeName;

}

