package com.aaa.front.controller;


import com.aaa.front.entity.TbStoreAddress;
import com.aaa.front.service.TbStoreAddressService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  TbStoreAddressController
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:21
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbStoreAddress")
public class TbStoreAddressController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbStoreAddressService tbStoreAddressService;

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbStoreAddress> page) {
        return success(this.tbStoreAddressService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbStoreAddressService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbStoreAddressService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param tbStoreAddress 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbStoreAddress tbStoreAddress) {
        return success(this.tbStoreAddressService.save(tbStoreAddress));
    }

    /**
     * 修改数据
     *
     * @param tbStoreAddress 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbStoreAddress tbStoreAddress) {
        return success(this.tbStoreAddressService.updateById(tbStoreAddress));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbStoreAddressService.removeById(id));
    }
}

