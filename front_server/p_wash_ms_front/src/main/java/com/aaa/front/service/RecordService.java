package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.Record;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  RecordService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-27 14:18:33
 * @ version:1.0.0
 */
public interface RecordService extends IService<Record> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<Record> queryPage(PagePlugin<Record> pagePlugin);
}

