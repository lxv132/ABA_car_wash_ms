package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.CommodityType;

/**
 * @ fileName: 商品类型表(CommodityType)表控制层
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:26
 * @ version:1.0.0
 */
public interface CommodityTypeDao extends BaseMapper<CommodityType> {

    /**
     * 查询关于商品
     * @return
     */
    List<CommodityType> queryAboutCommodity(Integer commentId);

}

