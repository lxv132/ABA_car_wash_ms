package com.aaa.front.service.impl;

import com.aaa.front.entity.TbStation;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.TbCommodityDao;
import com.aaa.front.entity.TbCommodity;
import com.aaa.front.service.TbCommodityService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  TbCommodityServiceImpl
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:32
 * @ version:1.0.0
 */
@Service("tbCommodityService")
public class TbCommodityServiceImpl extends ServiceImpl<TbCommodityDao, TbCommodity> implements TbCommodityService {

    /**
     * 服务对象
     */
    @Resource
    private TbCommodityDao tbCommodityDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<TbCommodity> queryPage(PagePlugin<TbCommodity> pagePlugin) {
        // 分页参数设置
        Page<TbCommodity> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.tbCommodityDao.selectPage(page, queryWrapper);
    }

    @Override
    public IPage<TbCommodity> queryAllStore(PagePlugin<TbCommodity> pagePlugin) {
        // 分页参数设置
        Page<TbCommodity> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        Integer storeId = pagePlugin.getData().getStoreId();

        // 查询封装类
        return this.tbCommodityDao.queryAllStore(page,storeId);
    }

    @Override
    public IPage<TbCommodity> queryAllUnion(PagePlugin<TbCommodity> pagePlugin) {
        // 分页参数设置
        Page<TbCommodity> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());

        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("prompt");
        // 查询封装类
        return this.tbCommodityDao.queryAllUnion(page,queryWrapper);
    }
    /**
     查询所有门店兑换券
     * @return
     */
    @Override
    public List<TbCommodity> queryAllTbCommodity(Integer producttypeid) {
        return tbCommodityDao.queryAllTbCommodity(producttypeid);
    }

    @Override
    public IPage<TbCommodity> queryAboutCommodity(PagePlugin<TbCommodity> pagePlugin) {
        // 分页参数设置
        Page<TbCommodity> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());

        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("prompt");
        // 查询封装类
        return this.tbCommodityDao.queryAboutCommodity(page,queryWrapper);
    }
    /**
     * 前端商品商城商品 按照商品类型查询
     * @param product
     * @return
     */
    @Override
    public List<TbCommodity> frontMultiTableJointCheck(TbCommodity product) {
        return tbCommodityDao.frontMultiTableJointCheck(product);
    }

    @Override
    public TbCommodity queryAllTbCommodityId(Integer commodityId) {
        return tbCommodityDao.queryAllTbCommodityId(commodityId);
    }
}

