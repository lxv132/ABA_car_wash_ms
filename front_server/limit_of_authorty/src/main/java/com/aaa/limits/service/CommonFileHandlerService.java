package com.aaa.limits.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @ fileName:CommonFileHandlerService
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/21 20:32
 * @ version:1.0.0
 */
public interface CommonFileHandlerService {
    /**
     * 封装的图片上传方法
     * @param multipartFile
     * @param savePath
     * @return
     */
    Map uploadFile(MultipartFile multipartFile, String savePath);
}
