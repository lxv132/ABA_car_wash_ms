package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbImage
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-30 14:25:08
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_image") // 设值数据库表名
public class TbImage extends Model<TbImage> implements Serializable {
    /**
     * ID
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品id
     */
    private Integer shopId;

    /**
     * 图片地址
     */
    private String url;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String storeName;
}

