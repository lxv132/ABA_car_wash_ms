package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.entity.Integral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.Balance;

/**
 * @ fileName: 登陆用户余额
 * (Balance)表控制层
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:11
 * @ version:1.0.0
 */
public interface BalanceDao extends BaseMapper<Balance> {

    Balance queryInteg(Integer userId);

    /**
     * 而根据用户id查询余额，积分，门店名称
     * @param userId
     * @return
     */
    List<Balance> queryAllye(Integer userId);
    /**
     * 根据用户id和门店id查询余额
     * @param userId
     * @param storeId
     * @return
     */
    Balance queryUserAndStore(@Param("userId") Integer userId, @Param("storeId") Integer storeId);

    /**
     * 权限管理需要的字段
     *
     * @param
     */
    <E extends IPage<Balance>> E queryMune(E page);

}

