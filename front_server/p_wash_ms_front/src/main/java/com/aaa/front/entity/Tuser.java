package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ fileName: Tuser
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-26 18:51:12
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("t_pt_tuser") // 设值数据库表名
public class Tuser extends Model<Tuser> implements Serializable {
    /**
     * uuid方式生成
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 可用手机号或账号登陆
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 昵称
     */
    private String smallname;

    /**
     * 性别
     */
    private String sex;
    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * qq
     */
    private String qq;

    /**
     * 证件号
     */
    private String zjno;

    /**
     * 状态
     */
    private String state;

    /**
     * 创建时间
     */
    private String cjtime;

    /**
     * 更新时间
     */
    private String gxtime;

    /**
     * 盐
     */
    private String salt;
    /**
     * 服务门店
     */
    @TableField(exist = false)
    private Integer storeNum;
    /**
     * 服务次数
     */
    @TableField(exist = false)
    private Integer serviceNum;
    /*
     * 每日新增
     */
    @TableField(exist = false)
    private Integer today;

    /*
     * 每周新增
     */
    @TableField(exist = false)
    private Integer weekday;

    /*
     * 每月新增
     */
    @TableField(exist = false)
    private Integer monthday;

    /*
     * 每年新增
     */
    @TableField(exist = false)
    private Integer yearday;
    /**
     * 总的数据量
     */
    @TableField(exist = false)
    private Integer total;
}

