package com.aaa.limits.dao;

import com.aaa.limits.entity.Role;
import com.aaa.limits.entity.UserAndRole;


import java.util.List;

public interface UserAndRoleDao {
    //添加
    int add(UserAndRole userAndRole);

    //根据id删除拥有的角色
    int delete(Integer userId);

    //根据id查询拥有的角色
    List<Role> queryById(Integer userId);
}
