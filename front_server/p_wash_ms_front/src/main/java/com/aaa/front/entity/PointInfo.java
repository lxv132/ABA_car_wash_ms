package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: PointInfo
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:28
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("point_info") //设值数据库表名
public class PointInfo extends Model<PointInfo> implements Serializable {
    /**
     * 积分ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID，用于关联用户信息
     */
    private Integer userId;

    /**
     * 用户积分
     */
    private Integer points;

    /**
     * 积分变化量，可以为正或负数
     */
    private Integer change;

    /**
     * 积分变化原因描述
     */
    private String reason;

    /**
     * 积分变化时间
     */
    private Date createTime;
}

