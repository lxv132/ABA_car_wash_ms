package com.aaa.common.util;

/**
 * @ fileName:CustomException
 * @ description: 自定义异常
 * @ author:lx
 * @ createTime:2023/5/4 9:22
 * @ version:1.0.0
 */
//RuntimeException 运行时异常
public class CustomException extends RuntimeException {
    private int errorCode;
    private String errorMessage;

    /**
     * 自定义构造
     * @param errorCode
     * @param errorMessage
     */
    public CustomException(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public CustomException() {
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
