package com.aaa.common.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ fileName:TreeNode
 * @ description: 树形菜单节点实体
 * @ author:lx
 * @ createTime:2023/5/12 10:15
 * @ version:1.0.0
 */
@Data
public class TreeNode implements Serializable {
    private int id;
    private String label;
    private int parentId;
    private String url;
    private String perms;
    private String icon;
    private String parentName;
    private List<TreeNode> children;
    private String visible;
    private String menuType;
}
