package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbOrderComment
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:44
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_order_comment") //设值数据库表名
public class TbOrderComment extends Model<TbOrderComment> implements Serializable {
    /**
     * 评论id
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Integer commentId;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 门店评分(满分5分)
     */
    private Integer storeScore;

    /**
     * 服务评分(满分5分)
     */
    private Integer serveScore;

    /**
     * 整体评分(满分5分)
     */
    private Integer wholeScore;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 点评时间
     */
    private Date commentTime;

    /**
     * 点评人id
     */
    private Integer userId;
}

