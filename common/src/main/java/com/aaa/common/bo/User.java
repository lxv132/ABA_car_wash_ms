package com.aaa.common.bo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ fileName: User
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:32:41
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
public class User implements Serializable {
    private Integer userId;

    /** 用户头像 */
    private String avatar;

    /** 登录账号 */
    private String loginName;

    /** 用户名 */
    private String userName;

    /** 手机号码 */
    private String phonenumber;

    /** 密码 */
    private String password;

    /** 盐加密 */
    private String salt;

    /** serviceTotal */
    private Integer storeTotal;

    /** 服务次数 */
    private Integer serviceTotal;

    /** 创建人 */
    private String createName;

    /** 创建时间 */
    private String userDate;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 注册是否通过(0未审核,1通过，2未通过) */
    private Integer passThrough;
}

