package com.aaa.front.dao;

import com.aaa.front.entity.Car;
import com.aaa.front.entity.CheckoutInfo;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName: 车辆信息
 * (Car)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 10:30:55
 * @ version:1.0.0
 */
public interface CarDao extends BaseMapper<Car> {
    /**
     * 用户和车辆多表联查
     * @param userId
     * @return
     */
    List<Car> query(@Param("userId") Integer userId);

    /**
     * 门店和车辆多表联查
     * @param id
     * @return
     */
    List<Car> queryCar( Integer id);

    Car queryCarNo(@Param("carNo") String carNo,@Param("storeId") Integer storeId);

    /**
     * 根据用户和门店i的查询所有车辆
     * @param userId
     * @return
     */
    List<Car> queryStore(@Param("userId") Integer userId,@Param("storeId") Integer storeId);


    /**
     * 权限管理需要的字段
     *
     * @param
     */
    <E extends IPage<Car>> E queryMune(E page);
}

