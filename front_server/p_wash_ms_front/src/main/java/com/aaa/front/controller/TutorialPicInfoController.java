package com.aaa.front.controller;


import com.aaa.front.entity.TutorialPicInfo;
import com.aaa.front.service.TutorialPicInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  TutorialPicInfoController
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:20:06
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tutorialPicInfo")
public class TutorialPicInfoController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TutorialPicInfoService tutorialPicInfoService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TutorialPicInfo> page) {
        return success(this.tutorialPicInfoService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tutorialPicInfoService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tutorialPicInfoService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param tutorialPicInfo 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TutorialPicInfo tutorialPicInfo) {
        return success(this.tutorialPicInfoService.save(tutorialPicInfo));
    }

    /**
     * 修改数据
     *
     * @param tutorialPicInfo 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TutorialPicInfo tutorialPicInfo) {
        return success(this.tutorialPicInfoService.updateById(tutorialPicInfo));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tutorialPicInfoService.removeById(id));
    }
}

