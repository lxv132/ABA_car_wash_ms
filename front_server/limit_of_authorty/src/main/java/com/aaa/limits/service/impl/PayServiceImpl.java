package com.aaa.limits.service.impl;


import com.aaa.limits.entity.UserDetail;
import com.aaa.limits.service.PayService;
import com.aaa.limits.util.HttpClient;
import com.aaa.limits.util.PayConfigUtil;
import com.github.wxpay.sdk.WXPayUtil;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * @ fileName:PayServiceImpl
 * @ description:微信二维码支付 实现类
 * @ author:
 * @ date: 2023/4/7 22:48
 * @ version:1.0.0
 */
@Service
public class PayServiceImpl implements PayService {
    /**
     *获取支持二维码
     * @param userDetail
     * @return
     */
    @Override
    public Map getNativeByOrder(UserDetail userDetail) {
        try {
            //统一下单   生成预支付的订单
            Map m = new HashMap();
            //参数
            m.put(PayConfigUtil.APP_ID, PayConfigUtil.APPID);
            m.put("mch_id", PayConfigUtil.MCH_ID);//"1558950191"
            //商户编号
            m.put("out_trade_no", userDetail.getDetailId());
            //1.00    1
            //支付费用
            m.put("total_fee", new BigDecimal(0.01).multiply(new BigDecimal("100")).longValue() + ""); //单位 分
            //支付状态
            m.put("body", "待支付");
            m.put("nonce_str", WXPayUtil.generateNonceStr());
            //处理支付回调ip
            m.put("spbill_create_ip", "172.16.9.72");
            //支付方式  NATIVE：扫码支付
            m.put("trade_type", "NATIVE");
            // 唤醒地址 ，回调地址
            m.put("notify_url", "http://www.chenkaixiang.top/notifyWeiXinPay\n");
            //统一下单（使用微信系统，生成二维码）
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
            //设置请求参数和API_KEY
            client.setXmlParam(WXPayUtil.generateSignedXml(m,PayConfigUtil.API_KEY));
            //设置使用http请求方式
            client.setHttps(true);
            //执行
            client.post();
            //生成的预支付订单的消息
            String xml=client.getContent();//xml
            System.out.println("-------------"+xml);
            //把返回的xml字符串转换为map
            Map<String, String> resultMap = WXPayUtil.xmlToMap(xml);
            //打印返回值
            System.out.println(resultMap);
            //再次定义返回对象
            Map map = new HashMap<>();
            //放入的是二维码地址
            map.put("code_url", resultMap.get("code_url"));
            return map;
        }catch(Exception e){
             //System.out.println(e.getMessage());
             e.printStackTrace();
            //log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 获取支付状态
     * @param detailId
     * @return
     */
    @Override
    public Map getPayStatus(String detailId) {
        try {
            Map m = new HashMap();
            //参数
            m.put("appid", PayConfigUtil.APPID);
            m.put("mch_id", PayConfigUtil.MCH_ID);//"1558950191"
            m.put("out_trade_no", detailId+"");
            //1.00    1
            //m.put("total_fee", new BigDecimal(order.getPrice()*order.getNum()).multiply(new BigDecimal("100")).longValue() + ""); //单位 分
            m.put("nonce_str", WXPayUtil.generateNonceStr());
            //m.put("sign", "127.0.0.1");
            //m.put("trade_type", "NATIVE");
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
            client.setXmlParam(WXPayUtil.generateSignedXml(m, PayConfigUtil.API_KEY));
            client.setHttps(true);
            client.post();
            String xml = client.getContent();//xml
            System.out.println("支付后的信息========" + xml);
            //System.out.println("支付后的信息========" + xml);
            Map<String, String> resultMap = WXPayUtil.xmlToMap(xml);
            System.out.println("支付之后的信息的内容==================="+resultMap);
            return resultMap;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
