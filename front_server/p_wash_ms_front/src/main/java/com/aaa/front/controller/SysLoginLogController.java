package com.aaa.front.controller;


import com.aaa.front.entity.SysLoginLog;
import com.aaa.front.service.SysLoginLogService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  SysLoginLogController
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:30
 * @ version:1.0.0
 */

@RestController
@RequestMapping("sysLoginLog")
public class SysLoginLogController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private SysLoginLogService sysLoginLogService;

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<SysLoginLog> page) {
        return success(this.sysLoginLogService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.sysLoginLogService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.sysLoginLogService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param sysLoginLog 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody SysLoginLog sysLoginLog) {
        return success(this.sysLoginLogService.save(sysLoginLog));
    }

    /**
     * 修改数据
     *
     * @param sysLoginLog 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody SysLoginLog sysLoginLog) {
        return success(this.sysLoginLogService.updateById(sysLoginLog));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.sysLoginLogService.removeById(id));
    }
}

