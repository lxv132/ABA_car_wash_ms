package com.aaa.front.service;

import com.aaa.front.entity.GoodsImage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  GoodsImageService
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:48
 * @ version:1.0.0
 */
public interface GoodsImageService extends IService<GoodsImage> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<GoodsImage> queryPage(PagePlugin<GoodsImage> pagePlugin);

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<GoodsImage> queryAllStore(PagePlugin<GoodsImage> pagePlugin);
}

