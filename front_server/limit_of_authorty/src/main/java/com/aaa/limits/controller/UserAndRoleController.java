package com.aaa.limits.controller;

import com.aaa.common.util.BaseController;

import com.aaa.common.util.Result;
import com.aaa.limits.entity.UserAndRole;
import com.aaa.limits.service.UserAndRoleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("UserAndRole")
public class UserAndRoleController extends BaseController {
    //依赖注入，依赖Service
    @Resource
    private UserAndRoleService userAndRoleService;

    //添加
    @PostMapping("add")
    public Result add(@RequestBody UserAndRole userAndRole){
        return success(userAndRoleService.add(userAndRole));
    }
    //根据id查询拥有的角色
    @GetMapping("queryByUserId")
    public Result queryByUserId(Integer userId){
        return success(userAndRoleService.queryById(userId));
    }
}
