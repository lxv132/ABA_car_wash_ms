package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.CommodityDetails;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  CommodityDetailsService
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:05
 * @ version:1.0.0
 */
public interface CommodityDetailsService extends IService<CommodityDetails> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<CommodityDetails> queryPage(PagePlugin<CommodityDetails> pagePlugin);
}

