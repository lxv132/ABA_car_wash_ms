package com.aaa.front.controller;


import com.aaa.front.entity.GoodsImage;
import com.aaa.front.entity.TbStation;
import com.aaa.front.service.GoodsImageService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  GoodsImageController
 * @ description: 商品图片
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:47
 * @ version:1.0.0
 */

@RestController
@RequestMapping("goodsImage")
public class GoodsImageController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GoodsImageService goodsImageService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<GoodsImage> page) {
        return success(this.goodsImageService.queryPage(page));
    }


    /**
     * 根据商品编号查询商品
     */
    @PostMapping("queryAllStore")
    public Result queryAllStore(@RequestBody PagePlugin<GoodsImage> page) {
        return success(this.goodsImageService.queryAllStore(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.goodsImageService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.goodsImageService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param goodsImage 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody GoodsImage goodsImage) {
        return success(this.goodsImageService.save(goodsImage));
    }

    /**
     * 修改数据
     *
     * @param goodsImage 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody GoodsImage goodsImage) {
        return success(this.goodsImageService.updateById(goodsImage));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.goodsImageService.removeById(id));
    }
}

