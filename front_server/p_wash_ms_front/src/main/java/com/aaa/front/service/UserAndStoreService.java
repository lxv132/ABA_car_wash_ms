package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.UserAndStore;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  UserAndStoreService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-06 11:01:55
 * @ version:1.0.0
 */
public interface UserAndStoreService extends IService<UserAndStore> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<UserAndStore> queryPage(PagePlugin<UserAndStore> pagePlugin);

    /**
     * 根据用户名和店铺名查询
     * @param userId
     * @param storeId
     * @return
     */
    List<UserAndStore> selectByStoreIdAndUserId(@Param("userId") Integer userId, @Param("storeId") Integer storeId);

}

