package com.aaa.gs.filter;

import com.aaa.common.util.Result;
import com.aaa.common.util.ResultStatus;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 * @ fileName:IllegalIPFilter
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/12 14:10
 * @ version:1.0.0
 */
@Component
public class IllegalIPFilter implements GlobalFilter, Ordered {
    //基本属性依赖注入配置的值
    @Value("${illegal_ip}")
    private String illegalIp;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("经过了非法IP过滤器。。。。。。。。。。。。。。。。。。");
        //获取request对象
        ServerHttpRequest request = exchange.getRequest();
        //获取response对象
        ServerHttpResponse response = exchange.getResponse();
        //IP过滤功能 如果是非法IP，直接拦截，提示错误
        //获取客户请求的IP地址
        InetSocketAddress remoteAddress = request.getRemoteAddress();
        //String hostName = remoteAddress.getHostName();
        InetAddress address = remoteAddress.getAddress();
        //System.out.println("主机名称----------------:"+hostName);
        String hostAddress = address.getHostAddress();
        System.out.println("IP地址----------------:"+hostAddress);
        //判断获取到IP是否是192.168.28.70，192.168.28.133
        //if(hostAddress.equals("192.168.28.173")||hostAddress.equals("192.168.28.70")||hostAddress.equals("192.168.28.133")||hostAddress.equals("192.168.28.47")){
        // illegalIp=192.168.28.173,192.168.28.70,192.168.28.133,192.168.28.47,192.168.28.238
        // "192.168.28.173,192.168.28.70,192.168.28.133,192.168.28.47,192.168.28.238".contains("192.168.28.173") 为true
        if(illegalIp.contains(hostAddress)){
            //定义返回map对象
            Result result = new Result(ResultStatus.ILLEGAL_IP_ERROR.getReturnCode(),
                    ResultStatus.ILLEGAL_IP_ERROR.getReturnMes(),
                    "非法IP地址");
            byte[] bytesMap = null;
            try {
                //把map转换为字节数组
                bytesMap = JSON.toJSONString(result).getBytes("GBK");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            //spring 提供数据缓冲类  使用response.bufferFactory()中提供的wrap方法，把字节数组转换为DataBuffer
            DataBuffer dataBuffer = response.bufferFactory().wrap(bytesMap);
            //writeWith 向响应对象中写入影响数据  需要的是Publisher接口的实现类   Mono就是该接口的实现类
            return response.writeWith(Mono.just(dataBuffer));
        }
        //如果不是，让程序继续运行
        return chain.filter(exchange);
    }
    /**
     * 设置当前过滤器的优先级   返回数字越小，优先级越高   存在多个filter时使用
     * @return
     */
    @Override
    public int getOrder() {
        return 6;
    }
}
