package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.Address;

/**
 * @ fileName: 登陆用户信息
 * (Address)表控制层
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:14:41
 * @ version:1.0.0
 */
public interface AddressDao extends BaseMapper<Address> {

    /**
     * 修改所有数据
     *
     * @param addressId 实体对象
     * @return 修改结果
     */
    int updateDefault( Integer addressId);

}

