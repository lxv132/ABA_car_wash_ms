package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.AboutUsImage;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  AboutUsImageService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-25 19:19:20
 * @ version:1.0.0
 */
public interface AboutUsImageService extends IService<AboutUsImage> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<AboutUsImage> queryPage(PagePlugin<AboutUsImage> pagePlugin);
}

