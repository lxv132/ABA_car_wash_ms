package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: AboutUsImage
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-25 19:19:20
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("about_us_image") // 设值数据库表名
public class AboutUsImage extends Model<AboutUsImage> implements Serializable {
    /**
     * 关于我们图片ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 关于我们信息ID，用于关联关于我们信息
     */
    private Integer aboutId;
    /**
     * 关于我们图片链接
     */
    private String imageUrl;

    /**
     * 图片在关于我们信息中的排序序号
     */
    private Integer orderNum;

    /**
     * 记录创建时间
     */
    private String createTime;
}

