package com.aaa.front.controller;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;

import com.aaa.front.entity.StoreLabels;
import com.aaa.front.service.StoreLabelsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (StoreLabels)表控制层
 *
 * @author makejava
 * @since 2023-07-01 11:34:26
 */
@RestController
@RequestMapping("storeLabels")
public class StoreLabelsController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private StoreLabelsService storeLabelsService;


    /**
     * 根据商店id查询对应的商店标签
     * @param storeId
     * @return
     */
    @GetMapping("selectLabelByStoreId")
    public Result selectLabelByStoreId(@RequestParam Integer storeId){
        return success(storeLabelsService.selectLabelByStoreId(storeId));
    }



    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne")
    public Result selectOne(Integer id) {
        return success(this.storeLabelsService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param storeLabels 实体对象
     * @return 新增结果
     */
    @PostMapping("insert")
    public Result insert(@RequestBody StoreLabels storeLabels) {
        return success(this.storeLabelsService.save(storeLabels));
    }

    /**
     * 修改数据
     *
     * @param storeLabels 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody StoreLabels storeLabels) {
        return success(this.storeLabelsService.updateById(storeLabels));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("delete")
    public Result delete(Integer id) {
        return success(this.storeLabelsService.removeById(id));
    }
}

