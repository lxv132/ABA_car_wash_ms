package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.SysLoginLog;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  SysLoginLogService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:30
 * @ version:1.0.0
 */
public interface SysLoginLogService extends IService<SysLoginLog> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<SysLoginLog> queryPage(PagePlugin<SysLoginLog> pagePlugin);
}

