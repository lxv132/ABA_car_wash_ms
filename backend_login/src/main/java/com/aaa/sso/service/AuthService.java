package com.aaa.sso.service;

import com.aaa.common.util.Result;

/**
 * @ fileName:AuthService
 * @ description:让用户和会员有共同的接口
 * @ author:lx
 * @ createTime:2023/6/16 11:39
 * @ version:1.0.0
 */
public interface AuthService {
    /**
     * 认证接口
     * @param userName
     * @param password
     * @return
     */
    Result auth(String userName, String password);
    /**
     * 认证接口
     * @param mobile
     * @param password
     * @return
     */
    Result authA(String mobile, String password);
    /**
     * 验证token
     * @param token
     * @return
     */
    boolean checkToken(String token);
}
