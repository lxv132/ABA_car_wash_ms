package com.aaa.front.controller;


import com.aaa.front.entity.Balance;
import com.aaa.front.entity.Integral;
import com.aaa.front.service.IntegralService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  IntegralController
 * @ description: 我的积分
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:41
 * @ version:1.0.0
 */

@RestController
@RequestMapping("integral")
public class IntegralController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private IntegralService integralService;


    /**
     * 权限管理需要的字段
     * @param page
     * @return
     */
    @PostMapping("queryMune")
    public Result queryMune(@RequestBody PagePlugin<Integral> page){
        return success(integralService.queryMune(page));
    }

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Integral> page) {
        return success(this.integralService.queryPage(page));
    }

    /**
     * 根据用户编号查询积分
     */
    @GetMapping("/queryInteg")
    public Result queryInteg(@RequestParam Integer userId, @RequestParam Integer storeId) {
        return success(this.integralService.queryInteg(userId,storeId));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.integralService.list());
    }
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.integralService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param integral 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody Integral integral) {
        return success(this.integralService.save(integral));
    }

    /**
     * 修改数据
     *
     * @param integral 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody Integral integral) {
        return success(this.integralService.updateById(integral));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.integralService.removeById(id));
    }
}

