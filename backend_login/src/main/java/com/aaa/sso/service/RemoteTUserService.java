package com.aaa.sso.service;

import com.aaa.common.bo.Tuser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ fileName:RemoteUserService
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/16 15:09
 * @ version:1.0.0
 */
@FeignClient(name = "FrontServer")
public interface RemoteTUserService {
    /**
     * 根据用户名查询用户信息
     * @param mobile
     * @return
     */
    @GetMapping("/tuser/queryByMobileLogin")
    Tuser queryByMobile(@RequestParam("mobile") String mobile);
}
