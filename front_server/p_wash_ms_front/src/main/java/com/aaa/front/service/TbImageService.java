package com.aaa.front.service;

import com.aaa.front.entity.TbCommodity;
import com.aaa.front.entity.TbImage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbImageService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:35
 * @ version:1.0.0
 */
public interface TbImageService extends IService<TbImage> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbImage> queryPage(PagePlugin<TbImage> pagePlugin);

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbImage> queryAllStore(PagePlugin<TbImage> pagePlugin);

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbImage> queryAllShop(PagePlugin<TbImage> pagePlugin);
}

