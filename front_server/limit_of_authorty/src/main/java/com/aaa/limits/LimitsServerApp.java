package com.aaa.limits;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ fileName:GatewayServerApp
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/12 9:32
 * @ version:1.0.0
 */
// @SpringBootApplication(exclude = DataSourceAutoConfiguration.class) //springboot 启动类注解
@SpringBootApplication//springboot 启动类注解
@EnableDiscoveryClient // 开启发现客户端  注册中心客户端注解
@MapperScan("com.aaa.limits.dao")
@EnableSwagger2
public class LimitsServerApp {
    public static void main(String[] args) {
       SpringApplication.run(LimitsServerApp.class, args);
    }
}
