package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbProduct;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbProductService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:07
 * @ version:1.0.0
 */
public interface TbProductService extends IService<TbProduct> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbProduct> queryPage(PagePlugin<TbProduct> pagePlugin);
}

