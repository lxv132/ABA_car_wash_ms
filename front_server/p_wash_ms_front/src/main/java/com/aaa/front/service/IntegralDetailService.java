package com.aaa.front.service;

import com.aaa.front.entity.UserDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.IntegralDetail;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  IntegralDetailService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-10 10:11:46
 * @ version:1.0.0
 */
public interface IntegralDetailService extends IService<IntegralDetail> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<IntegralDetail> queryPage(PagePlugin<IntegralDetail> pagePlugin);
    /**
     * 根据用户id和门店id查询余额的详细信息
     * @param userId
     * @param storeId
     * @return
     */
    List<UserDetail> queryUserStore(@Param("userId") Integer userId, @Param("storeId") Integer storeId);
}

