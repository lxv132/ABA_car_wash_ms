package com.aaa.common.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;

import java.io.InputStream;
import java.io.OutputStream;

public class OssUtil {
    /**
     * 通用的文件上传方法
     * @param inputStream 文件对象的输入流(字节流)
     * @param savePath 保存路径(任意路径,加文件名称) 填写Object完整路径，完整路径中不能包含Bucket名称，例如 /scenic/imgs/xxxxxx.jpg。
     * @return
     */
    public static boolean uploadFile (InputStream inputStream,String savePath){

//        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如 /scenic/imgs/xxxxxx.jpg。
//        String objectName = "exampledir/exampleobject.txt";
//        // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
//        // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
//        String filePath= "D:\\localpath\\examplefile.txt";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ConstUtil.FileHandLeConfig.END_POINT,ConstUtil.FileHandLeConfig.ACCESS_KEY_ID , ConstUtil.FileHandLeConfig.ACCESS_KEY_SECRET);

        try {
            //InputStream inputStream = new FileInputStream(filePath);
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(ConstUtil.FileHandLeConfig.BUCKET_NAME, savePath, inputStream);
            // 设置该属性可以返回response。如果不设置，则返回的response为空。
            putObjectRequest.setProcess("true");
            // 创建PutObject请求。
            PutObjectResult result = ossClient.putObject(putObjectRequest);
            // 如果上传成功，则返回200。
            int statusCode = result.getResponse().getStatusCode();
            System.out.println(statusCode);
            //判断statusCode返回值是否为200
            if (statusCode==200){
                return true;
            }
        } catch (Exception oe) {
            oe.printStackTrace();
        }  finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return false;
    }
    /**
     * 通用的文件下载方法
     * @param outputStream 文件对象的输出流(字节流)
     * @param filePath 要下载文件的路径(和上传的路径一致)
     * @return
     */
    public static boolean downloadFile (OutputStream outputStream, String filePath){
        return false;
    }
}
