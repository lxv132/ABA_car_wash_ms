package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.AboutUs;

/**
 * @ fileName: 关于我们信息表(AboutUs)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-25 19:18:34
 * @ version:1.0.0
 */
public interface AboutUsDao extends BaseMapper<AboutUs> {
}

