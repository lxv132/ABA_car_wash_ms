package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: Balance
 * @ description: 登录用户余额
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:11
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_user_balance") //设值数据库表名
public class Balance extends Model<Balance> implements Serializable {
    /**
     * 余额ID
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "balance_id", type = IdType.AUTO)
    private Integer balanceId;

    /**
     * 用户ID，外键关联用户表
     */
    private Integer userId;

    /**
     * 账户余额，保留两位小数
     */
    private double amount;

    /**
     * 门店id，外键关联门店表
     */
    private Integer storeId;

    /**
     * 余额记录创建时间
     */
    private String balanceTime;
    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String storeName;
    /**
     * 我的积分
     */
    @TableField(exist = false)
    private String Inteer;

    /**
     * 用户名
     */
    @TableField(exist = false)
    private String smallname;
}

