package com.aaa.front.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.TbStation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @ fileName:  TbStationService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:51:01
 * @ version:1.0.0
 */
public interface TbStationService extends IService<TbStation> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbStation> queryPage(PagePlugin<TbStation> pagePlugin);

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbStation> queryAllStore(PagePlugin<TbStation> pagePlugin);
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbStation> queryMune(PagePlugin<TbStation> pagePlugin);


}

