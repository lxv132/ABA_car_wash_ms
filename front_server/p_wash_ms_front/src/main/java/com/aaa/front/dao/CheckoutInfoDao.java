package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.CarWashOrderRecord;
import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.entity.CheckoutStatistics;
import com.aaa.front.entity.TbOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

/**
 * @ fileName: 结算信息表(CheckoutInfo)表控制层
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:26
 * @ version:1.0.0
 */
public interface CheckoutInfoDao extends BaseMapper<CheckoutInfo> {

    /**
     * 根据用户id查询信息
     */
    List<CheckoutInfo> queryUser(@Param("userId") Integer userId);
    /**
     * 根据车牌号查询信息
     */
    CheckoutInfo queryCarNo(@Param("carNo") String carNo);

    /**
     * 根据时间统计所有门店信息
     * @param date
     * @return
     */
    List<CheckoutStatistics> getDailyCheckoutStatistics(@Param("date")  String date);

    /**
     * 洗车订单信息
     * @param date
     * @return
     */
    List<CarWashOrderRecord> queryOrder(@Param("date")  String date, @Param("userId") Integer userId);
    /**
     * 门店id查询单个门店的信息
     * @param storeId
     * @return
     */
    List<CheckoutStatistics> getDailyCheckoutStatisticsId(@Param("storeId")  Integer storeId);


    /**
     * 权限管理需要的字段
     *
     * @param
     */
    <E extends IPage<CheckoutInfo>> E queryMune(E page);

    List<CheckoutInfo> queryOrderTime();
    List<CheckoutInfo> queryOrderDay();
    List<CheckoutInfo> queryOrderTotal();
}

