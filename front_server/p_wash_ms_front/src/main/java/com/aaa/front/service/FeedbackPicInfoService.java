package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.FeedbackPicInfo;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  FeedbackPicInfoService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:27
 * @ version:1.0.0
 */
public interface FeedbackPicInfoService extends IService<FeedbackPicInfo> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<FeedbackPicInfo> queryPage(PagePlugin<FeedbackPicInfo> pagePlugin);

    /**
     *
     * @param entities
     * @param feedbackId
     * @return
     */
    int insertBatch( List entities,Integer feedbackId);
}

