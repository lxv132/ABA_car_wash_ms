package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ fileName: FeedbackPicInfo
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:27
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("feedback_pic_info") //设值数据库表名
public class FeedbackPicInfo extends Model<FeedbackPicInfo> implements Serializable {
    /**
     * 反馈图片ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "feedback_pic_id", type = IdType.AUTO)
    private Integer feedbackPicId;

    /**
     * 反馈ID，用于关联反馈信息
     */
    private int feedbackId;

    /**
     * 反馈图片链接
     */
    private String picUrl;

    /**
     * 记录创建时间
     */
    private Date createTime;
}

