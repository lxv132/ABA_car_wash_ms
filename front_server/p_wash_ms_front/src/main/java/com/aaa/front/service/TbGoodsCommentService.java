package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbGoodsComment;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @ fileName:  TbGoodsCommentService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:33
 * @ version:1.0.0
 */
public interface TbGoodsCommentService extends IService<TbGoodsComment> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbGoodsComment> queryPage(PagePlugin<TbGoodsComment> pagePlugin);
    /**
     * 评价查看
     * @param storeId
     * @param score
     * @param isimage
     * @return
     */
    List<TbGoodsComment> queryStore(@Param("storeId") Integer storeId, @Param("score") Integer score ,@Param("isimage") String isimage);


}

