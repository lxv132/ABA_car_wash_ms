package com.aaa.front.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.Car;
import com.aaa.front.entity.CheckoutInfo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @ fileName:  CarService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 10:30:55
 * @ version:1.0.0
 */
public interface CarService extends IService<Car> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<Car> queryPage(PagePlugin<Car> pagePlugin);

    /**
     * 根据用户id查询所有车辆
     * @param userId
     * @return
     */
    List<Car> queryPageAll(@Param("userId") Integer userId);

    /**
     * 根据用户和门店i的查询所有车辆
     * @param userId
     * @return
     */
    List<Car> queryStore(@Param("userId") Integer userId,@Param("userId") Integer storeId);

    /**
     * 门店和车辆多表联查
     * @return
     */
    List<Car> queryCar(Integer id);

   Car queryCarNo(@Param("carNo") String carNo,@Param("storeId") Integer storeId);


    /**
     * 权限管理需要的字段
     *
     * @param
     */
    IPage queryMune(PagePlugin<Car> pagePlugin);
}

