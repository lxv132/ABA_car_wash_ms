package com.aaa.limits.controller;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;


import com.aaa.limits.entity.UserDetail;
import com.aaa.limits.service.PayService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @ fileName:PayController
 * @ description: 微信支付
 * @ author:
 * @ date: 2023/4/8 0:05
 * @ version:1.0.0
 */
@RestController
@RequestMapping("pay")
public class PayController extends BaseController {
    @Resource
    private PayService payService;

    /**
     * 生成支付二维码
     * @param detail
     * @return
     */
    @PostMapping("getNativeCodeUrl")
    public Result getNativeCodeUrl(@RequestBody UserDetail detail){
        Map map=payService.getNativeByOrder(detail);
        return success(map);
    }

    /**
     * 获取支付状态
     * @param detailId
     * @return
     */
    @GetMapping("getPayStatus/{detailId}")
    public Result getPayStatus(@PathVariable("detailId") String detailId)   {
        Map<String, String> map = payService.getPayStatus(detailId);
        return success(map);
    }
}
