package com.aaa.front.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.dao.AreaDao;
import com.aaa.front.entity.Area;
import com.aaa.front.service.AreaService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ fileName:  AreaServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 17:07:07
 * @ version:1.0.0
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaDao, Area> implements AreaService {

    /**
     * 服务对象
     */
    @Resource
    private AreaDao areaDao;


    @Override
    public Integer queryByAreaName(String areaName, String city, String province) {
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(areaName != null, "name", areaName);
        if (areaDao.selectList(queryWrapper).size() > 1) {
            queryWrapper.like(city != null, "fullname", city);
            if (areaDao.selectList(queryWrapper).size() > 1) {
                queryWrapper.like(province != null, "fullname", province);
            }
        }
        Area area = areaDao.selectOne(queryWrapper);
        return area.getCode();
    }

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<Area> queryPage(PagePlugin<Area> pagePlugin) {
        // 分页参数设置
        Page<Area> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.areaDao.selectPage(page, queryWrapper);
    }


}

