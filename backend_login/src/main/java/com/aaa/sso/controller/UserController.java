package com.aaa.sso.controller;

import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.sso.service.AuthService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @ fileName:UserController
 * @ description: 后端用户管理登录的接口
 * @ author:lx
 * @ createTime:2023/6/16 11:29
 * @ version:1.0.0
 */
@RestController
@RequestMapping("Auser")
public class UserController extends BaseController {

    @Resource(name="uas") //依赖注入 按名称注入
    private AuthService authService;
    /**
     * 后台用户登录
     * @param userName
     * @param password
     * @return
     */
    @GetMapping("login")
    public Result login(String userName, String password){
        //调用服务层
        return authService.auth(userName, password);
    }

    /**
     * 验证token
     * @param token
     * @return
     */
    @GetMapping("checkToken")
    public boolean checkToken(@RequestParam("token") String token){
        return authService.checkToken(token);
    }
}
