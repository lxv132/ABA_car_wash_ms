package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @ fileName: FeedbackInfo
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-27 14:28:24
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("feedback_info") //设值数据库表名
public class FeedbackInfo extends Model<FeedbackInfo> implements Serializable {
    /**
     * 反馈ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "feedback_id", type = IdType.AUTO)
    private Integer feedbackId;

    /**
     * 用户ID，用于关联用户信息
     */
    private Integer userId;

    /**
     * 反馈类型（1，操作复杂，2 内容卡顿，3  App体验太差）
     */
    private String feedbackType;

    /**
     * 反馈消息
     */
    private String feedbackMsg;

    /**
     * 联系方式（手机号或者QQ号）
     */
    private String contact;

    /**
     * 记录创建时间
     */
    private String createTime;
    /**
     * 反馈图片
     */
    @TableField(exist = false)
    private List feedbackList;
}

