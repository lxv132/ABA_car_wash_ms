package com.aaa.front.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.dao.CarAndStoreDao;
import com.aaa.front.entity.CarAndStore;
import com.aaa.front.service.CarAndStoreService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ fileName:  CarAndStoreServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 15:59:56
 * @ version:1.0.0
 */
@Service("carAndStoreService")
public class CarAndStoreServiceImpl extends ServiceImpl<CarAndStoreDao, CarAndStore> implements CarAndStoreService {

    /**
     * 服务对象
     */
    @Resource
    private CarAndStoreDao carAndStoreDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<CarAndStore> queryPage(PagePlugin<CarAndStore> pagePlugin) {
        // 分页参数设置
        Page<CarAndStore> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.carAndStoreDao.selectPage(page, queryWrapper);
    }
}

