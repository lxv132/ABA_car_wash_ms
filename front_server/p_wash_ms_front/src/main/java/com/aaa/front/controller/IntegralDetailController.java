package com.aaa.front.controller;


import com.aaa.front.entity.IntegralDetail;
import com.aaa.front.service.IntegralDetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

/**
 * @ fileName:  IntegralDetailController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-10 10:11:45
 * @ version:1.0.0
 */

@RestController
@RequestMapping("integralDetail")
public class IntegralDetailController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private IntegralDetailService integralDetailService;


    /**
     * 根据用户编号和门店编号查询余额详情
     *
     */
    @GetMapping("/queryUserStore")
    public Result queryUserStore(@RequestParam Integer userId, @RequestParam Integer storeId) {
        return success(this.integralDetailService.queryUserStore(userId,storeId));
    }


    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<IntegralDetail> page) {
        return success(this.integralDetailService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.integralDetailService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.integralDetailService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param integralDetail 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody IntegralDetail integralDetail) {
        return success(this.integralDetailService.save(integralDetail));
    }

    /**
     * 修改数据
     *
     * @param integralDetail 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody IntegralDetail integralDetail) {
        return success(this.integralDetailService.updateById(integralDetail));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.integralDetailService.removeById(id));
    }
}

