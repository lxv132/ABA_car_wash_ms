package com.aaa.front.service;

import com.aaa.front.entity.CarWashOrderRecord;
import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.entity.CheckoutStatistics;
import com.aaa.front.entity.TbOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @ fileName:  CheckoutInfoService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:26
 * @ version:1.0.0
 */
public interface CheckoutInfoService extends IService<CheckoutInfo> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<CheckoutInfo> queryPage(PagePlugin<CheckoutInfo> pagePlugin);
    /**
     * 根据用户id查询信息
     */
    List<CheckoutInfo> queryUser(@Param("userId") Integer userId);
    /**
     * 根据车牌号查询信息
     */
    CheckoutInfo queryCarNo(@Param("carNo") String carNo);

    /**
     * 洗车订单信息
     * @param date
     * @return
     */
    List<CarWashOrderRecord> queryOrder(@Param("date")  String date, @Param("userId") Integer userId);

    List<CheckoutStatistics> getDailyCheckoutStatistics(@Param("date")  String date);

    List<CheckoutStatistics> getDailyCheckoutStatisticsId(@Param("storeId")  Integer storeId);
    List<CheckoutInfo> queryOrderTime();
    List<CheckoutInfo> queryOrderDay();
    List<CheckoutInfo> queryOrderTotal();
    /**
     * 权限管理需要的字段
     *
     * @param
     */
    IPage queryMune(PagePlugin<CheckoutInfo> pagePlugin);
}

