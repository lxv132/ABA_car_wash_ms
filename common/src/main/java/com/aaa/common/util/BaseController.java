package com.aaa.common.util;

/**
 * @ fileName:BaseController
 * @ description:封装controller层的返回方法
 * @ author:lx
 * @ createTime:2023/5/4 9:06
 * @ version:1.0.0
 */
public class BaseController {
    /** 其他方法继承该方法类时，都可调用此方法
     * 封装成功方法
     * @param data
     * @return
     * @param <T>
     */
    //因为注入用来被继承，所以推荐使用 protected
    protected <T> Result success(T data){
        return new Result(ResultStatus.SUCCESS.getReturnCode(),
                ResultStatus.SUCCESS.getReturnMes(), data);
    }
    protected <T> Result  error(T data){
        //把重复两个参数在这写
        return new Result(ResultStatus.ERROR.getReturnCode(),
                ResultStatus.ERROR.getReturnMes(),data);
    }
}
