package com.aaa.common.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ fileName:Result
 * @ description:统一返回结果处理
 * @ author:lx
 * @ createTime:2023/5/4 8:58
 * @ version:1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
//<T> 泛型
public class Result<T> {

    //返回码
    private int code;
    //返回信息
    private String message;
    //返回数据
    private T data;
}
