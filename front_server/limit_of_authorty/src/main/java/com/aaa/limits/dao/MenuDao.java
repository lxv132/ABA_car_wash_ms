package com.aaa.limits.dao;

import com.aaa.common.vo.TreeNode;
import com.aaa.limits.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.io.Serializable;
import java.util.List;

/**
 * @ fileName: 菜单权限表(Menu)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:31:51
 * @ version:1.0.0
 */
public interface MenuDao extends BaseMapper<Menu>    {
    /**
     * 添加
     * @param treeNode
     * @return
     */
    int  add(TreeNode treeNode);

    /**
     * 修改
     * @param treeNode
     * @return
     */
    int  update(TreeNode treeNode);

    /**
     * 查询所有权限信息
     * @return
     */
    List<TreeNode>  queryByAllMenu();
    /**
     * 根据用户编号，查询菜单树
     * @param userId
     * @return
     */
    List<TreeNode> queryTreeNodeByUserId(int userId);

    /**
     * 根据用户编号查询该用户拥有的按钮权限
     * @return
     */
    List<String> queryButtonPermsByUserId(Integer userId);
}

