package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ fileName: Area
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 17:07:06
 * @ version:1.0.0
 */
@Data
@TableName("tb_area") // 设值数据库表名
public class Area extends Model<Area> implements Serializable {
    /**
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "code", type = IdType.AUTO)
    private Integer code;

    /**
     * 名字
     */
    private String name;

    /**
     * country:国家、province:省份（直辖市会在province和city显示）、city:市（直辖市会在province和city显示）、district:区县
     */
    private String level;

    /**
     * 所属行政区划代码
     */
    private Integer pcode;

    /**
     * 所属行政区划名字
     */
    private String pname;

    /**
     * 行政区划完整名字
     */
    private String fullname;

    /**
     * 经度
     */
    private double longitude;

    /**
     * 纬度
     */
    private double latitude;

    @TableField(exist = false)
    private Integer id;

    @TableField(exist = false)
    private String label;

    @TableField(exist = false)
    private Integer parentId;

    @TableField(exist = false)
    private String parentName;
    @TableField(exist = false)
    private List<Area> children;
}

