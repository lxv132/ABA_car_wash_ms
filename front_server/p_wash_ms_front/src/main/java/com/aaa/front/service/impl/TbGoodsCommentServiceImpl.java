package com.aaa.front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.TbGoodsCommentDao;
import com.aaa.front.entity.TbGoodsComment;
import com.aaa.front.service.TbGoodsCommentService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  TbGoodsCommentServiceImpl
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:34
 * @ version:1.0.0
 */
@Service("tbGoodsCommentService")
public class TbGoodsCommentServiceImpl extends ServiceImpl<TbGoodsCommentDao, TbGoodsComment> implements TbGoodsCommentService {

    /**
     * 服务对象
     */
    @Resource
    private TbGoodsCommentDao tbGoodsCommentDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<TbGoodsComment> queryPage(PagePlugin<TbGoodsComment> pagePlugin) {
        // 分页参数设置
        Page<TbGoodsComment> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.tbGoodsCommentDao.selectPage(page, queryWrapper);
    }

    @Override
    public List<TbGoodsComment> queryStore(Integer storeId, Integer score,String isimage) {
        return tbGoodsCommentDao.queryStore(storeId, score,isimage);
    }
}

