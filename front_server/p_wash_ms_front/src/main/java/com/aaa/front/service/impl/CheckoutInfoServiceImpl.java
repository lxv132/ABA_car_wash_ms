package com.aaa.front.service.impl;

import com.aaa.front.entity.CarWashOrderRecord;
import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.entity.CheckoutStatistics;
import com.aaa.front.entity.TbOrder;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.CheckoutInfoDao;
import com.aaa.front.service.CheckoutInfoService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  CheckoutInfoServiceImpl
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:26
 * @ version:1.0.0
 */
@Service("checkoutInfoService")
public class CheckoutInfoServiceImpl extends ServiceImpl<CheckoutInfoDao, CheckoutInfo> implements CheckoutInfoService {

    /**
     * 服务对象
     */
    @Resource
    private CheckoutInfoDao checkoutInfoDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<CheckoutInfo> queryPage(PagePlugin<CheckoutInfo> pagePlugin) {
        // 分页参数设置
        Page<CheckoutInfo> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.checkoutInfoDao.selectPage(page, queryWrapper);
    }

    @Override
    public List<CheckoutInfo> queryUser(Integer userId) {
        return checkoutInfoDao.queryUser(userId);
    }

    @Override
    public CheckoutInfo queryCarNo(String carNo) {
        return checkoutInfoDao.queryCarNo(carNo);
    }

    @Override
    public List<CarWashOrderRecord> queryOrder(String date, Integer userId) {
        return checkoutInfoDao.queryOrder(date, userId);
    }


    @Override
    public List<CheckoutStatistics> getDailyCheckoutStatistics(String date) {
        return checkoutInfoDao.getDailyCheckoutStatistics(date);
    }

    @Override
    public List<CheckoutStatistics> getDailyCheckoutStatisticsId(Integer storeId) {
        return checkoutInfoDao.getDailyCheckoutStatisticsId(storeId);
    }
    @Override
    public List<CheckoutInfo> queryOrderTime() {
        return this.checkoutInfoDao.queryOrderTime();
    }
    @Override
    public List<CheckoutInfo> queryOrderDay() {
        return checkoutInfoDao.queryOrderDay();
    }
    @Override
    public List<CheckoutInfo> queryOrderTotal() {
        return checkoutInfoDao.queryOrderTotal();
    }

    @Override
    public IPage  queryMune(PagePlugin<CheckoutInfo> pagePlugin) {
        // 分页参数设置
        Page<CheckoutInfo> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        return checkoutInfoDao.queryMune(page);
    }
}

