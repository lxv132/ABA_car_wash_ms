package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbOrderStatusCode;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbOrderStatusCodeService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-12 10:18:24
 * @ version:1.0.0
 */
public interface TbOrderStatusCodeService extends IService<TbOrderStatusCode> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbOrderStatusCode> queryPage(PagePlugin<TbOrderStatusCode> pagePlugin);
}

