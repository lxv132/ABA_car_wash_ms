package com.aaa.front.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ fileName:CheckoutStatistics
 * @ description:
 * @ author:lx
 * @ createTime:2023/7/9 14:40
 * @ version:1.0.0
 */
@Data
public class CheckoutStatistics {
    private String storeName;
    private String date;
    private Integer count;
    private Integer storeId;
}
