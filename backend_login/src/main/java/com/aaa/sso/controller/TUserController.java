package com.aaa.sso.controller;

import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.sso.service.AuthService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ fileName:UserController
 * @ description: 前端用户管理登录的接口
 * @ author:李壮壮
 * @ createTime:2023/6/16 11:29
 * @ version:1.0.0
 */
@RestController
@RequestMapping("TuserA")
public class TUserController extends BaseController {

    @Resource(name="tUser") //依赖注入 按名称注入
    private AuthService authService;
    /**
     * 后台用户登录
     * @param mobile
     * @param password
     * @return
     */
    @GetMapping("login")
    public Result login(String mobile, String password){
        //调用服务层
        return authService.authA(mobile, password);
    }

    /**
     * 验证token
     * @param token
     * @return
     */
    @GetMapping("checkToken")
    public boolean checkToken(@RequestParam("token") String token){
        return authService.checkToken(token);
    }
}
