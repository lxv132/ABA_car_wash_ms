package com.aaa.front.controller;


import com.aaa.front.entity.Address;
import com.aaa.front.service.AddressService;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.List;
import java.util.Map;

/**
 * @ fileName:  AddressController
 * @ description: 收货地址表
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:14:41
 * @ version:1.0.0
 */

@RestController
@RequestMapping("address")
public class AddressController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private AddressService addressService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Address> page) {
        return success(this.addressService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll(String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String userId = login.toString().replace("\"", "");
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",Integer.valueOf(userId));
        return success(this.addressService.list(queryWrapper));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.addressService.getById(id));
    }

    /**
     * 根据用户编号查询用户的收货地址
     *
     * @param token
     * @return 单条数据
     */
    @GetMapping("/queryDefaultAddress")
    public Result queryDefaultAddress(String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String userId = login.toString().replace("\"", "");
        QueryWrapper<Address> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("user_id",Integer.valueOf(userId)).eq("is_default",'1');
        List<Address> list = this.addressService.list(objectQueryWrapper);
        System.out.println("---------------"+list);
        return success(list);
    }


    /**
     * 新增数据
     *
     * @param address 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody Address address) {
        boolean save = this.addressService.save(address);
        this.updateDefault(address.getAddressId());
        return success(save);
    }

    /**
     * 修改所有数据
     *
     * @param addressId 实体对象
     * @return 修改结果
     */
    @PostMapping("updateDefault")
    public Result updateDefault( Integer addressId) {
        return success(this.addressService.updateDefault(addressId));
    }


    /**
     * 修改数据
     *
     * @param address 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody Address address) {
        this.updateDefault(address.getAddressId());
        return success(this.addressService.updateById(address));
    }

    /**
     * 删除数据
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.addressService.removeById(id));
    }
}

