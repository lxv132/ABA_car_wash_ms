package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ fileName: TbStore
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:50:40
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_store") //设值数据库表名
public class TbStore extends Model<TbStore> implements Serializable {
    /**
     * 门店ID，主键
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "store_id", type = IdType.AUTO)
    private Integer storeId;

    /**
     * 门店名称
     */
    private String storeName;

    /**
     * 门店地址
     */
    private String address;

    /**
     * 门店电话
     */
    private String phone;

    /**
     * 所在城市
     */
    private String city;

    /**
     * 所在省份
     */
    private String province;

    /**
     * 门店图片
     */
    private String storeimage;

    /**
     * 门店地址
     */
    private double washPrice;
    /**
     * 门店评分
     */
    private String country;

    /**
     * 经度
     */
    private double longitude;

    /**
     * 纬度
     */
    private double latitude;
    /**
     * 门店状态，如1表示营业中，0表示关闭
     */
    private String status;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 门店创建时间
     */
    private String createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 门店更新时间
     */
    private String updateTime;


    @TableField(exist = false)
    private List storeLabels;

    /**
     * 所有工位数量
     */
    @TableField(exist = false)
    private String countA;
    /**
     * 使用中工位数量
     */
    @TableField(exist = false)
    private String countB;

    /**
     * 工位空闲数量   这个字段是自己定义的，表中并没有这个字段
     */
    @TableField(exist = false)
    private Integer idleNumber;

    @TableField(exist = false)
    public String jvli;

    /**
     * 用户id
     */
    @TableField(exist = false)
    private Integer userId;
    /**
     * 用户Name
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 用户积分id
     */
    @TableField(exist = false)
    private Integer integralId;
    /**
     * 用户积分
     */
    @TableField(exist = false)
    private Integer credits;
    /**
     * 用户余额id
     */
    @TableField(exist = false)
    private Integer balanceId;
    /**
     * 用户余额
     */
    @TableField(exist = false)
    private Integer amount;


    /**
     * 车辆id
     */
    @TableField(exist = false)
    private Integer carId;
    /**
     * 车牌
     */
    @TableField(exist = false)
    private String carNo;
}

