package com.aaa.front.controller;


import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.entity.TbGoodsComment;
import com.aaa.front.service.TbGoodsCommentService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.Date;

/**
 * @ fileName:  TbGoodsCommentController
 * @ description: 商品评价表
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:33
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbGoodsComment")
public class TbGoodsCommentController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbGoodsCommentService tbGoodsCommentService;
    @Resource
    private CheckoutInfoController checkoutInfoController;

    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbGoodsComment> page) {
        return success(this.tbGoodsCommentService.queryPage(page));
    }


    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbGoodsCommentService.list());
    }

    /**
     * 根据门店id查询该门店的所有评论
     */
    @GetMapping("/queryStore")
    public Result queryStore(@RequestParam Integer storeId, @RequestParam Integer score, @RequestParam String isimage) {
        return success(this.tbGoodsCommentService.queryStore(storeId, score, isimage));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbGoodsCommentService.getById(id));
    }

    /**
     * 只适用与洗车
     *
     * @param tbGoodsComment 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    @Transactional // 开启事务
    public Result insert(@RequestBody TbGoodsComment tbGoodsComment) {
        Result result = checkoutInfoController.queryById(tbGoodsComment.getGoodid());
        CheckoutInfo checkoutInfo= (CheckoutInfo)result.getData();
        checkoutInfo.setGoodid("0");
        tbGoodsComment.setEvaluatetime(new Date().toLocaleString());
        boolean save = this.tbGoodsCommentService.save(tbGoodsComment);
        checkoutInfoController.update(checkoutInfo);
        return success(save);
    }

    /**
     * 修改数据
     *
     * @param tbGoodsComment 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbGoodsComment tbGoodsComment) {
        return success(this.tbGoodsCommentService.updateById(tbGoodsComment));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbGoodsCommentService.removeById(id));
    }
}

