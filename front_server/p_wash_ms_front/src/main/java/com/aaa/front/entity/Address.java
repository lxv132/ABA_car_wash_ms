package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: Address
 * @ description: 收货地址表
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:14:41
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_user_address") //设值数据库表名
public class Address extends Model<Address> implements Serializable {
    /**
     * 地址ID
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "address_id", type = IdType.AUTO)
    private Integer addressId;

    /**
     * 用户ID，用于关联用户信息
     */
    private Integer userId;

    /**
     * 收件人名称
     */
    private String addressName;

    /**
     * 收件人手机号码
     */
    private String phone;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区县
     */
    private String district;

    /**
     * 详细地址
     */
    private String detail;

    private String isDefault;
}

