package com.aaa.limits.util;

import com.auth0.jwt.interfaces.Claim;
import lombok.extern.log4j.Log4j2;

import java.util.Map;

/**
 * @ fileName:queryButtonPermByToken
 * @ description: 实现获取登录方法的工具类
 * @ author:lx
 * @ createTime:2023/6/21 20:56
 * @ version:1.0.0
 */
@Log4j2
public class queryButton{
    public  static  Map<String, Claim>  queryButtonPermByToken(String token){
        String tokenSub = token.substring(token.indexOf("-") + 1);
        Map<String, Claim> payloadByToken = JwtUtils.getPayloadByToken(tokenSub);
        return payloadByToken;
    }
}
