package com.aaa.front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.TbStoreEvaluateDao;
import com.aaa.front.entity.TbStoreEvaluate;
import com.aaa.front.service.TbStoreEvaluateService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbStoreEvaluateServiceImpl
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 14:17:43
 * @ version:1.0.0
 */
@Service("tbStoreEvaluateService")
public class TbStoreEvaluateServiceImpl extends ServiceImpl<TbStoreEvaluateDao, TbStoreEvaluate> implements TbStoreEvaluateService {

    /**
     * 服务对象
     */
    @Resource
    private TbStoreEvaluateDao tbStoreEvaluateDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<TbStoreEvaluate> queryPage(PagePlugin<TbStoreEvaluate> pagePlugin) {
        // 分页参数设置
        Page<TbStoreEvaluate> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空  
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
             queryWrapper.like(null != pagePlugin.getData().getEvaluateId(), "evaluate_id", pagePlugin.getData().getEvaluateId());
        }
        return this.tbStoreEvaluateDao.selectPage(page, queryWrapper);
    }
}

