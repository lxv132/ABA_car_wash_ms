package com.aaa.front.service.impl;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.dao.TuserDao;
import com.aaa.front.entity.Tuser;
import com.aaa.front.service.TuserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ fileName:  TuserServiceImpl
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-25 09:52:44
 * @ version:1.0.0
 */
@Service
public class TuserServiceImpl extends ServiceImpl<TuserDao, Tuser> implements TuserService {

    /**
     * 服务对象
     */
    @Resource
    private TuserDao tuserDao;

    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<Tuser> queryPage(PagePlugin<Tuser> pagePlugin) {
        // 分页参数设置
        Page<Tuser> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            queryWrapper.like(null != pagePlugin.getData().getMobile(), "mobile", pagePlugin.getData().getMobile());
            queryWrapper.like(null != pagePlugin.getData().getSex(), "sex", pagePlugin.getData().getSex());
            queryWrapper.like(null != pagePlugin.getData().getState(), "state", pagePlugin.getData().getState());
        }
        return this.tuserDao.selectPage(page, queryWrapper);
    }

    /**
     * 根据用户账号查询数据
     * @param mobile
     * @return
     */
    @Override
    public Tuser queryByMobile(String mobile) {
        Tuser user =new Tuser();
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // // 使用加各种参数  判断某字符串是否为空
        user.setMobile(mobile);
        // user.setStatus("0");
        // user.setDelFlag("0");
        queryWrapper.eq(null!=user,"mobile",user.getMobile());
        // queryWrapper.eq("del_flag",user.getDelFlag());
        return tuserDao.selectOne(queryWrapper);
    }

    @Override
    public Tuser queryUserWash(Integer userId) {
        return tuserDao.queryUserWash(userId);
    }
    @Override
    public List<Tuser> annualQuery(Tuser tuser) {
        return tuserDao.annualQuery(tuser);
    }
}

