package com.aaa.front.controller;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.TbStore;
import com.aaa.front.service.CommonFileHandlerService;
import com.aaa.front.service.TbStoreService;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ fileName:  TbStoreController
 * @ description: 门店表
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:50:39
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbStore")
public class TbStoreController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbStoreService tbStoreService;

    @Resource
    private CommonFileHandlerService commonFileHandlerService;

    /**
     * 文件上传对象  必须使用@RequestParam绑定上传对象,不可以少，少了上传不了
     * MultipartFile
     * @param file1
     * @return
     */
    @PostMapping("uploadFile")
    public Result uploadFile(@RequestParam MultipartFile file1) {
        // 调用封装的服务接口
        return success(commonFileHandlerService.uploadFile(file1, "two/qianduan/"));
    }

    /**
     * 查询店铺信息
     * @return
     */
    @GetMapping("/selectStoreByStoreId")
    public Result selectStoreByStoreId(TbStore tbStore) {
        return success(tbStoreService.selectStoreByStoreId(tbStore));
    }
    /**
     * 查询所有数据
     */
    @GetMapping("/queryAllCount")
    public Result queryAllCount(Integer storeId) {
        return success(this.tbStoreService.queryAllCount(storeId));
    }


    /**
     * 根据用户编号查询门店
     * @param id
     * @return
     */
    @GetMapping("queryUserStore")
    public Result queryUserStore(Integer id) {
        return success(this.tbStoreService.queryUserStore(id));
    }

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbStore> page) {
        return success(this.tbStoreService.queryPage(page));
    }

    /**
     * 查询所有数据(淘汰)
     */
    @GetMapping("/queryAll")
    public Result queryAll(TbStore tbStore) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", "1");
        queryWrapper.orderByDesc("country");
        if (tbStore != null) {
            queryWrapper.eq(tbStore.getProvince() != null, "province", tbStore.getProvince());
            queryWrapper.eq(tbStore.getCity() != null, "city", tbStore.getCity());
            queryWrapper.like(tbStore.getStoreName() != null, "store_name", tbStore.getStoreName());
        }
        return success(this.tbStoreService.list(queryWrapper));
    }


    /**
     * 根据门店名称查询所有数据
     */
    @GetMapping("/selectAllStore")
    public Result selectAllStore(@RequestParam String storeName) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", "1");
        queryWrapper.like("" != storeName, "store_name", storeName);
        return success(this.tbStoreService.list(queryWrapper));
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.like("status", "1");
        queryWrapper.like("store_id", id);
        return success(this.tbStoreService.list(queryWrapper));
    }

    /**
     * 新增数据
     * @param tbStore 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbStore tbStore, String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        tbStore.setCreateBy(loginName);
        tbStore.setCreateTime(new Date().toLocaleString());
        return success(this.tbStoreService.save(tbStore));
    }

    /**
     * 根据token获取用户id查询用户绑定的门店和对应门店的积分和余额
     * @param
     * @return
     */
    @GetMapping("selectStoreByStoreIdAndUserId")
    public Result selectStoreByStoreIdAndUserId(@RequestParam Integer userId, @RequestParam Integer storeId) {
        return success(tbStoreService.selectStoreByStoreIdAndUserId(userId, storeId));
    }

    /**
     * 根据车牌和门店查询
     * @param carNo
     * @return
     */
    @PostMapping("storeUserCarId")
    public Result storeUserCarId(@RequestParam String carNo) {
        return success(tbStoreService.storeUserCarId(carNo));
    }

    /**
     * 修改数据
     * @param tbStore 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbStore tbStore, String token) {
        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("login");
        String loginName = login.toString().replace("\"", "");
        tbStore.setUpdateBy(loginName);
        tbStore.setUpdateTime(new Date().toLocaleString());
        return success(this.tbStoreService.updateById(tbStore));
    }

    /**
     * 删除数据
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbStoreService.removeById(id));
    }
}

