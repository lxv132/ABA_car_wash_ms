package com.aaa.front.controller;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.CarAndStore;
import com.aaa.front.service.CarAndStoreService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @ fileName:  CarAndStoreController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 16:05:20
 * @ version:1.0.0
 */

@RestController
@RequestMapping("carAndStore")
public class CarAndStoreController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private CarAndStoreService carAndStoreService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<CarAndStore> page) {
        return success(this.carAndStoreService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.carAndStoreService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.carAndStoreService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param carAndStore 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody CarAndStore carAndStore) {
        return success(this.carAndStoreService.save(carAndStore));
    }

    /**
     * 修改数据
     *
     * @param carAndStore 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody CarAndStore carAndStore) {
        return success(this.carAndStoreService.updateById(carAndStore));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.carAndStoreService.removeById(id));
    }
}

