package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: PayLog
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:28
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("pay_log") //设值数据库表名
public class PayLog extends Model<PayLog> implements Serializable {
    /**
     * 订单日志id，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "pay_log_id", type = IdType.AUTO)
    private Integer payLogId;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 支付状态id
     */
    private Integer payStatusId;

    /**
     * 支付状态码(和订单状态表绑定)
     */
    private Integer payStatusCode;

    /**
     * 支付状态
     */
    private String payStatusName;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 操作时间
     */
    private Date operationTime;

    /**
     * 操作人id
     */
    private Integer operationId;
}

