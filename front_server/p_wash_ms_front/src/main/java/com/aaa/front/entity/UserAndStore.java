package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ fileName: UserAndStore
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-06 11:01:54
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_and_store") // 设值数据库表名
public class UserAndStore extends Model<UserAndStore> implements Serializable {

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 门店id
     */
    private Integer storeId;
}

