package com.aaa.front.controller;


import cn.hutool.core.date.DateUtil;
import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.AboutUs;
import com.aaa.front.service.AboutUsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @ fileName:  AboutUsController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-25 19:18:33
 * @ version:1.0.0
 */

@RestController
@RequestMapping("aboutUs")
public class AboutUsController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private AboutUsService aboutUsService;

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<AboutUs> page) {
        return success(this.aboutUsService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.aboutUsService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.aboutUsService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param aboutUs 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody AboutUs aboutUs) {
        aboutUs.setCreateTime(DateUtil.now());
        return success(this.aboutUsService.save(aboutUs));
    }

    /**
     * 修改数据
     *
     * @param aboutUs 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody AboutUs aboutUs) {
        return success(this.aboutUsService.updateById(aboutUs));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.aboutUsService.removeById(id));
    }
}

