package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ fileName: Car
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 10:30:55
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_car") // 设值数据库表名
public class Car extends Model<Car> implements Serializable {
    /**
     * 车辆ID
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "car_id", type = IdType.AUTO)
    private Integer carId;

    /**
     * 所属用户ID，外键关联用户表
     */
    private Integer userId;

    /**
     * 车牌号
     */
    private String carNo;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 用户昵称
     */
    @TableField(exist = false)
    private String smallname;

    /**
     * 用户电话
     */
    @TableField(exist = false)
    private String mobile;

    /**
     * 车辆绑定的门店数量
     */
    @TableField(exist = false)
    private int carNum;
}

