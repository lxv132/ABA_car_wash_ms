package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.TbGoodsComment;

/**
 * @ fileName: 商品评论
 * (TbGoodsComment)表控制层
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:33
 * @ version:1.0.0
 */
public interface TbGoodsCommentDao extends BaseMapper<TbGoodsComment> {

    /**
     * 评价查看
     * @param storeId
     * @param score
     * @param isimage
     * @return
     */
    List<TbGoodsComment> queryStore(@Param("storeId") Integer storeId, @Param("score") Integer score,@Param("isimage") String isimage);


}

