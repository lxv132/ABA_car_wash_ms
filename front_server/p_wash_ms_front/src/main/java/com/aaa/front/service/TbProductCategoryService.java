package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TbProductCategory;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TbProductCategoryService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:11
 * @ version:1.0.0
 */
public interface TbProductCategoryService extends IService<TbProductCategory> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TbProductCategory> queryPage(PagePlugin<TbProductCategory> pagePlugin);
}

