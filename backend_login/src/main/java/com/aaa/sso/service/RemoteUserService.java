package com.aaa.sso.service;

import com.aaa.common.bo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ fileName:RemoteUserService
 * @ description:
 * @ author:lx
 * @ createTime:2023/6/16 15:09
 * @ version:1.0.0
 */
@FeignClient(name = "LimitsServer")
public interface RemoteUserService {
    /**
     * 根据用户名查询用户信息
     * @param userName
     * @return
     */
    @GetMapping("/user/queryByUserName")
    User queryByUserName(@RequestParam("userName") String userName);
}
