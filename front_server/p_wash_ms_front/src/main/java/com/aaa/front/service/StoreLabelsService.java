package com.aaa.front.service;



import com.aaa.front.entity.StoreLabels;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * (StoreLabels)表服务接口
 *
 * @author makejava
 * @since 2023-07-01 11:34:27
 */
public interface StoreLabelsService extends IService<StoreLabels> {

    /**
     * 根据商店id查询对应的商店标签
     * @param storeId
     * @return
     */
    List<StoreLabels> selectLabelByStoreId(Integer storeId);

}

