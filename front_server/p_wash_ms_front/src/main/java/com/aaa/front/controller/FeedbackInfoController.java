package com.aaa.front.controller;


import com.aaa.front.entity.FeedbackInfo;
import com.aaa.front.service.FeedbackInfoService;
import com.aaa.front.service.FeedbackPicInfoService;
import com.aaa.front.util.queryButton;
import com.auth0.jwt.interfaces.Claim;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.Date;
import java.util.Map;

/**
 * @ fileName:  FeedbackInfoController
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-27 14:28:23
 * @ version:1.0.0
 */

@RestController
@RequestMapping("feedbackInfo")
public class FeedbackInfoController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private FeedbackInfoService feedbackInfoService;
    @Resource
    private FeedbackPicInfoService feedbackPicInfoService;


    /**
     * 分页查询所有数据
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<FeedbackInfo> page) {
        return success(this.feedbackInfoService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.feedbackInfoService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.feedbackInfoService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param feedbackInfo 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody FeedbackInfo feedbackInfo,String token) {

        Map<String, Claim> stringClaimMap = queryButton.queryButtonPermByToken(token);
        Claim login = stringClaimMap.get("tuserId");
        String loginName = login.toString().replace("\"", "");
        //设置用户id
        feedbackInfo.setUserId(Integer.valueOf(loginName));
        //设置创建时间
        feedbackInfo.setCreateTime(new Date().toLocaleString());
        boolean save = this.feedbackInfoService.save(feedbackInfo);

        Integer feedbackId = feedbackInfo.getFeedbackId();

        System.out.println("feedbackInfo.getFeedbackList() = " + feedbackInfo.getFeedbackList());
        this.feedbackPicInfoService.insertBatch(feedbackInfo.getFeedbackList(),feedbackId);
        return success(save);
    }

    /**
     * 修改数据
     *
     * @param feedbackInfo 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody FeedbackInfo feedbackInfo) {
        return success(this.feedbackInfoService.updateById(feedbackInfo));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.feedbackInfoService.removeById(id));
    }
}

