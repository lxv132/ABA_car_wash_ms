package com.aaa.limits.controller;


import cn.hutool.jwt.JWTUtil;
import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;
import com.aaa.common.vo.TreeNode;
import com.aaa.limits.entity.Menu;
import com.aaa.limits.service.MenuService;
import com.aaa.limits.util.JwtUtils;
import com.auth0.jwt.interfaces.Claim;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.List;
import java.util.Map;

import static org.checkerframework.checker.units.UnitsTools.s;

/**
 * @ fileName:  MenuController
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 18:31:49
 * @ version:1.0.0
 */
@Log4j2
@RestController
@RequestMapping("menu")
public class MenuController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private MenuService menuService;

    /**
     * 获取当前用户拥有权限按钮列表
     * @param token
     * @return
     */
    @GetMapping("queryButtonPermByToken")
    public Result queryButtonPermByToken(String token){
        String tokenSub = token.substring(token.indexOf("-") + 1);
        Map<String, Claim> payloadByToken = JwtUtils.getPayloadByToken(tokenSub);
        Claim userId1 = payloadByToken.get("userId");
        log.info(userId1+".................");
        //userId="1"
        String userId = userId1.toString().replace("\"", "");
        //根据用户编号，查询出该用户权限菜单
        return success(menuService.queryButtonPermsByUserId(Integer.valueOf(userId)));
    }


    /**
     * 根据用户编号，查询菜单树
     * @return
     */
    @GetMapping("queryTreeNodeByUserId")
   public Result queryTreeNodeByUserId(String token){
        return success(menuService.queryTreeNodeByUserId(token));
    }

    /**
     * 生成
     *
     * @return
     */
    @PostMapping("queryTree")
    public Result queryTreeData() {
        return success(menuService.queryTreeData());
    }
    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     * @param page 分页对象
     * @return 所有数据
     */
    @GetMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Menu> page) {
        return success(this.menuService.queryPage(page));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.menuService.list());
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.menuService.getById(id));
    }

    /**
     * 添加
     * @param treeNode
     * @return
     */
    @PutMapping("add")  //地址栏中的请求，都是get请求，该请求无法在地址栏进行模拟  如果在地址栏模拟，会出现 HTTP状态 405 - 方法不允许
    public Result add(@RequestBody TreeNode treeNode) {//@RequestBody  把对象以json字符串的格式放入Request对象的Body中传递，get请求是没有body
        return success(menuService.add(treeNode));
    }

    /**
     * 更新
     * @param treeNode
     * @return
     */
    @PutMapping("update")
    public Object update(@RequestBody  TreeNode treeNode) {
        return success(menuService.update(treeNode));
    }


    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.menuService.removeById(id));
    }
}

