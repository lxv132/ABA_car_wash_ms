package com.aaa.front.service;

import com.aaa.front.entity.Balance;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.Integral;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

/**
 * @ fileName:  IntegralService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:41
 * @ version:1.0.0
 */
public interface IntegralService extends IService<Integral> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<Integral> queryPage(PagePlugin<Integral> pagePlugin);

    /**
     * 根据用户标号查询积分
     * @param userId
     * @return
     */
    Integral  queryInteg(@Param("userId") Integer userId, @Param("storeId") Integer storeId);
    /**
     * 权限管理需要的字段
     *
     * @param
     */
    IPage queryMune(PagePlugin<Integral> pagePlugin);
}

