package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: Record
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-27 14:18:33
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("car_wash_record") //设值数据库表名
public class Record extends Model<Record> implements Serializable {
    /**
     * 洗车记录ID，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID，用于关联用户信息
     */
    private Integer userId;

    /**
     * 车辆ID，用于关联车辆信息
     */
    private Integer carId;

    /**
     * 洗车时间
     */
    private Date washTime;

    /**
     * 洗车类型
     */
    private String washType;

    /**
     * 洗车价格
     */
    private Double washPrice;

    /**
     * 是否会员（0或1）
     */
    private Integer isMember;

    /**
     * 记录创建时间
     */
    private Date createTime;
}

