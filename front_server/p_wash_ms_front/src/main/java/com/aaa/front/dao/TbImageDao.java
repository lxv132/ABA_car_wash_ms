package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.TbCommodity;
import com.aaa.front.entity.TbImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

/**
 * @ fileName: 图片表(TbImage)表控制层
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:32
 * @ version:1.0.0
 */
public interface TbImageDao extends BaseMapper<TbImage> {

    <E extends IPage<TbImage>> E queryAllStore(E page, int shopId);
    <E extends IPage<TbImage>> E queryAllsjop(E page);

}

