package com.aaa.front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.AboutUsDao;
import com.aaa.front.entity.AboutUs;
import com.aaa.front.service.AboutUsService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  AboutUsServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-25 19:18:35
 * @ version:1.0.0
 */
@Service("aboutUsService")
public class AboutUsServiceImpl extends ServiceImpl<AboutUsDao, AboutUs> implements AboutUsService {

    /**
     * 服务对象
     */
    @Resource
    private AboutUsDao aboutUsDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<AboutUs> queryPage(PagePlugin<AboutUs> pagePlugin) {
        // 分页参数设置
        Page<AboutUs> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
             queryWrapper.like(null != pagePlugin.getData().getTitle(), "title", pagePlugin.getData().getTitle());
        }
        return this.aboutUsDao.selectPage(page, queryWrapper);
    }
}

