package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.RegionInfo;

/**
 * @ fileName: 地区信息表(RegionInfo)表控制层
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:29
 * @ version:1.0.0
 */
public interface RegionInfoDao extends BaseMapper<RegionInfo> {

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<RegionInfo> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<RegionInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<RegionInfo> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<RegionInfo> entities);

}

