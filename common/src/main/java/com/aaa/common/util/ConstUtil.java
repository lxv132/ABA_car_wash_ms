package com.aaa.common.util;

/**
 * @ fileName:ConstUtil
 * @ description: 常量工具类
 * @ author:lx
 * @ createTime:2023/6/14 9:10
 * @ version:1.0.0
 */
public class ConstUtil {
    /**
     * 定义shiro的加密相关的常量
     */
    public interface CredentialsMatcher {
        String HASH_ALGORITHM_NAME = "SHA-512";
        int HASH_ITERATIONS = 1024;
    }

    /**
     * 部门状态
     */
    public interface DeptStatus {
        int ENABLED = 1;
        int DISABLED = 2;
    }

    /**
     * 职位/角色状态
     */
    public interface PositionStatus {
        int ENABLED = 1;
        int DISABLED = 0;
    }

    /**
     * 日志操作类型
     */
    public interface OperLogBussinessType {
        int ADD = 1;
        int UPDATE = 2;
        int DELETE = 3;
    }

    /**
     * 日志操作类型
     */
    public interface OperLogOperatorType {
        int BACKUSER = 1;
        int MOBILEUSER = 2;
        int OTHER = 0;
    }

    /**
     * 积分和余额状态
     */
    public interface integral {
        // 为在该门店消费
        int UNCONSUMED = 301;
        // 积分不足
        int UNDERINTEGRATION = 302;
        // 余额充足
        int BANDEQUACY = 303;

    }

    // 其他常量配置  继续在下面配置

    /**
     * 订单状态
     */
    public interface OrderStatus {
        // 已完成
        Integer COMPLETED = 5;
        //  待付款
        Integer PENDING_PAYMENT = 11;
        //  已付款
        Integer PAID = 12;
        // 待发货
        int Tickets_have_been_collected = 13;
        // 已取消
        int Comment_timeout_ended = 9;
        // 下单
        int Pending_review = 3;
        // 待退款
        int To_be_refunded = 15;
        // 待付款退单结束
        int Pending_Payment_Chargeback_End = 16;
        // 付款超时结束
        int Payment_timeout_ended = 17;
        // 预订成功退单结束
        int Reservation_successful_Chargeback_end = 18;
        // 退款审核不通过结束
        int Refund_review_failed_End = 19;
        // 退款完成结束
        int Refund_completed = 20;
        // 取票超时结束
        int Ticket_collection_timeout_ended = 21;
        // 退票结束
        int End_of_refund = 22;
        // 点评完成结束
        int End_of_review_completion = 28;
    }

    public interface FileHandLeConfig {
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String END_POINT = "https://oss-cn-beijing.aliyuncs.com";
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String ACCESS_KEY_ID = "LTAI5t7Sa8endaganT3qLjwi";
        String ACCESS_KEY_SECRET = "xC6fG226rUaMzI1XLSxC7rCRZTDanG";
        // 填写Bucket名称，例如examplebucket。
        String BUCKET_NAME = "flyskyfly";
        // bucket的域名
        String BUCKET_DAEMON = "https://flyskyfly.oss-cn-beijing.aliyuncs.com/";

    }
    // 定义其他常量.........
}
