package com.aaa.front.dao;

import com.aaa.front.entity.TbStation;
import com.aaa.front.entity.TbStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName: 门店信息
 * (TbStore)表控制层
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:50:40
 * @ version:1.0.0
 */
public interface TbStoreDao extends BaseMapper<TbStore> {
   /**
    * 根据用户编号查询门店
    * @param id
    * @return
    */
   List<TbStore> queryUserStore(Integer id);

   List<TbStore> queryAllCount(Integer storeId);

   /**
    * 使用中的工位
    * @param storeId
    * @return
    */
   List<TbStore> queryAllCounA(Integer storeId);


   /**
    * 根据用户id和门店id查询店铺信息
    * @param userId
    * @param storeId
    * @return
    */
   TbStore selectStoreByStoreIdAndUserId(@Param("userId") Integer userId,@Param("storeId") Integer storeId);

   /**
    * 查询店铺信息
    * @return
    */
   List<TbStore> selectStoreByStoreId(TbStore tbStore);

   /**
    * 通过店铺信息查看与之关联的车辆
    * @param carNo
    * @return
    */
   List<TbStore> storeUserCarId(@Param("carNo") String carNo);
}

