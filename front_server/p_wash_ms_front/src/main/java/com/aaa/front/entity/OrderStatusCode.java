package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: OrderStatusCode
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:27
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("order_status_code") //设值数据库表名
public class OrderStatusCode extends Model<OrderStatusCode> implements Serializable {
    /**
     * 状态id，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "status_id", type = IdType.AUTO)
    private Integer statusId;

    /**
     * 状态码
     */
    private Integer orderStatusCode;

    /**
     * 状态
     */
    private String orderStatusName;
}

