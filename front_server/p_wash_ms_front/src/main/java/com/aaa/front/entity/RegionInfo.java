package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: RegionInfo
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:03:29
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("region_info") //设值数据库表名
public class RegionInfo extends Model<RegionInfo> implements Serializable {
    /**
     * 地区唯一编码，主键自增
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 地区名称
     */
    private String name;

    /**
     * 地区简称
     */
    private String shortName;

    /**
     * 上级地区编码
     */
    private Integer parentId;

    /**
     * 地域层级（国家、省市）
     */
    private Integer level;

    /**
     * 是否为末级地区节点
     */
    private Integer isLeaf;

    /**
     * 描述
     */
    private String description;
}

