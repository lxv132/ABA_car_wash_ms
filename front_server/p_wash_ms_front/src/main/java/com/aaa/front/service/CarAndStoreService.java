package com.aaa.front.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.CarAndStore;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ fileName:  CarAndStoreService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-28 15:59:56
 * @ version:1.0.0
 */
public interface CarAndStoreService extends IService<CarAndStore> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<CarAndStore> queryPage(PagePlugin<CarAndStore> pagePlugin);
}

