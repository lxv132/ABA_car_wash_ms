package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.TbCommodity;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.TbOrder;

/**
 * @ fileName: 订单(TbOrder)表控制层
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:05:39
 * @ version:1.0.0
 */
public interface TbOrderDao extends BaseMapper<TbOrder> {

    /**
     * 查询订单状态
     * @param orderStatusCode
     * @return
     */
    List<TbOrder> queryByIdCode(Integer userId,Integer orderStatusCode);



    /**
     * 权限管理需要的字段
     * @param orderStatusCode
     */
    <E extends IPage<TbOrder>> E  queryMune( E page,@Param("orderStatusCode") Integer orderStatusCode,@Param("tcount") String tcount);
}

