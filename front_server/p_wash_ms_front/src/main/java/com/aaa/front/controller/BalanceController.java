package com.aaa.front.controller;


import com.aaa.front.entity.Balance;
import com.aaa.front.entity.CheckoutInfo;
import com.aaa.front.entity.Integral;
import com.aaa.front.service.BalanceService;
import com.aaa.front.service.IntegralService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.PagePlugin;

import java.util.Date;

/**
 * @ fileName:  BalanceController
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:11
 * @ version:1.0.0
 */

@RestController
@RequestMapping("balance")
public class BalanceController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private BalanceService balanceService;
    @Resource
    private IntegralService integralService;

    /**
     * 权限管理需要的字段
     * @param page
     * @return
     */
    @PostMapping("queryMune")
    public Result queryMune(@RequestBody PagePlugin<Balance> page){
        return success(balanceService.queryMune(page));
    }


    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<Balance> page) {
        return success(this.balanceService.queryPage(page));
    }


    /**
     * 根据用户编号和门店编号查询余额
     */
    @GetMapping("/queryUserAndStore")
    public Result queryUserAndStore(@RequestParam Integer userId, @RequestParam Integer storeId) {
        return success(this.balanceService.queryUserAndStore(userId,storeId));
    }


    /**
     * 根据用户编号查询积分
     */
    @GetMapping("/queryInteg")
    public Result queryInteg(Integer userId) {
        return success(this.balanceService.queryInteg(userId));
    }

    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.balanceService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.balanceService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param balance 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody Balance balance) {
        boolean save = this.balanceService.save(balance);
        return success(save);
    }

    /**
     * 添加余额和商品
     * @param userId
     * @param storeId
     * @return
     */
    @PostMapping("addBalanceIna")
    public Result addBalanceIna(@RequestParam Integer userId, @RequestParam Integer storeId){
        Balance balance = new Balance();
        balance.setUserId(userId);
        balance.setStoreId(storeId);
        balance.setAmount(0.00);
        balance.setBalanceTime(new Date().toLocaleString());
        boolean save = balanceService.save(balance);

        Integral integral = new Integral();
        integral.setUserId(userId);
        integral.setStoreId(storeId);
        integral.setCredits(0);
        integral.setIntegralTime(new Date().toLocaleString());
        integralService.save(integral);
        return success(save);
    }
    /**
     * 而根据用户id查询余额，积分，门店名称
     * @param userId
     * @return
     */
    @PostMapping("queryAllye")
    public Result queryAllye(Integer userId) {
        return success(this.balanceService.queryAllye(userId));
    }

    /**
     * 修改数据
     *
     * @param balance 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody Balance balance) {
        return success(this.balanceService.updateById(balance));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.balanceService.removeById(id));
    }
}

