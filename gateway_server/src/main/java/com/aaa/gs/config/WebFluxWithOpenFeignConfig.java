package com.aaa.gs.config;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;

import java.util.stream.Collectors;

/**
 * @ fileName:WebFluxWithOpenFeignConfig
 * @ description: 解决gateway不支持openfeign的问题的
 * @ author:lx
 * @ createTime:2023/6/17 10:51
 * @ version:1.0.0
 */
@Configuration
public class WebFluxWithOpenFeignConfig {
    @Bean
    @ConditionalOnMissingBean  //判断如果没有HttpMessageConverters 不会被加载
    public HttpMessageConverters messageConverters(ObjectProvider<HttpMessageConverter<?>> converters) {
        return new HttpMessageConverters(converters.orderedStream().collect(Collectors.toList()));
    }
}
