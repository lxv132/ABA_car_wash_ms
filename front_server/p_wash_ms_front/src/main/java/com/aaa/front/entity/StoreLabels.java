package com.aaa.front.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * (StoreLabels)表实体类
 *
 * @author makejava
 * @since 2023-07-01 11:34:27
 */
@SuppressWarnings("serial")
@TableName("tb_store_labels")
@Data
public class StoreLabels extends Model<StoreLabels> {
    //标签id
    @TableId(type = IdType.AUTO)
    private Integer labelId;
    //标签关联门店id
    private Integer storeId;
    //标签内容
    private String labelContent;

    @TableField (exist = false)
    private TbStore store;
}

