package com.aaa.limits.entity;

import lombok.Data;

@Data
public class UserAndRole {
    private Integer  userId;
    private Integer roleId;
}
