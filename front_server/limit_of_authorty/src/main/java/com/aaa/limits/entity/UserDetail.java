package com.aaa.limits.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * (UserDetail)表实体类
 *
 * @author makejava
 * @since 2023-07-03 21:19:52
 */
@SuppressWarnings("serial")
@TableName("tb_user_detail")
@Data
public class UserDetail extends Model<UserDetail> {
    //明细ID
    private String  detailId;
    //外键，关联用户ID
    private Integer userId;
    //支出金额
    private Double expenditure;
    //实际充值金额
    private Double rechargeMoney;
    //优惠金额
    private Double discountsMoney;
    //实际到账金额 (实际充值金额+优惠金额)
    private Double accountMoney;
    //充值时间
    private Date rechargeTime;
    //支出时间
    private Date expenditureTime;
    //外键关联门店id
    private Integer storeId;
}

