package com.aaa.front.service.impl;

import com.aaa.front.entity.UserDetail;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.IntegralDetailDao;
import com.aaa.front.entity.IntegralDetail;
import com.aaa.front.service.IntegralDetailService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  IntegralDetailServiceImpl
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-10 10:11:46
 * @ version:1.0.0
 */
@Service("integralDetailService")
public class IntegralDetailServiceImpl extends ServiceImpl<IntegralDetailDao, IntegralDetail> implements IntegralDetailService {

    /**
     * 服务对象
     */
    @Resource
    private IntegralDetailDao integralDetailDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<IntegralDetail> queryPage(PagePlugin<IntegralDetail> pagePlugin) {
        // 分页参数设置
        Page<IntegralDetail> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.integralDetailDao.selectPage(page, queryWrapper);
    }

    @Override
    public List<UserDetail> queryUserStore(Integer userId, Integer storeId) {
        return integralDetailDao.queryUserStore(userId, storeId);
    }
}

