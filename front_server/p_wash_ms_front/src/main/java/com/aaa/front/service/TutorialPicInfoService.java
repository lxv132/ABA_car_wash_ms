package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.TutorialPicInfo;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  TutorialPicInfoService
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:20:06
 * @ version:1.0.0
 */
public interface TutorialPicInfoService extends IService<TutorialPicInfo> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<TutorialPicInfo> queryPage(PagePlugin<TutorialPicInfo> pagePlugin);
}

