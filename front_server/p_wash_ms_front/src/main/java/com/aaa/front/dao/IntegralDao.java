package com.aaa.front.dao;

import java.util.List;

import com.aaa.front.entity.CheckoutInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.Integral;

/**
 * @ fileName: 登陆用户积分
 * (Integral)表控制层
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:41
 * @ version:1.0.0
 */
public interface IntegralDao extends BaseMapper<Integral> {
    Integral  queryInteg(@Param("userId") Integer userId, @Param("storeId") Integer storeId);
    /**
     * 权限管理需要的字段
     *
     * @param
     */
    <E extends IPage<Integral>> E queryMune(E page);
}

