package com.aaa.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.front.entity.CreditInfo;
import com.aaa.common.vo.PagePlugin;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  CreditInfoService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-27 14:29:14
 * @ version:1.0.0
 */
public interface CreditInfoService extends IService<CreditInfo> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<CreditInfo> queryPage(PagePlugin<CreditInfo> pagePlugin);
}

