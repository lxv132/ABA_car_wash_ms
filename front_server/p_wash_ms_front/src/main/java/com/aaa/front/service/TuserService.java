package com.aaa.front.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.Tuser;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ fileName:  TuserService
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-25 09:52:44
 * @ version:1.0.0
 */
public interface TuserService extends IService<Tuser> {
    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    IPage<Tuser> queryPage(PagePlugin<Tuser> pagePlugin);

    Tuser queryByMobile(String mobile);
    /**
     * 查询洗车详情信息
     * @param userId
     * @return
     */
    Tuser queryUserWash(@Param("userId") Integer userId);
    //统计图查询
    List<Tuser> annualQuery(Tuser tuser);
}

