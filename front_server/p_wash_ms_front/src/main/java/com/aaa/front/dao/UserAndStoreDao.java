package com.aaa.front.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aaa.front.entity.UserAndStore;

/**
 * @ fileName: (UserAndStore)表控制层
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-07-06 11:01:54
 * @ version:1.0.0
 */
public interface UserAndStoreDao extends BaseMapper<UserAndStore> {

    /**
     * 根据用户名和店铺名查询
     * @param userId
     * @param storeId
     * @return
     */
    List<UserAndStore> selectByStoreIdAndUserId(@Param("userId") Integer userId,@Param("storeId") Integer storeId);
}

