package com.aaa.common.vo;

import lombok.Data;

@Data
// 泛型
public class Page<T>{
    //页码
   private Integer pageNo;
   //每页显示的数量
   private Integer pageSize;
   //泛型 输入什么类型就是什么类型
   private T data;
}
