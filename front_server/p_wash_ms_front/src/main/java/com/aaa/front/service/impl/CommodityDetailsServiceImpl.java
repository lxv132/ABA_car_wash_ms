package com.aaa.front.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.CommodityDetailsDao;
import com.aaa.front.entity.CommodityDetails;
import com.aaa.front.service.CommodityDetailsService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @ fileName:  CommodityDetailsServiceImpl
 * @ description:
 * @ fileName: Dandelion
 * @ createTime: 2023-06-25 19:45:05
 * @ version:1.0.0
 */
@Service("commodityDetailsService")
public class CommodityDetailsServiceImpl extends ServiceImpl<CommodityDetailsDao, CommodityDetails> implements CommodityDetailsService {

    /**
     * 服务对象
     */
    @Resource
    private CommodityDetailsDao commodityDetailsDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<CommodityDetails> queryPage(PagePlugin<CommodityDetails> pagePlugin) {
        // 分页参数设置
        Page<CommodityDetails> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空  
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
        }
        return this.commodityDetailsDao.selectPage(page, queryWrapper);
    }
}

