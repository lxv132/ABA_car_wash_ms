package com.aaa.front.service;

import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.Area;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @ fileName:  AreaService
 * @ description:
 * @ fileName: lx
 * @ createTime: 2023-06-15 17:07:07
 * @ version:1.0.0
 */
public interface AreaService extends IService<Area> {

    /**
     * 根据名字查询数组
     * @param areaName
     * @param city
     * @param province
     * @return
     */
    Integer queryByAreaName(String areaName,String city,String province);
    IPage<Area> queryPage(PagePlugin<Area> pagePlugin);

}

