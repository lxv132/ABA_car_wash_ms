package com.aaa.front.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ fileName: TbProduct
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-29 10:06:06
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
@TableName("tb_product") //设值数据库表名
public class TbProduct extends Model<TbProduct> implements Serializable {
    /**
     * 商品ID
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    @TableId(value = "product_id", type = IdType.AUTO)
    private Integer productId;

    /**
     * 外键关联商品分类
     */
    private Integer proTypeId;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 图片
     */
    private String picture;

    /**
     * 积分
     */
    private String integral;

    /**
     * 价格
     */
    private String price;

    /**
     * 优惠券
     */
    private String coupon;

    /**
     * 运费
     */
    private String freight;

    /**
     * 温馨提示
     */
    private String prompt;

    /**
     * 商品详情（使用富文本编辑器存储）
     */
    private String detailsId;
}

