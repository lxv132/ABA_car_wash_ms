package com.aaa.front.service.impl;

import com.aaa.front.entity.Integral;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.front.dao.BalanceDao;
import com.aaa.front.entity.Balance;
import com.aaa.front.service.BalanceService;
import org.springframework.stereotype.Service;
import com.aaa.common.vo.PagePlugin;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @ fileName:  BalanceServiceImpl
 * @ description:
 * @ fileName: zzw
 * @ createTime: 2023-06-25 19:19:12
 * @ version:1.0.0
 */
@Service("balanceService")
public class BalanceServiceImpl extends ServiceImpl<BalanceDao, Balance> implements BalanceService {

    /**
     * 服务对象
     */
    @Resource
    private BalanceDao balanceDao;


    /**
     * 分页查询
     *
     * @param pagePlugin 筛选条件
     * @return 查询结果
     */
    @Override
    public IPage<Balance> queryPage(PagePlugin<Balance> pagePlugin) {
        // 分页参数设置
        Page<Balance> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        // 查询封装类
        QueryWrapper queryWrapper = new QueryWrapper();
        // 使用加各种参数  判断某字符串是否为空
        if (pagePlugin.getData() != null) {
            // 例如 queryWrapper.like(null!=语句,"列名",pagePingin.getData.属性名)-> if(属性名!=null){ 列名 like "%属性名%"
            // queryWrapper.like(null != pagePlugin.getData().getName(), "name", pagePlugin.getData().getName());
            queryWrapper.like(null != pagePlugin.getData().getBalanceId(), "integral_id", pagePlugin.getData().getBalanceId());

        }
        return this.balanceDao.selectPage(page, queryWrapper);
    }

    @Override
    public Balance queryInteg(Integer userId) {
        return this.balanceDao.queryInteg(userId);
    }

    @Override
    public List<Balance> queryAllye(Integer userId) {
        return this.balanceDao.queryAllye(userId);
    }

    @Override
    public Balance queryUserAndStore(Integer userId, Integer storeId) {
        return balanceDao.queryUserAndStore(userId, storeId);
    }

    @Override
    public IPage queryMune(PagePlugin<Balance> pagePlugin) {
        // 分页参数设置
        Page<Balance> page = new Page<>();
        // 设置当前页码  第几页
        page.setCurrent(pagePlugin.getPageNo());
        // 设置每页显示条数
        page.setSize(pagePlugin.getPageSize());
        return balanceDao.queryMune(page);
    }
}

