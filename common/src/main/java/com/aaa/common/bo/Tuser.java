package com.aaa.common.bo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ fileName: Tuser
 * @ description:
 * @ fileName: 李壮壮
 * @ createTime: 2023-06-25 09:53:55
 * @ version:1.0.0
 */
@SuppressWarnings("serial")
@Data
public class Tuser  implements Serializable {
    /**
     * uuid方式生成
     * AUTO 数据库主键--自增
     * INPUT 手动生成
     * NONE 不设置
     * ASSIGN_UUID UUID生成
     * ASSIGN_ID 雪花算法
     */
    private Integer id;

    /**
     * 可用手机号或账号登陆
     */
    private String account;
    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 昵称
     */
    private String smallname;

    /**
     * 性别
     */
    private String sex;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * qq
     */
    private String qq;

    /**
     * 证件号
     */
    private String zjno;

    /**
     * 状态
     */
    private String state;

    /** 创建时间 */
    private String cjtime;

    /** 更新时间 */
    private String gxtime;

    /**
     * 盐
     */
    private String salt;
}

