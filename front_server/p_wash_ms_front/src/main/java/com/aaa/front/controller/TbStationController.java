package com.aaa.front.controller;


import com.aaa.common.util.BaseController;
import com.aaa.common.util.Result;
import com.aaa.common.vo.Page;
import com.aaa.common.vo.PagePlugin;
import com.aaa.front.entity.TbStation;
import com.aaa.front.service.CommonFileHandlerService;
import com.aaa.front.service.TbStationService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ fileName:  TbStationController
 * @ description:  门店关联表
 * @ fileName: zzw
 * @ createTime: 2023-06-26 16:51:00
 * @ version:1.0.0
 */

@RestController
@RequestMapping("tbStation")
public class TbStationController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TbStationService tbStationService;
    @Resource
    private CommonFileHandlerService commonFileHandlerService;

    /**
     * 文件上传对象  必须使用@RequestParam绑定上传对象,不可以少，少了上传不了
     * MultipartFile
     *
     * @param file1
     * @return
     */
    @PostMapping("uploadFile")
    public Result uploadFile(@RequestParam MultipartFile file1) {
        // 调用封装的服务接口
        return success(commonFileHandlerService.uploadFile(file1, "two/lxx/"));
    }

    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryMune")
    public Result queryMune(@RequestBody PagePlugin<TbStation> page) {
        return success(this.tbStationService.queryMune(page));
    }



    /**
     * 分页查询所有数据
     * <p>
     * 如果需要带参数分页请前往Impl中添加
     *
     * @param page 分页对象
     * @return 所有数据
     */
    @PostMapping("queryPage")
    public Result queryPage(@RequestBody PagePlugin<TbStation> page) {
        return success(this.tbStationService.queryPage(page));
    }

    /**
     * 多表联查
     */
    @PostMapping("queryAllStore")
    public Result queryAllStore(@RequestBody PagePlugin<TbStation> page) {
        return success(this.tbStationService.queryAllStore(page));
    }


    @GetMapping("queryStoreName")
    public List<TbStation> queryStoreName(Integer storeId,String storeName){
        QueryWrapper<TbStation> qw = new QueryWrapper<>();
        qw.eq("store_id",storeId);
        qw.eq("station_name",storeName);
        return this.tbStationService.list(qw);
    }


    /**
     * 查询所有数据
     */
    @GetMapping("/queryAll")
    public Result queryAll() {
        return success(this.tbStationService.list());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryById")
    public Result queryById(Integer id) {
        return success(this.tbStationService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param tbStation 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Result insert(@RequestBody TbStation tbStation) {
        return success(this.tbStationService.save(tbStation));
    }

    /**
     * 修改数据
     *
     * @param tbStation 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Result update(@RequestBody TbStation tbStation) {
        return success(this.tbStationService.updateById(tbStation));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除结果
     */
    @DeleteMapping("deleteById")
    public Result delete(Integer id) {
        return success(this.tbStationService.removeById(id));
    }
}

