package com.aaa.sso.service.Impl;

import com.aaa.common.bo.Tuser;
import com.aaa.common.util.ConstUtil;
import com.aaa.common.util.Result;
import com.aaa.common.util.ResultStatus;
import com.aaa.sso.service.AuthService;
import com.aaa.sso.service.RemoteTUserService;
import com.aaa.sso.util.JwtUtils;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @ fileName:UserAuthServiceImpl
 * @ description:
 * @ author:李壮壮
 * @ createTime:2023/6/16 11:41
 * @ version:1.0.0
 */
@Service("tUser")
public class TuserAuthServiceImpl implements AuthService {
    @Resource
    private RemoteTUserService remoteUserService;

    @Resource //依赖注入  spring框架封装的Redis连接模板类
    private RedisTemplate redisTemplate;
    @Override
    public Result auth(String userName, String password) {
        return null;
    }
    @Override
    public Result authA(String mobile, String password) {
        // 根据用户名查询用户信息
        Tuser tuser = remoteUserService.queryByMobile(mobile);
        System.out.println("---------"+tuser);
        // 再次使用密码和盐值进行加密运算
        Sha512Hash sha512Hash = new Sha512Hash(password, tuser.getSalt(),
                ConstUtil.CredentialsMatcher.HASH_ITERATIONS);
        // 判断密码是否正确
        if (tuser.getPassword().equals(sha512Hash.toString())) {
            Map<String,String> payload = new HashMap<>();
            payload.put("tuserId", tuser.getId().toString());
            payload.put("userName", tuser.getName());
            payload.put("login", tuser.getMobile());
            //生成token  使用jwt
            String token = tuser.getSmallname()+"-"+JwtUtils.generateToken(payload);

            //将生成的token存储到redis中  方便后面校验token
            //设置redis的key的序列化方式  使用StringRedisSerializer 方式时，存储到redis中的就是string字符串
            redisTemplate.setKeySerializer(new StringRedisSerializer());
            //先查询userName+"-"+*****
            Set keys = redisTemplate.keys(tuser.getSmallname() + "-*");
            //在循环删除
            for (Object key : keys) {
                redisTemplate.delete(key);
            }
            //把token放入redis把token作为key 用户对象作为值 方便后面使用token获取用户信息
            redisTemplate.opsForValue().set(token,tuser,8, TimeUnit.HOURS);

            //把token返回给前端
            return new Result(ResultStatus.SUCCESS.getReturnCode(),
                    ResultStatus.SUCCESS.getReturnMes(), token);
        } else {
            //用户名或者密码错误返回错误JSON
            return new Result(ResultStatus.ERROR.getReturnCode(),
                    ResultStatus.ERROR.getReturnMes(), "用户名或者密码错误");
        }
    }

    @Override
    public boolean checkToken(String token) {
        //设置redis的key的序列化方式  使用StringRedisSerializer 方式时，存储到redis中的就是string字符串
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        String substring = token.substring(token.indexOf("-") + 1);
        //根据JwtUtils判断token是否正确  在判断redis是否存在(是否过期)
        if (JwtUtils.verify(substring)&&redisTemplate.hasKey(token)){
            return true;
        }
        return false;
    }

}
